<?php
require_once("../modelos/galeriasModel.php");
require_once("../core/helpers/paginacion.php");
//--Declaraciones
$mensajes = array();
$arreglo_datos = helper_userdata();
redireccionar_metodos($arreglo_datos);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'consultar_imagenes_galeria':
			consultar_imagenes_galeria($arreglo_datos["idioma"]);
			break;
		case 'consultar_cuerpo_galeria_detalle':
			consultar_cuerpo_galeria_detalle($arreglo_datos["idioma"],$arreglo_datos["offset"],$arreglo_datos["limit"],$arreglo_datos["actual"]);
			break;
		case 'consultar_imagen_principal':
			consultar_imagen_principal($arreglo_datos['id'],$arreglo_datos["idioma"],$arreglo_datos["offset"],$arreglo_datos["limit"],$arreglo_datos["actual"]);		
			break;
		case 'consultar_cuerpo_galeria_video':
			consultar_cuerpo_galeria_video($arreglo_datos["idioma"],$arreglo_datos["offset"],$arreglo_datos["limit"],$arreglo_datos["actual"]);		
			break;
	}
}
//---
function helper_userdata(){
	$user_data = array();
	if($_POST){
		//--
		if(array_key_exists('idioma', $_POST)){
			$user_data['idioma'] = $_POST['idioma'];
		}
		if(array_key_exists('accion', $_POST)){
			$user_data["accion"] = $_POST["accion"];
		}
		if(array_key_exists('offset', $_POST)){
			$user_data["offset"] = $_POST["offset"];
		}
		if(array_key_exists('limit', $_POST)){
			$user_data["limit"] = $_POST["limit"];
		}
		if(array_key_exists('actual', $_POST)){
			$user_data["actual"] = $_POST["actual"];
		}
		if(array_key_exists('id', $_POST)){
			$user_data["id"] = $_POST["id"];
		}	
		//--
	}
	return $user_data;
}
//---
function consultar_imagenes_galeria($idioma){
	$arreglo = array();
	$obj = new galeriasModel();
	$recordset = $obj->consultar_imagenes($idioma);
	if($recordset!="error"){
		for($i=0;$i<count($recordset);$i++){
			$arreglo[$i]["imagen"] = $recordset[$i][1];
		}
	}else{
		$arreglo[0]["error"] ="error";
	}
	die(json_encode($arreglo));
}
//--
//--Funcion para carga de cuerpo videos
function consultar_cuerpo_galeria_video($idioma,$offset,$limit,$actual){
	$arreglo = array();
	$obj = new galeriasModel();
	$video_cabecera_html = "";
	$video_detalle_html = "";
	//--Parametros de consulta...
	if($actual=="")$actual=0;
	$contador_interno=0;
	$nom_fun = "cargar_pagina_video";
	$num_rows = $obj->consultar_num_rows_videos();
	$arreglo["cuantos_son"] = $num_rows;
	//---
	$recordset = $obj->consultar_videos_paginacion($offset,$limit,$actual);
	//die(json_encode($recordset));
	if($recordset!="error"){
		//--Armando cuepro de la consulta...
		$imagen_detalle_html.=  '<div class="col-lg-12 col-md-12 col-xs-12">';
		for($i=0;$i<count($recordset);$i++){
			$enlace_url=$recordset[$i][1];
			$material2=explode("=", $enlace_url);
			//die(json_encode($material2));
			$material="https://www.youtube.com/v/".$material2[1];
			$video_detalle_html.='<div class="col-lg-6  col-md-4 col-xs-12 col-sm-12">
										<object height="300px" width="80%" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								          <param name="movie" value="'.$material.'">
								          <param name="wmode" value="transparent">
								          <embed src="'.$material.'" type="application/x-shockwave-flash" wmode="transparent" height="260" width="100%">
								        </object>
								   </div>';
        } 
       	$video_detalle_html.='</div>';
		//--Cuerpo paginacion...
		$cuerpo_paginacion =new paginacion($idioma,$actual,$num_rows,$nom_fun,$limit);
		//--
		$arreglo["detalle_html"] = $video_detalle_html;
		$arreglo["cuerpo_paginacion"] = $cuerpo_paginacion->crear_paginacion();
	}else{
		$arreglo["error"] ="error";
	}
	die(json_encode($arreglo));	
}
//--
function  consultar_cuerpo_galeria_detalle($idioma,$offset,$limit,$actual){
	$arreglo = array();
	$obj = new galeriasModel();
	$imagen_cabecera_html = "";
	$imagen_detalle_html = "";
	//--Parametros de consulta...
	if($actual=="")$actual=0;
	$contador_interno=0;
	$nom_fun = "cargar_pagina_imagen";
	$num_rows = $obj->consultar_num_rows($idioma);
	//---
	$recordset = $obj->consultar_imagenes_paginacion($idioma,$offset,$limit,$actual);
	//die(json_encode($recordset));
	if($recordset!="error"){
		//--Armando cuepro de la consulta...
		$imagen_detalle_html.=  '<section id="gallery" style="">
									<div class="container-fluid">
            					 	<div class="">
                						<div class="row">
                    						<div class="gallery-items-wrapper col-lg-12">
                        						<div class="gallery-items" id="gallery-masonry"> 
                            						<div class="col-lg-12" style="display: flex; justify-content: center; align-items: center;">';
		for($i=0;$i<count($recordset);$i++){
			/*$arreglo[$i]["id"] = $recordset[$i][0];
			$arreglo[$i]["titulo"] = $recordset[$i][1];
			$arreglo[$i]["parrafo"] = $recordset[$i][2];
			$arreglo[$i]["imagen"] = $recordset[$i][3];*/
			if($i==0){
			//--Para el cuerpo de la imagen principal
				$imagen_cabecera_html.='<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
                        		    <div class=" col-lg-8 col-md-8 col-xs-12 col-sm-12 fadeInUp wow">
                            			<img src="site_media/img/galeria/'.$recordset[$i][2].'" class="imagen_centrada2" id="imagen_detalles" name="imagen_detalles">
                        			</div>
                        			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
			                        <div id="contenedor_imagenes_detalle" name="contenedor_imagenes_detalle" style="text-align:justify; padding-bottom: 93px;" class="col-lg-12 fadeInUp wow contenedor_imagenes_detalle">
			                        '.$recordset[$i][1].'
			                        </div>';
            }else{
            	//--Me quede aqui...
            //--Para el cuerpo de la imagen inferior
                $imagen_detalle_html.='<div class="single-item drinks col-lg-4 col-md-4 col-xs-12 col-md-12"> 
			                              	<figure class="gallery-image"> 
                                 				<img onclick="cargar_imagen_principal('.$i.','.$recordset[$i][0].','.$idioma.','.$offset.','.$limit.','.$actual.')" id="imagen_galeria'.$i.'" name="imagen_galeria'.$i.'" src="site_media/img/galeria/'.$recordset[$i][2].'" class="img-responsive" width="1200" height="800">
                                			</figure>
                            			</div>';           
                $contador_interno++;
                if($contador_interno==3){
                	$imagen_detalle_html.='</div><div class="col-lg-12" style="display: flex; justify-content: center; align-items: center;">';
                	$contador_interno=0;
                }            			     	
            }

		}
		$imagen_detalle_html.= "</div></div></div></div></div></div></section>";
		//--Cuerpo paginacion...
		$cuerpo_paginacion =new paginacion($idioma,$actual,$num_rows,$nom_fun,$limit);
		//--
		$arreglo["cabecera_html"] = $imagen_cabecera_html;
		$arreglo["detalle_html"] = $imagen_detalle_html;
		$arreglo["cuerpo_paginacion"] = $cuerpo_paginacion->crear_paginacion();
	}else{
		$arreglo["error"] ="error";
	}
	die(json_encode($arreglo));	
}
//--
//--Sobre carga del metodo de consulta de cuerpo detalle imagenes...
function consultar_imagen_principal($id,$idioma,$offset,$limit,$actual){
//-------------------------------------------------------------------------
	$arreglo = array();
	$obj = new galeriasModel();
	$imagen_cabecera_html = "";
	$imagen_detalle_html = "";
	//--Parametros de consulta...
	if($actual=="")$actual=0;
	$contador_interno=0;
	$nom_fun = "cargar_pagina_imagen";
	$num_rows = $obj->consultar_num_rows($idioma);
	$arreglo["cuantos_son"] = $num_rows;
	//---
	$recordset = $obj->consultar_imagenes_paginacion($idioma,$offset,$limit,$actual);
	//die(json_encode($recordset));
	if($recordset!="error"){
		//--Armando cuepro de la consulta...
		$imagen_detalle_html.=  '<section id="gallery" style="padding:6%;">
									<div class="container-fluid">
            					 	<div class="">
                						<div class="row">
                    						<div class="gallery-items-wrapper col-lg-12">
                        						<div class="gallery-items" id="gallery-masonry"> 
                            						<div class="col-lg-12" style="display: flex; justify-content: center; align-items: center;">';
		for($i=0;$i<count($recordset);$i++){
			/*$arreglo[$i]["id"] = $recordset[$i][0];
			$arreglo[$i]["titulo"] = $recordset[$i][1];
			$arreglo[$i]["parrafo"] = $recordset[$i][2];
			$arreglo[$i]["imagen"] = $recordset[$i][3];*/
			if($id==$recordset[$i][0]){
			//--Para el cuerpo de la imagen principal
				$imagen_cabecera_html.='<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
                        		    <div class=" col-lg-8 col-md-8 col-xs-12 col-sm-12 fadeInUp wow">
                            			<img src="site_media/img/galeria/'.$recordset[$i][2].'" class="imagen_centrada2" id="imagen_detalles" name="imagen_detalles">
                        			</div>
                        			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
			                        <div id="contenedor_imagenes_detalle" name="contenedor_imagenes_detalle" style="text-align:justify" class="col-lg-12 fadeInUp wow contenedor_imagenes_detalle">
			                        '.$recordset[$i][1].'
			                        </div>';
            }else{
            	//--Me quede aqui...
            //--Para el cuerpo de la imagen inferior
                $imagen_detalle_html.='<div class="single-item drinks "> 
			                              	<figure class="gallery-image"> 
                                 				<img onclick="cargar_imagen_principal('.$i.','.$recordset[$i][0].','.$idioma.','.$offset.','.$limit.','.$actual.')" id="imagen_galeria'.$i.'" name="imagen_galeria'.$i.'" src="site_media/img/galeria/'.$recordset[$i][2].'" class="img-responsive" width="1200" height="800">
                                			</figure>
                            			</div>';           
                $contador_interno++;
                if($contador_interno==3){
                	$imagen_detalle_html.='</div><div class="col-lg-12" style="display: flex; justify-content: center; align-items: center;">';
                	$contador_interno=0;
                }            			     	
            }

		}
		$imagen_detalle_html.= "</div></div></div></div></div></div></section>";
		//--Cuerpo paginacion...
		$cuerpo_paginacion =new paginacion($idioma,$actual,$num_rows,$nom_fun,$limit);
		//--
		$arreglo["cabecera_html"] = $imagen_cabecera_html;
		$arreglo["detalle_html"] = $imagen_detalle_html;
		$arreglo["cuerpo_paginacion"] = $cuerpo_paginacion->crear_paginacion();
	}else{
		$arreglo["error"] ="error";
	}
	die(json_encode($arreglo));
//-------------------------------------------------------------------------
}
//--
?>