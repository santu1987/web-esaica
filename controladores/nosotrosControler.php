<?php
require_once("../modelos/nosotrosModel.php");
//--Declaraciones
$mensajes = array();
$arreglo_datos = helper_userdata();
redireccionar_metodos($arreglo_datos);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'consultar':
			consultar_pagina_nosotros($arreglo_datos["idioma"]);
			break;
	}
}
//---
function helper_userdata(){
	$user_data = array();
	if($_POST){
		//--
		if(array_key_exists('idioma', $_POST)){
			$user_data['idioma'] = $_POST['idioma'];
		}
		if(array_key_exists('accion', $_POST)){
			$user_data["accion"] = $_POST["accion"];
		}	
		//--
	}
	return $user_data;
}
//---
function consultar_pagina_nosotros($idioma){
	$arreglo = array();
	$obj = new nosotrosModel();
	$recordset = $obj->consultar_nosotros($idioma);
	$arreglo["quienes_somos"] = $recordset[0][3];	
	$arreglo["mision"] = $recordset[0][4];	
	$arreglo["vision"] = $recordset[0][5];
	$arreglo["historia"] = $recordset[0][7];
	$arreglo["cv1"] = $recordset[0][8];
	$arreglo["cv2"] = $recordset[0][9];
	die(json_encode($arreglo));
}
?>