<?php
require_once("../modelos/contactoModel.php");
//--Declaraciones
$mensajes = array();
$arreglo_datos = helper_userdata();
redireccionar_metodos($arreglo_datos);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'consultar':
			consultar_pagina_contacto($arreglo_datos["idioma"]);
			break;
		case 'consultar_redes':
			consultar_redes($arreglo_datos["red"]);
			break;	
	}
}
//---
function helper_userdata(){
	$user_data = array();
	if($_POST){
		//--
		if(array_key_exists('idioma', $_POST)){
			$user_data['idioma'] = $_POST['idioma'];
		}
		if(array_key_exists('accion', $_POST)){
			$user_data["accion"] = $_POST["accion"];
		}
		if(array_key_exists('red', $_POST)){
			$user_data["red"] = $_POST["red"];
		}	
		//--
	}
	return $user_data;
}
//---
function consultar_pagina_contacto($idioma){
	$arreglo = array();
	$obj = new contactoModel();
	$recordset = $obj->consultar_contacto($idioma);
	$arreglo["direccion"] = $recordset[0][1];	
	$arreglo["telefono"] = $recordset[0][2];
	$arreglo["telefono2"] = $recordset[0][3];
	$arreglo["telefono3"] = $recordset[0][4];	
	$arreglo["email"] = $recordset[0][5];
	$arreglo["horario"] = $recordset[0][6];
	die(json_encode($arreglo));
}
//--Metodo que consulta las redes sociales 
function consultar_redes($red){
	$arreglo = array();
	$obj = new contactoModel();
	$recordset = $obj->consultar_redes($red);
	$arreglo["instagram"] = $recordset[0][0];
	die(json_encode($arreglo));
}
?>