<?php
require_once("../modelos/inicioModel.php");
require_once("../modelos/correoModel.php");
//--Declaraciones
$mensajes =  array();
$arreglo_datos = helper_userdata();
redireccionar_metodos($arreglo_datos);
//---
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'consultar':
			consultar_pagina_inicio($arreglo_datos["idioma"]);
			break;
		case 'guardar_contacto':
			guardar_contacto($arreglo_datos);
			break;
		case 'enviar_correo':
			enviar_correo($arreglo_datos);
			break;
	}
}
//---
function helper_userdata(){
	$user_data = array();
	if($_POST){
		//--
		if(array_key_exists('idioma', $_POST)){
			$user_data['idioma'] = $_POST['idioma'];
		}
		if(array_key_exists('accion', $_POST)){
			$user_data["accion"] = $_POST["accion"];
		}
		//--Guardar Contactos
		if(array_key_exists('nombres_contacto', $_POST)){
			$user_data["nombres_contacto"] = $_POST["nombres_contacto"];
		}
		if(array_key_exists('apellidos_contacto', $_POST)){
			$user_data["apellidos_contacto"] = $_POST["apellidos_contacto"];
		}
		if(array_key_exists('telefonos_contacto', $_POST)){
			$user_data["telefonos_contacto"] = $_POST["telefonos_contacto"];
		}
		if(array_key_exists('correo_contacto', $_POST)){
			$user_data["correo_contacto"] = $_POST["correo_contacto"];
		}	
		if(array_key_exists('pais_contacto', $_POST)){
			$user_data["pais_contacto"] = $_POST["pais_contacto"];
		}
		if(array_key_exists('mensaje_contacto', $_POST)){
			$user_data["contenido_contacto"] = $_POST["mensaje_contacto"];
		}
	}
	return $user_data;
}
//---
function consultar_pagina_inicio($idioma){
	$arreglo = array();
	$obj = new inicioModel();
	$recordset = $obj->consultar_inicio($idioma);
	$arreglo["quienes_somos"] = $recordset[0][3];	
	$arreglo["mision"] = $recordset[0][4];	
	$arreglo["vision"] = $recordset[0][5];
	$arreglo["producto"] = $recordset[0][6];
	die(json_encode($arreglo));
}
//--
function guardar_contacto($arreglo_data){
	$arreglo_retorno = array();
	$obj = new inicioModel();
	$recordset = $obj->guardar_contacto_inicio($arreglo_data);
	if($recordset== true){
		$arreglo_retorno["mensaje"] = 1;
	}else
	{
		$arreglo_retorno["mensaje"] = 0;
	}
	die(json_encode($arreglo_retorno));
} 
//--
function enviar_correo($arreglo_datos){
	$correo_sistema = "esaica.cacao.vzla@gmail.com";//aquí va el correo del sistema
	$correo_destino = "gianni.d.santucci@gmail.com";
	$titulo = "Datos de contacto";
	$encabezado = $arreglo_datos['nombres_contacto']."  ".$arreglo_datos['apellidos_contacto']."  realizó el registro de sus datos en www.esaica.com:<br>";
	$result = enviar_correo_contacto($correo_sistema,$correo_destino,$titulo,$encabezado,$arreglo_datos);
	$arreglo_retorno["mensaje"] = $result;
	die(json_encode($arreglo_retorno));
}
//--
function enviar_correo_contacto($correo_sistema,$correo_destino,$titulo,$encabezado,$arreglo_datos){
	$obj = new correoModel();
	$mensaje = $obj->crear_cuadro($titulo,$encabezado,$arreglo_datos);
	$resp = $obj->enviar_correo($correo_sistema,$correo_destino,$mensaje,$titulo);
	return $resp;
} 
?>