<?php
require_once("../modelos/noticiasModel.php");
require_once("../core/helpers/paginacion.php");
//--Declaraciones
$mensajes = array();
$arreglo_datos = helper_userdata();
redireccionar_metodos($arreglo_datos);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'consultar_cuerpo_noticias':
			consultar_cuerpo_noticias($arreglo_datos["idioma"],$arreglo_datos["offset"],$arreglo_datos["limit"],$arreglo_datos["actual"]);
			break;
		case 'consultar_noticias_detalles':
			consultar_noticias_detalles($arreglo_datos["idioma"],$arreglo_datos["offset"],$arreglo_datos["limit"],$arreglo_datos["actual"]);
			break;
		case 'consultar_leer_mas':
			consultar_leer_mas($arreglo_datos["id"],$arreglo_datos["idioma"],$arreglo_datos["offset"],$arreglo_datos["limit"],$arreglo_datos["actual"]);
			break;		
	}
}
//---
function helper_userdata(){
	$user_data = array();
	if($_POST){
		//--
		if(array_key_exists('idioma', $_POST)){
			$user_data['idioma'] = $_POST['idioma'];
		}
		if(array_key_exists('accion', $_POST)){
			$user_data["accion"] = $_POST["accion"];
		}
		if(array_key_exists('offset', $_POST)){
			$user_data["offset"] = $_POST["offset"];
		}
		if(array_key_exists('limit', $_POST)){
			$user_data["limit"] = $_POST["limit"];
		}
		if(array_key_exists('actual', $_POST)){
			$user_data["actual"] = $_POST["actual"];
		}
		if(array_key_exists('id', $_POST)){
			$user_data["id"] = $_POST["id"];
		}
		//--
	}
	return $user_data;
}
//---
function consultar_cuerpo_noticias($idioma,$offset,$limit,$actual){
	$arreglo = array();
	$obj = new noticiasModel();
	$recordset = $obj->consultar_noticias($idioma,$offset,$limit,$actual);
	if($recordset!="error"){
		for($i=0;$i<count($recordset);$i++){
			$arreglo[$i]["id"] = $recordset[$i][0];
			$arreglo[$i]["titulo"] = $recordset[$i][1];
			$arreglo[$i]["parrafo"] = substr($recordset[$i][2],0,200)." ...";
			$arreglo[$i]["imagen"] = $recordset[$i][3];
		}
	}else{
		$arreglo[0]["error"] ="error";
	}
	die(json_encode($arreglo));
}
//--
function  consultar_noticias_detalles($idioma,$offset,$limit,$actual){
	$arreglo = array();
	$obj = new noticiasModel();
	$noticia_cabecera_html = "";
	$noticia_detalle_html = "";
	//--Parametros de consulta...
	if($actual=="")$actual=0;
	$nom_fun = "consultar_cuerpo_noticias";
	$num_rows = $obj->consultar_num_rows($idioma);
	//---
	$recordset = $obj->consultar_noticias($idioma,$offset,$limit,$actual);
	//die(json_encode($recordset));
	if($recordset!="error"){
		//--Armando cuepro de la consulta...
		$noticia_detalle_html.=  '<div class="container wow fadeIn estructura_noticias">';
		for($i=0;$i<count($recordset);$i++){
			/*$arreglo[$i]["id"] = $recordset[$i][0];
			$arreglo[$i]["titulo"] = $recordset[$i][1];
			$arreglo[$i]["parrafo"] = $recordset[$i][2];
			$arreglo[$i]["imagen"] = $recordset[$i][3];*/
			if($i==0){
			//--Para el cuerpo de la noticia principal
				$noticia_cabecera_html.='<div class="fadeInDown wow">
                            	   	<h2 class="section-heading"><div id="titulo_noticias_detalles" name="titulo_noticias_detalles">'.$recordset[$i][1].'</div></h2>
                        		    </div>
                        		    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
                        		    <div class=" col-lg-8 col-md-8 col-xs-12 col-sm-12 fadeInUp wow">
                            			<img src="site_media/img/noticias/'.$recordset[$i][3].'" class="imagen_centrada2" id="imagen_noticias_detalles" name="imagen_noticias_detalles">
                        			</div>
                        			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
			                        <div id="contenedor_noticias_detalle" name="contenedor_noticias_detalle" style="text-align:justify" class="col-lg-12 fadeInUp wow contenedor_noticias_detalle">
			                        '.$recordset[$i][2].'
			                        </div>
			                        <input type="hidden" name="id_noticia_detalle" id="id_noticia_detalle" value="'.$recordset[$i][0].'">';
            }else{
            //--Para el cuerpo de la noticia inferior
                $noticia_detalle_html.= '<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 cuerpo_noticia">
	                            			<img id="imagen_noticias'.$i.'" name="imagen_noticias'.$i.'" src="site_media/img/noticias/'.$recordset[$i][3].'" class="imagen_centrada">
				                            <div class="titulos_noticias" id="titulo_noticias'.$i.'" name="titulo_noticias'.$i.'">'.$recordset[$i][1].'</div>
				                            <div class="parrafos_imagen left">
				                                <p style="text-align:justify">
				                                    <div id="parrafo_noticias'.$i.'"" name="parrafo_noticias'.$i.'">
				                                  	'.substr($recordset[$i][2],0,200).'... 
				                                    </div>
				                                </p>
				                            </div>
				                            <div class="left div_btn_noticias">
				                                <div class="boton_noticias ocultar"id="btn_noticias'.$i.'" name="btn_noticias'.$i.'" onclick="leer_mas('.$recordset[$i][0].','.$idioma.','.$offset.','.$limit.','.$actual.')" >LEER MAS</div>
				                            </div>
				                            <input type="hidden" name="id_noticia" id="id_noticia" value="'.$recordset[$i][0].'">
				                            <div class="clear"></div>
			                    		</div>';                	
            }

		}
		$noticia_detalle_html.= "</div>";
		//--Cuerpo paginacion...
		$limit = 4;
		$cuerpo_paginacion =new paginacion($idioma,$actual,$num_rows,$nom_fun,$limit);
		//--
		$arreglo["cabecera_html"] = $noticia_cabecera_html;
		$arreglo["detalle_html"] = $noticia_detalle_html;
		$arreglo["cuerpo_paginacion"] = $cuerpo_paginacion->crear_paginacion();
	}else{
		$arreglo["error"] ="error";
	}
	die(json_encode($arreglo));	
}
//--Sobre carga de consultar_noticias_detalles....
function  consultar_leer_mas($id,$idioma,$offset,$limit,$actual){
	$arreglo = array();
	$obj = new noticiasModel();
	$noticia_cabecera_html = "";
	$noticia_detalle_html = "";
	//--Parametros de consulta...
	if($actual=="")$actual=0;
	$nom_fun = "consultar_cuerpo_noticias";
	$num_rows = $obj->consultar_num_rows($idioma);
	//---
	$recordset = $obj->consultar_noticias($idioma,$offset,$limit,$actual);
	//die(json_encode($recordset));
	if($recordset!="error"){
		//--Armando cuepro de la consulta...
		$noticia_detalle_html.=  '<div class="container wow fadeIn estructura_noticias">';
		for($i=0;$i<count($recordset);$i++){
			/*$arreglo[$i]["id"] = $recordset[$i][0];
			$arreglo[$i]["titulo"] = $recordset[$i][1];
			$arreglo[$i]["parrafo"] = $recordset[$i][2];
			$arreglo[$i]["imagen"] = $recordset[$i][3];*/
			if($id==$recordset[$i][0]){
			//--Para el cuerpo de la noticia principal
				$noticia_cabecera_html.='<div class="fadeInDown wow">
                            	   	<h2 class="section-heading"><div id="titulo_noticias_detalles" name="titulo_noticias_detalles">'.$recordset[$i][1].'</div></h2>
                        		    </div>
                        		    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
                        		    <div class=" col-lg-8 col-md-8 col-xs-12 col-sm-12 fadeInUp wow">
                            			<img src="site_media/img/noticias/'.$recordset[$i][3].'" class="imagen_centrada2" id="imagen_noticias_detalles" name="imagen_noticias_detalles">
                        			</div>
                        			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
			                        <div id="contenedor_noticias_detalle" name="contenedor_noticias_detalle" style="text-align:justify" class="col-lg-12 fadeInUp wow contenedor_noticias_detalle">
			                        '.$recordset[$i][2].'
			                        </div>
			                        <input type="hidden" name="id_noticia_detalle" id="id_noticia_detalle" value="'.$recordset[$i][0].'">';
            }else{
            //--Para el cuerpo de la noticia inferior
                $noticia_detalle_html.= '<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 cuerpo_noticia">
	                            			<img id="imagen_noticias'.$i.'" name="imagen_noticias'.$i.'" src="site_media/img/noticias/'.$recordset[$i][3].'" class="imagen_centrada">
				                            <div class="titulos_noticias" id="titulo_noticias'.$i.'" name="titulo_noticias'.$i.'">'.$recordset[$i][1].'</div>
				                            <div class="parrafos_imagen left">
				                                <p style="text-align:justify">
				                                    <div id="parrafo_noticias'.$i.'"" name="parrafo_noticias'.$i.'">
				                                  	'.substr($recordset[$i][2],0,300).'... 
				                                    </div>
				                                </p>
				                            </div>
				                            <div class="left div_btn_noticias">
				                                <div class="boton_noticias ocultar"id="btn_noticias'.$i.'" name="btn_noticias'.$i.'" onclick="leer_mas('.$recordset[$i][0].','.$idioma.','.$offset.','.$limit.','.$actual.')" >LEER MAS</div>
				                            </div>
				                            <input type="hidden" name="id_noticia" id="id_noticia" value="'.$recordset[$i][0].'">
				                            <div class="clear"></div>
			                    		</div>';                	
            }

		}
		$noticia_detalle_html.= "</div>";
		//--Cuerpo paginacion...
		$limit = 4;
		$cuerpo_paginacion =new paginacion($idioma,$actual,$num_rows,$nom_fun,$limit);
		//--
		$arreglo["cabecera_html"] = $noticia_cabecera_html;
		$arreglo["detalle_html"] = $noticia_detalle_html;
		$arreglo["cuerpo_paginacion"] = $cuerpo_paginacion->crear_paginacion();
	}else{
		$arreglo["error"] ="error";
	}
	die(json_encode($arreglo));	
}

//--
?>