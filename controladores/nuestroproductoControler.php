<?php
require_once("../modelos/nuestroproductoModel.php");
//--Declaraciones
$mensajes = array();
$arreglo_datos = helper_userdata();
redireccionar_metodos($arreglo_datos);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'consultar':
			consultar_pagina_nuestroproducto($arreglo_datos["idioma"]);
			break;
	}
}
//---
function helper_userdata(){
	$user_data = array();
	if($_POST){
		//--
		if(array_key_exists('idioma', $_POST)){
			$user_data['idioma'] = $_POST['idioma'];
		}
		if(array_key_exists('accion', $_POST)){
			$user_data["accion"] = $_POST["accion"];
		}	
		//--
	}
	return $user_data;
}
//---
function consultar_pagina_nuestroproducto($idioma){
	$arreglo = array();
	$obj = new nuestroproductoModel();
	$recordset = $obj->consultar_nuestroproducto($idioma);
	$arreglo["producto"] = $recordset[0][6];
	die(json_encode($arreglo));
}
?>