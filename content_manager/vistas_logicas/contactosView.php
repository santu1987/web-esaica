<?php
require("../core/fbasic.php");
//--Armo diccionario 
function render_list($html,$data){
//---------------------------------------------------------------------------------------
	$match_cal = set_match_identificador_dinamico($html,"<!--row_contactos-->");
	if($data!="error"){
		for($i=0;$i<count($data);$i++){
			$dic = array(
									"{i}" 	=> $i,
									"{nombres_apellidos}" 	=> $data[$i][1]." ".$data[$i][2],
									"{tab}" => "tab".$i,
									"{telefonos}"	=> $data[$i][3],
									"{correo}"	=>  $data[$i][4],
									"{pais}"	=>  $data[$i][5],
									"{consultar_contacto}"=>'consultar_contacto('.$i.');',
									"{data}"=>$data[$i][0]."|".$data[$i][1]."|".$data[$i][2]."|".$data[$i][3]."|".$data[$i][4]."|".$data[$i][5]."|".$data[$i][6]
								);
			$render.=str_replace(array_keys($dic), array_values($dic), $match_cal);
		}
	}
	$html = str_replace($match_cal, $render, $html);
	return $html;
//----------------------------------------------------------------------------------------	
}
//--Para renderización estática
/*function render_estaticos($html,$data){
	foreach ($data as $clave => $valor) {
		$html = str_replace('{'.$clave.'}', $valor, $html);
	}
	return $html;
}*/
//----------------------------------------------------------------------------------------
//--Para renderizacion dinamica
function render_vista_consulta($html,$data){
	$template=get_template($html);
	$html = render_list($template,$data);//--renderización dinamica
	//$html = render_estaticos($html,$arr_pag);//--renderización estatica
	print $html;
	//die("aaa");
	//die($html);
}
//----------------------------------------------------------------------------------------
?>