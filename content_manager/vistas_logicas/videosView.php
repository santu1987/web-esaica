
<?php
require("../core/fbasic.php");
//--Armo diccionario 
function render_list($html,$data){
//---------------------------------------------------------------------------------------

	$match_cal = set_match_identificador_dinamico($html,"<!--row_videos-->");
	if($data!="error"){
		//--
		for($i=0;$i<count($data);$i++){
			//--Armando botones:
			if($data[$i][2]==0){
				$btn_activar = "<div title='Publicar video' class='btn btn-success' id='btn_estatus_".$data[$i][0]."' name='btn_estatus_".$data[$i][0]."' onclick='publicar_video(".$data[$i][0].")'><i id='icono_estatus_".$data[$i][0]."' class='fa fa-cloud-upload' aria-hidden='true' ></i></div>";
				$estatus ="<div id='div_estatus_".$data[$i][0]."'>Inactivo</div>";
			}else if($data[$i][2]==1){
				$btn_activar = "<div title='Inactivar video' class='btn btn-warning' id='btn_estatus_".$data[$i][0]."' name='btn_estatus_".$data[$i][0]."' onclick='publicar_video(".$data[$i][0].")'><i id='icono_estatus_".$data[$i][0]."' class='fa fa-cloud-download' aria-hidden='true' ></i></div>";
				$estatus ="<div id='div_estatus_".$data[$i][0]."'>Publicado</div>";
			}
			$btn_consultar = "<div title='Consultar video' class='btn btn-primary' id='btn_consultar_video' name='btn_consultar_video' onclick='consultar_video(".$data[$i][0].");'><i  class='fa fa-search' aria-hidden='true'></i></div>";
			$operacion = $btn_consultar." ".$btn_activar;
			$dic = array(
									"{i}" 	=> $i,
									"{id}" 	=> $data[$i][0],
									"{url}" => $data[$i][1],
									"{estatus}" => $estatus,
									"{tab}" => "tab".$data[$i][0],
									"{consultar_video}"=>'consultar_video('.$data[$i][0].');',
									"{data}"=>$data[$i][0]."|".$data[$i][1]."|".$data[$i][2],
									"{operacion}"=>$operacion
						);
			$render.=str_replace(array_keys($dic), array_values($dic), $match_cal);
		}
	}
	$html = str_replace($match_cal, $render, $html);
	return $html;
//----------------------------------------------------------------------------------------	
}
//--Para renderización estática
/*function render_estaticos($html,$data){
	foreach ($data as $clave => $valor) {
		$html = str_replace('{'.$clave.'}', $valor, $html);
	}
	return $html;
}*/
//----------------------------------------------------------------------------------------
//--Para renderizacion dinamica
function render_vista_consulta($html,$data){
	$template=get_template($html);
	$html = render_list($template,$data);//--renderización dinamica
	//$html = render_estaticos($html,$arr_pag);//--renderización estatica
	print $html;
	//die("aaa");
	//die($html);
}
//----------------------------------------------------------------------------------------
?>