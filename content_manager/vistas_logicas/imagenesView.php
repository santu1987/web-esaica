
<?php
require("../core/fbasic.php");
//--Armo diccionario 
function render_list($html,$data){
//---------------------------------------------------------------------------------------

	$match_cal = set_match_identificador_dinamico($html,"<!--row_noticias-->");
	if($data!="error"){
		//--
		for($i=0;$i<count($data);$i++){
			//--Armando botones:
			if($data[$i][5]==0){
				$btn_activar = "<div title='Publicar imagen' class='btn btn-success' id='btn_estatus_".$data[$i][0]."' name='btn_estatus_".$data[$i][0]."' onclick='publicar_imagen(".$data[$i][0].")'><i id='icono_estatus_".$data[$i][0]."' class='fa fa-cloud-upload' aria-hidden='true' ></i></div>";
				$estatus ="<div id='div_estatus_".$data[$i][0]."'>Inactivo</div>";
			}else if($data[$i][5]==1){
				$btn_activar = "<div title='Inactivar imagen' class='btn btn-warning' id='btn_estatus_".$data[$i][0]."' name='btn_estatus_".$data[$i][0]."' onclick='publicar_imagen(".$data[$i][0].")'><i id='icono_estatus_".$data[$i][0]."' class='fa fa-cloud-download' aria-hidden='true' ></i></div>";
				$estatus ="<div id='div_estatus_".$data[$i][0]."'>Publicado</div>";
			}
			$btn_consultar = "<div title='Consultar imagen' class='btn btn-primary' id='btn_consultar_imagen' name='btn_consultar_imagen' onclick='consultar_imagen(".$data[$i][0].");'><i  class='fa fa-search' aria-hidden='true'></i></div>";
			$operacion = $btn_consultar." ".$btn_activar;
			$dic = array(
									"{i}" 	=> $i,
									"{id}" 	=> $data[$i][0],
									"{id_idioma}"	=>  $data[$i][1],
									"{idioma}" => $data[$i][2],
									"{contenido}" => $data[$i][3],
									"{archivo}" => $data[$i][4],
									"{estatus}" => $data[$i][5],
									"{tab}" => "tab".$data[$i][0],
									"{consultar_imagen}"=>'consultar_imagen('.$data[$i][0].');',
									"{data}"=>$data[$i][0]."|".$data[$i][1]."|".$data[$i][2]."|".$data[$i][3]."|".$data[$i][4]."|".$data[$i][5],
									"{operacion}"=>$operacion,
									"{estatus}"=>$estatus
						);
			$render.=str_replace(array_keys($dic), array_values($dic), $match_cal);
		}
	}
	$html = str_replace($match_cal, $render, $html);
	return $html;
//----------------------------------------------------------------------------------------	
}
//--Para renderización estática
/*function render_estaticos($html,$data){
	foreach ($data as $clave => $valor) {
		$html = str_replace('{'.$clave.'}', $valor, $html);
	}
	return $html;
}*/
//----------------------------------------------------------------------------------------
//--Para renderizacion dinamica
function render_vista_consulta($html,$data){
	$template=get_template($html);
	$html = render_list($template,$data);//--renderización dinamica
	//$html = render_estaticos($html,$arr_pag);//--renderización estatica
	print $html;
	//die("aaa");
	//die($html);
}
//----------------------------------------------------------------------------------------
?>