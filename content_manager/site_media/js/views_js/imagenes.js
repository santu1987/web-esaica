//---Bloque de Eventos
//-----------------------------------------------------
/*    "use strict";

    // Init Theme Core    
    Core.init();

    // Init Demo JS  
    Demo.init();
*/
//-----------------------------------------------------
cargar_select_imagen();
$("#btn_limpiar_imagen").click(function(e){
	e.preventDefault();
	limpiar_imagen();
});
$("#btn_ver_listado_imagen").click(function(e){
	e.preventDefault();
	cargar_lista_imagen();
	//Cargar datos tabla
	setTimeout(function(){
		quitar_preloader();
	},2000);
});
$("#btn_guardar_imagen").click(function(){
	var id_imagen = $("#id_imagen").val();
	var idioma = $("#select_idioma_imagen").val();
	var contenido = $("#text_imagen").val();
	var accion = "guardar_imagen";
	var data = {
					'accion':accion,
					'id_imagen':id_imagen,
					'contenido':contenido,
					'idioma':idioma
	}
	mensaje_preloader2("#campo_mensaje","Espere unos segundos mientras se realiza el proceso...");
	if(validar_formulario_imagen()==true){
		$.ajax({
					url:'./controladores/imagenesController.php',
					type:'POST',
					cache:false,
					data:data,
					error: function(resp){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
					},
					success: function(resp){
						var recordset = $.parseJSON(resp);
						if(recordset[0]=='1'){
							//--------------------------------------------
							$("#id_imagen").val(recordset[1]);
							if($("#file_imagen").val()!=""){
								cargar_archivo_imagen("registro_exitoso");	
							}else{
								mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Registro exitoso", "alert-success");
								limpiar_imagen();
							}
							//-------------------------------------------
						}else
						if(recordset[0]=='2'){
							mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error en proceso de registro", "alert-danger");
						}else
						if(recordset[0]=='3'){
							//-------------------------------------------
							if($("#file_imagen").val()!=""){
								cargar_archivo_imagen("actualizacion_exitosa");
							}else{
								mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Actualizaci&oacute;n exitoso", "alert-success");
								limpiar_imagen();
							}
							//--------------------------------------------	
						}else
						if(recordset[0]=='4'){
							mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error en proceso de actualizaci&oacute;n", "alert-danger");
						}else
						if(recordset[0]=='-3'){
							mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> No existe el registro", "alert-danger");
						}
					}
		});
	}
});
$("#file_imagen").change(function(){
	mostrarImagen(this);
});
//--Bloque de Funciones
function cargar_lista_imagen(){
	var accion = "consultar_listado_imagen";
	var data = {'accion':accion};
	$.ajax({
				url:'./controladores/imagenesController.php',
				type:'POST',
				cache:false,
				data:data,
				error:function(resp){
					console.log(resp);
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
				},
				success:function(resp){
					if(resp=="error"){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
					}else{
						$("#cuerpo_principal").html(resp);
						iniciar_datatable();
						mensaje_preloader("#campo_mensaje_lista");
					}
				}
	});
}
//--
function validar_formulario_imagen(){
	
	if($("#text_imagen").val()==""){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe ingresar el texto relacionado a la imagen", "alert-danger");
		return false;
	}else
	if($("#select_idioma_imagen").val()=="0"){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe seleccionar idioma", "alert-danger");
		return false;
	}else
	if($("#img_destino").attr("src")=="./site_media/img/gallery.png"){
		if($("#file_imagen").val()==""){
			mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe seleccionar una imagen", "alert-danger");
			return false;
		}
	}	
	else{
		return true;
	}
}
//--
function mostrarImagen(input) {
	if (input.files && input.files[0]) {
	  var reader = new FileReader();
	  reader.onload = function (e) {
	   $('#img_destino').attr('src', e.target.result);
	  }
	  reader.readAsDataURL(input.files[0]);
	}
}
//--
function cargar_select_imagen(){
	var accion = "consultar_select_idiomas";
	var data = {
					'accion':accion
	};
	$.ajax({
				url:'./controladores/empresaController.php',
				type:'POST',
				cache:false,
				data:data,
				error:function(resp){
					console.log(resp);
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					$("#select_idioma_imagen").html(recordset["opciones"]);
				}
	});
}
//--
function cargar_contenido_imagen(idioma){
	var accion = "consultar_contenido_noticia";
	var data = {
					'accion':accion,
					'idioma':idioma
	}
	$.ajax({
				url:'./controladores/noticiasController.php',
				type:'POST',
				cache:false,
				data:data,
				error:function(resp){
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute;o un error inesperado", "alert-danger");
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					//alert(toMarkdown(recordset["contenido"]));
					if(recordset["contenido"]!=null){
						$("#md-footer").text(recordset["contenido"]);
						$("#markdown-editor").val(toMarkdown(recordset["contenido"]));
						$('#md-footer').show().html(recordset["contenido"]);
						$("#id_noticia").val(recordset["id"]);
					}else{
						$("#md-footer").text("");
						$("#markdown-editor,#id_noticia").val("");
					}
				}
	});
}
//--
function limpiar_imagen(){
	$("#id_imagen,#text_imagen,#file_imagen").val("");
	$("#select_idioma_imagen").val("0");
	$("#btn_guardar_imagen").removeClass("disabled");
	$("#img_destino").attr("src","./site_media/img/gallery.png");
}
//--
function cargar_archivo_imagen(valor){
      var formData = new FormData($("#form-ui")[0]);
      var message = "";   
      //hacemos la petición ajax  
      $.ajax({
                url:'./controladores/imagenesController.php',  
                type: 'POST',
                // Form data
                //datos del formulario
                data: formData,
                //necesario para subir archivos via ajax
                cache: false,
                contentType: false,
                processData: false,
                //mientras enviamos el archivo
                beforeSend: function(){
                    //message = $("<span class='before'>Subiendo la imagen, por favor espere...</span>");
                   // showMessage(message)         
                },
                //si ha ocurrido un error
                error: function()
                {
                    console.log(arguments);
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");                    
                },
                //una vez finalizado correctamente
  				success: function(data)
                {
                    var recordset=$.parseJSON(data);
                    if((valor=="registro_exitoso")&&(recordset[0]=="archivo_cargado"))
                    {
			           mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Registro exitoso", "alert-success");
                       limpiar_imagen();
                    }else
                    if((valor=="actualizacion_exitosa")&&(recordset[0]=="archivo_cargado"))
                    {
					   mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Actualizaci&oacute;n exitosa", "alert-success");
			       	   limpiar_imagen();	
			       	}else
                    if(recordset[0]=="error_tipo_archivo")
                    {
                       	mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error- Carga imag&eacute;n: Solo puede subir imagenes en formato jpg, de tama&ntilde;o menor a 2 megas", "alert-danger");
                    }else
                    if(recordset[0]=="error_no_carga")
                    {
   						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error- Carga imag&eacute;n", "alert-danger");
                    }else
                    if(recordset[0]=="id_blanco")
                    {
   						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error- No se puede relacionar el registro con la imag&eacute;n", "alert-danger");
                    }else
                    if(recordset[0]=="no_existe_registro")
                    {
   						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error- No se puede relacionar el registro con la imag&eacute;n", "alert-danger");
                    }else{
                    	alert("ESTE ES EL recordset:"+recordset);
                    }  
                }

      });
}