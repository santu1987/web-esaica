$("#btn_nuevo_imagen").click(function(){
	$("#cuerpo_principal").load("site_media/templates/imagenes.html");
});
//--Bloque de funciones
function consultar_imagen(numero){
	var datos_noticias = $("#tab"+numero).attr("data");
	var datos = datos_noticias.split("|");
	$("#cuerpo_principal").load("./site_media/templates/imagenes.html");
	if($("#btn_estatus_"+numero).hasClass("btn-warning")){
		valor_estatus="inactivo";
	}else if($("#btn_estatus_"+numero).hasClass("btn-success")){
		valor_estatus="activo";
	}		
	setTimeout(function(){
		$("#id_imagen").val(datos[0]);
		$("#text_imagen").val(datos[3]);
		$("#select_idioma_imagen").val(datos[1]);
		if(valor_estatus=='inactivo'){
			$("#btn_guardar_imagen").addClass("disabled");
		}else if(valor_estatus=='activo'){
			$("#btn_guardar_imagen").removeClass("disabled");
		}
		$("#img_destino").attr("src","../site_media/img/galeria/"+datos[4]);
	},1000);
}
//--
function publicar_imagen(numero){
	var datos_noticias = $("#tab"+numero).attr("data");
	var datos = datos_noticias.split("|");
	var id_imagen = datos[0];
	var accion = 'publicar_imagen';
	var data = {
					'id_imagen':id_imagen,
					'accion':accion};
	$.ajax({
				url:'./controladores/imagenesController.php',
				type:'POST',
				data:data,
				cache:false,
				error:function(resp){
					console.log(resp);
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
				},	
				success:function(resp){
					var recordset = $.parseJSON(resp);
					if(recordset[0]==1){
						if(recordset[1]=="activar"){
							$("#icono_estatus_"+numero).removeClass("fa-cloud-upload").addClass("fa-cloud-download");
							$("#btn_estatus_"+numero).removeClass("btn-success").addClass("btn-warning");
							$("#btn_estatus_"+numero).attr({'title':'Inactivar publicación'});
							$("#div_estatus_"+numero).text("Publicado");
							mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Imagen publicada", "alert-success");
						}else{
							$("#icono_estatus_"+numero).removeClass("fa-cloud-download").addClass("fa-cloud-upload");
							$("#btn_estatus_"+numero).removeClass("btn-warning").addClass("btn-success");
							$("#btn_estatus_"+numero).attr({'title':'Publicar noticia'});
							$("#div_estatus_"+numero).text("Inactivo");
							mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Imagen inactiva", "alert-success");
						}
						
					}else if(recordset[0]==0){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
					}else if(recordset[0]==-1){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error en consulta de estatus ", "alert-danger");
					}
				}
	});				
}
//--