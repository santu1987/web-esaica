$("#btn_nuevo_noticia").click(function(){
	$("#cuerpo_principal").load("site_media/templates/noticias.html");
});
//--Bloque de funciones
function consultar_noticia(numero){
	var datos_noticias = $("#tab"+numero).attr("data");
	var datos = datos_noticias.split("|");
	$("#cuerpo_principal").load("./site_media/templates/noticias.html");
	if($("#btn_estatus_"+numero).hasClass("btn-warning")){
		valor_estatus="inactivo";
	}else if($("#btn_estatus_"+numero).hasClass("btn-success")){
		valor_estatus="activo";
	}	
	setTimeout(function(){
		$("#id_noticia").val(datos[0]);
		$("#select_idioma_noticia > option[value="+datos[1]+"]").attr('selected','selected');
		$("#text_titulo_noticias").val(datos[3]);		
		$('#md-footer').show().html(datos[4]);
		$("#markdown-editor").val(toMarkdown(datos[4]));
		if(valor_estatus=='inactivo'){
			$("#btn_guardar_noticia").addClass("disabled");
		}else if(valor_estatus=='activo'){
			$("#btn_guardar_noticia").removeClass("disabled");
		}
		if (datos[5]!=""){
			$("#img_destino_noticias").attr("src","../site_media/img/noticias/"+datos[5]);
		}
	},2000);
}
//--
function publicar_noticia(numero){
	var datos_noticias = $("#tab"+numero).attr("data");
	var datos = datos_noticias.split("|");
	var id_noticia = datos[0];
	var accion = 'publicar_noticias';
	var data = {
					'id_noticia':id_noticia,
					'accion':accion};
	$.ajax({
				url:'./controladores/noticiasController.php',
				type:'POST',
				data:data,
				cache:false,
				error:function(resp){
					console.log(resp);
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
				},	
				success:function(resp){
					var recordset = $.parseJSON(resp);
					if(recordset[0]==1){
						if(recordset[1]=="activar"){
							$("#icono_estatus_"+numero).removeClass("fa-cloud-upload").addClass("fa-cloud-download");
							$("#btn_estatus_"+numero).removeClass("btn-success").addClass("btn-warning");
							$("#btn_estatus_"+numero).attr({'title':'Inactivar publicación'});
							$("#div_estatus_"+numero).text("Publicado");
							mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Noticia publicada", "alert-success");
						}else{
							$("#icono_estatus_"+numero).removeClass("fa-cloud-download").addClass("fa-cloud-upload");
							$("#btn_estatus_"+numero).removeClass("btn-warning").addClass("btn-success");
							$("#btn_estatus_"+numero).attr({'title':'Publicar noticia'});
							$("#div_estatus_"+numero).text("Inactivo");
							mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Noticia inactiva", "alert-success");
						}
						
					}else if(recordset[0]==0){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
					}else if(recordset[0]==-1){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error en consulta de estatus ", "alert-danger");
					}
				}
	});				
}
//--