$("#btn_nuevo_video").click(function(){
	$("#cuerpo_principal").load("site_media/templates/videos.html");
});
//--Bloque de funciones
function consultar_video(numero){
	var datos_videos = $("#tab"+numero).attr("data");
	var datos = datos_videos.split("|");
	$("#cuerpo_principal").load("./site_media/templates/videos.html");
	if($("#btn_estatus_"+numero).hasClass("btn-warning")){
		valor_estatus="inactivo";
	}else if($("#btn_estatus_"+numero).hasClass("btn-success")){
		valor_estatus="activo";
	}		
	setTimeout(function(){
		$("#id_video").val(datos[0]);
		$("#text_url_video").val(datos[1]);
		cargar_enlace();
		$("#preview_material_multimedia").removeClass("ocultar").addClass("mostrar");
		if(valor_estatus=='inactivo'){
			$("#btn_guardar_video").addClass("disabled");
		}else if(valor_estatus=='activo'){
			$("#btn_guardar_video").removeClass("disabled");
		}
	},1000);
}
//--
function publicar_video(numero){
	var datos_video = $("#tab"+numero).attr("data");
	var datos = datos_video.split("|");
	var id_video = datos[0];
	var accion = 'publicar_video';
	var data = {
					'id_video':id_video,
					'accion':accion};
	$.ajax({
				url:'./controladores/videosController.php',
				type:'POST',
				data:data,
				cache:false,
				error:function(resp){
					console.log(resp);
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
				},	
				success:function(resp){
					var recordset = $.parseJSON(resp);
					if(recordset[0]==1){
						if(recordset[1]=="activar"){
							$("#icono_estatus_"+numero).removeClass("fa-cloud-upload").addClass("fa-cloud-download");
							$("#btn_estatus_"+numero).removeClass("btn-success").addClass("btn-warning");
							$("#btn_estatus_"+numero).attr({'title':'Inactivar publicación'});
							$("#div_estatus_"+numero).text("Publicado");
							mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Video publicado", "alert-success");
						}else{
							$("#icono_estatus_"+numero).removeClass("fa-cloud-download").addClass("fa-cloud-upload");
							$("#btn_estatus_"+numero).removeClass("btn-warning").addClass("btn-success");
							$("#btn_estatus_"+numero).attr({'title':'Publicar noticia'});
							$("#div_estatus_"+numero).text("Inactivo");
							mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Video inactivo", "alert-success");
						}
						
					}else if(recordset[0]==0){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
					}else if(recordset[0]==-1){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error en consulta de estatus ", "alert-danger");
					}
				}
	});				
}
//--