//---Bloque de Eventos
//-----------------------------------------------------
/*"use strict";
// Init Theme Core    
Core.init();
// Init Demo JS  
Demo.init();*/

//-----------------------------------------------------
$("#btn_limpiar_video").click(function(e){
	e.preventDefault();
	limpiar_video();
});
$("#btn_ver_listado_video").click(function(e){
	e.preventDefault();
	cargar_lista_videos();
	setTimeout(function(){
		quitar_preloader();
	},2000);
});
$("#btn_guardar_video").click(function(){
	var id_video = $("#id_video").val();
	var text_url_video = $("#text_url_video").val();
	var accion = "guardar_video";
	var data = {
					'accion':accion,
					'id_video':id_video,
					'text_url_video':text_url_video
	}
	mensaje_preloader2("#campo_mensaje","Espere unos segundos mientras se realiza el proceso...");
	if(validar_formulario_video()==true){
		$.ajax({
					url:'./controladores/videosController.php',
					type:'POST',
					cache:false,
					data:data,
					error: function(resp){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
					},
					success: function(resp){
						var recordset = $.parseJSON(resp);
						if(recordset[0]=='1'){
						//--------------------------------------------
							mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Registro exitoso", "alert-success");
							limpiar_video();
						//-------------------------------------------
						}else
						if(recordset[0]=='2'){
							mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error en proceso de registro", "alert-danger");
						}else
						if(recordset[0]=='3'){
						//-------------------------------------------
							mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Actualizaci&oacute;n exitoso", "alert-success");
							limpiar_video();
						//--------------------------------------------	
						}else
						if(recordset[0]=='4'){
							mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error en proceso de actualizaci&oacute;n", "alert-danger");
						}else
						if(recordset[0]=='-3'){
							mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> No existe el registro", "alert-danger");
						}
						else
						if(recordset[0]=='5'){
							mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Enlace de video no valido", "alert-danger");
						}
					}
		});
	}
});
$("#btn_vista_previa").click(function(e){
	e.preventDefault();
	mensaje_preloader2("#campo_mensaje2","Espere unos segundos mientras se carga la vista previa del video");
	cargar_enlace();
	setTimeout(function(e){
		quitar_preloader_video();
	},6000);
});
//--Bloque de Funciones
//--
function validar_formulario_video(){
	if($("#text_url_video").val()==""){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe ingresar la url del video", "alert-danger");
		return false;
	}else{
		return true;
	}
}
//--
function cargar_lista_videos(){
	var accion = "consultar_listado_video";
	var data = {'accion':accion};
	$.ajax({
				url:'./controladores/videosController.php',
				type:'POST',
				cache:false,
				data:data,
				error:function(resp){
					console.log(resp);
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
				},
				success:function(resp){
					if(resp=="error"){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
					}else{
						$("#cuerpo_principal").html(resp);
						mensaje_preloader("#campo_mensaje_lista");
						iniciar_datatable();
					}
				}
	});

}
//--
function limpiar_video(){
	$("#id_video,#text_url_video").val("");
	$("#preview_material_multimedia").html("");
}
//--