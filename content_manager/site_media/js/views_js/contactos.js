//--Bloque de eventos
$(document).on("click","#btn_volver_listado_contactos",function(e){
	e.preventDefault();
	cargar_lista_contactos();
	setTimeout(function(){
		quitar_preloader();
	},2000);
});
//--Bloque de funciones
//---
function consultar_contacto(numero){
	var datos_contactos = $("#tab"+numero).attr("data");
	var datos = datos_contactos.split("|");
		$("#cuerpo_principal").load("./site_media/templates/form_contactos.html");
	setTimeout(function(){
		$("#nombres_apellidos_contacto").text(datos[1]+" "+datos[2]);
		$("#telefonos_contacto").text(datos[3]);
		$("#correo_contacto").text(datos[4]);
		$("#pais_contacto").text(datos[5]);
		$("#mensaje_contacto").text(datos[6]);

	},1000);

}
function cargar_lista_contactos(){
	var accion = 'cargar_lista_contactos';
	var data = {'accion':accion};
	$.ajax({
				url:'./controladores/contactosController.php',
				type:'POST',
				data:data,
				cache:false,
				error:function(resp){
					console.log(resp);
				},
				success: function(resp){
					$("#cuerpo_principal").html(resp);
					mensaje_preloader("#campo_mensaje_lista");
					iniciar_datatable();
				}
	});
}
//---