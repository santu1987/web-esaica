//-------------------------------
//Bloque de Eventos
//-------------------------------
$(document).ready(function(){

	carga_inicio();
	//$("#cuerpo_principal").load("site_media/templates/empresa.html");
	//--Opciones de menu
	$("#op_empresa").click(function(){
		$("#cuerpo_principal").load("site_media/templates/empresa.html");	
	});
	$("#op_nosotros").click(function(e){
		e.preventDefault();
		//--
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		//--
		//cargar_lista_nosotros();
	});
	//--
	$("#op_nos_empresa").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		cargar_nos_empresa();
	});
	//--
	$("#op_nos_mision").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		cargar_nos_mision();
	});
	//--
	$("#op_nos_vision").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		cargar_nos_vision();
	});
	//--
	$("#op_nos_redes").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		$("#cuerpo_principal").load("site_media/templates/redes.html");
	});
	//--
	$("#op_nos_historia").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		cargar_nos_historia();
	});
	//--
	$("#op_nos_productos").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		cargar_nos_productos();
	});
	//--
	$("#op_contactos").click(function(e){
		e.preventDefault();
		cargar_contacto();
		setTimeout(function(){
			quitar_preloader();
		},6000);
	});	
	//--
	$("#op_cv").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		cargar_cv();
	});
	$("#op_noticias").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		cargar_noticias();
	});
	$("#op_galeria_imagenes").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		carga_imagenes_form();
	});
	$("#op_galeria_videos").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		carga_videos_form();
	});
	//--
	$("#op_usuarios_registros").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		carga_usuarios_form();
	});
	//--
	$("#op_usuarios_permisos").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		});
		carga_usuarios_permisos();
	});
	//--
	$("#cerrar_session,#cerrar_session2").click(function(){
	//--------------------------------------------
		var accion = "cerrar_session";
		var data = {'accion':accion};
		$.ajax({
					url:'./controladores/inicioController.php',
					type:'POST',
					data:data,
					cache:false,
					error:function(resp){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
					},
					success:function(resp){
						var recordset = $.parseJSON(resp);
						if(recordset["cerrado"]=="cerrado"){
							document.forms.form_inicio.action='./index.html';
						    document.forms.form_inicio.submit();
						}
					}
		});
	//--------------------------------------------	
	});
});
//-------------------------------
//Bloue de funciones
//-------------------------------
function carga_inicio(){
	var accion = "datos_usuario";
	var data = {'accion':accion};
	$.ajax({
				url:'./controladores/inicioController.php',
				type:'POST',
				data:data,
				cache:false,
				error:function(resp){
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					if(recordset["cerrado"]=="cerrado"){
						document.forms.form_inicio.action='./index.html';
						document.forms.form_inicio.submit();
					}else{
						$("#nombre_usuario").text(recordset["nombre_usuario"]);
						$("#tipo_usuario,#tipo_usuario2").text(recordset["tipo_usuario"]);
						var arreglo_permisos = recordset["permisos"];
						establecer_permisos(arreglo_permisos);
					}
					
				}
	});
}
function establecer_permisos(arreglo_permisos){
	var a = 0;
	var arreglo_menu= Array();
	arreglo_menu[0] = "#op_empresa_li";
	arreglo_menu[1] = "#op_nos_empresa_li";
	arreglo_menu[2] = "#op_nos_mision_li";
	arreglo_menu[3] = "#op_nos_vision_li";
	arreglo_menu[4] = "#op_nos_historia_li";
	arreglo_menu[5] = "#op_nos_productos_li";
	arreglo_menu[6] = "#op_nos_redes_li";
	arreglo_menu[7] = "#op_cv_li";
	arreglo_menu[8] = "#op_noticias_li";
	arreglo_menu[9] = "#op_galeria_imagenes_li";
	arreglo_menu[10] = "#op_galeria_videos_li";
	arreglo_menu[11] = "#op_contactos_li";
	arreglo_menu[12] = "#op_usuarios_registros_li";
	arreglo_menu[13] = "#op_usuarios_permisos_li";
	//---Para bloquear...
	for (j=0;j<13;j++){
		$(arreglo_menu[j]).attr("style","display:none");	
	}
	//--Para habilitar...
	for (i=1;i<=13;i++){
		if(arreglo_permisos[0][i]==1){
			$(arreglo_menu[a]).attr("style","display:block");
		}
		a++;	
	}
	//--
}
function activar_btn(seccion){
	$("#op_empresa,#op_nosotros,#op_productos,#op_noticias,#op_galerias,#op_contactos").removeClass("activo_top").removeClass("activo_normal");
	switch(seccion){
		case 'empresa':
			$("#op_empresa").addClass("activo_top");
			break;
		case 'nosotros':
			$("#op_nosotros").addClass("activo_top");
			break;
		case 'productos':
			$("#op_productos").addClass("activo_top");
			break;
		case 'noticias':
			$("#op_noticias").addClass("activo_top");
			break;
		case 'galerias':
			$("#op_galerias").addClass("activo_top");
			break;
		case 'contactos':
			$("#op_contactos").addClass("activo_top");
			break;								
	}
}
//-----------------------------------------------------------------
function cargar_lista_nosotros(){
	var accion = "consultar_nosotros";
	var data = {
					'accion':accion
	};
	$.ajax({
				url:'./controladores/nosotrosController.php',
				type:'POST',
				data: data,
				cache:false,
				error:function(resp){
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error inesperado", "alert-danger");
				},
				success:function(resp){
					alert(resp);
					$("#cuerpo_principal").html(resp);
					quitar_preloader();
					//--Para datatable...
					iniciar_datatable();
				}
	});
}
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
function cargar_nos_empresa(){
	$("#cuerpo_principal").load("site_media/templates/nuestra_empresa.html");	
}
//-------------------------------------------------------------------------------
function cargar_nos_mision(){
	$("#cuerpo_principal").load("site_media/templates/mision.html");
}
//-------------------------------------------------------------------------------
function cargar_nos_vision(){
	$("#cuerpo_principal").load("site_media/templates/vision.html");
}
//-------------------------------------------------------------------------------
function cargar_nos_historia(){
	$("#cuerpo_principal").load("site_media/templates/historia.html");	
}
//-------------------------------------------------------------------------------
function cargar_nos_productos(){
	$("#cuerpo_principal").load("site_media/templates/productos.html");
}
//-------------------------------------------------------------------------------
function cargar_contacto(){
	var accion = 'cargar_lista_contactos';
	var data = {'accion':accion};
	$.ajax({
				url:'./controladores/contactosController.php',
				type:'POST',
				data:data,
				cache:false,
				error:function(resp){
					console.log(resp);
				},
				success: function(resp){
					$("#cuerpo_principal").html(resp);
					mensaje_preloader("#campo_mensaje_lista");
					iniciar_datatable();
				}
	});
}
//----------------------------------------------------------------------------------
function cargar_cv(){
	$("#cuerpo_principal").load("site_media/templates/cv.html");
}
//----------------------------------------------------------------------------------
function cargar_noticias(){
	$("#cuerpo_principal").load("site_media/templates/noticias.html");
}
//----------------------------------------------------------------------------------
function carga_imagenes_form(){
	$("#cuerpo_principal").load("site_media/templates/imagenes.html");
}
//----------------------------------------------------------------------------------
function carga_videos_form(){
	$("#cuerpo_principal").load("site_media/templates/videos.html");
}
//----------------------------------------------------------------------------------
function carga_usuarios_form(){
	$("#cuerpo_principal").load("site_media/templates/usuarios.html");
}
//----------------------------------------------------------------------------------
function carga_usuarios_permisos(){
	$("#cuerpo_principal").load("site_media/templates/usuarios_permisos.html");
}
//----------------------------------------------------------------------------------