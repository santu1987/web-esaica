<?php
require_once("../core/conex.php");
class videosModel extends Conex{
	private $rs;
	//--Metodo constructor...
	public function __construct(){
	}
	//--Verificar si existe el video
	public function existe_video($arreglo_datos){
		$sql = "SELECT
						count(*)
				FROM
						tbl_videos
				WHERE
						id='".$arreglo_datos["id_video"]."'";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;				
	}
	//--Registrar datos de videos
	public function registrar_video($arreglo_datos){
		$sql = "INSERT INTO 
					tbl_videos
						(
							url
						)
				VALUES(
						'".$arreglo_datos["text_url_video"]."'
					);";
		$this->rs = $this->procesarQuery2($sql);
		return $this->rs;
		//--	
	}
	//---
	//--Metodo para modificar datos de videos
	public function actualizar_video($arreglo_datos){
		$sql1 = "UPDATE tbl_videos SET url='".$arreglo_datos["text_url_video"]."' WHERE id='".$arreglo_datos["id_video"]."';";
		$this->rs = $this->procesarQuery2($sql1);
		if($this->rs==false){
			return "error";
		}else
		if($this->rs==true){
			return $this->rs;
		}	
	}
	//--
	public function consultar_videos_lista(){
		$sql="SELECT
					id,
					url,
					estatus
			  FROM
			  		tbl_videos
			  order by
			  			id  DESC";
		$this->rs = $this->procesarQuery($sql);
		//return $sql;
		return $this->rs;
	}
	//---
	public function consultar_estatus($id){
		$sql ="SELECT estatus FROM tbl_videos WHERE id ='".$id."'";
		$this->rs = $this->procesarQuery($sql);
		//return $sql;
		return $this->rs;
	}
	//---
	public function activar_inactivar_video($id,$estatus){
		$sql = "UPDATE tbl_videos SET estatus='".$estatus."' WHERE id='".$id."';";
		$this->rs = $this->procesarQuery2($sql);
		//return $sql;
		return $this->rs;
	}
	//----
	public function url_exists($url){
		$file_headers = @get_headers($url);
		if(strpos($file_headers[0],"200 OK")==false){
			$exists = false;
			return $exists;
		}
		else{
			$exists = true;
			return $exists;
		}
	}
	//----
}
?>