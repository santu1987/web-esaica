<?php
require_once("../core/conex.php");
class noticiasModel extends Conex{
	private $rs;
	//--Metodo constructor...
	public function __construct(){
	}
	//--Verificar si existe la noticia
	public function existe_noticia($arreglo_datos){
		$sql = "SELECT
						count(*)
				FROM
						tbl_noticias
				WHERE
						id='".$arreglo_datos["id_noticia"]."'";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;				
	}
	//--Registrar datos de noticias
	public function registrar_noticia($arreglo_datos){
		$sql = "INSERT INTO 
				tbl_noticias
						(
							titulo,
							id_idioma
							)
				VALUES(
						'".$arreglo_datos["titulo_noticia"]."',
						'".$arreglo_datos["idioma"]."'
					);";
		$this->rs = $this->procesarQuery2($sql);
		//--Verifico que no sea error
		if($this->rs[0][0]!="error"){
		//--Consulto el id de la noticia
			$sql2 = "SELECT MAX(id) FROM tbl_noticias";
			$rs2 = $this->procesarQuery($sql2);
		//--
			if($rs2!="error"){
				$sql2 = "INSERT INTO
						tbl_detalles_noticias
						(
							id_noticia,
							parrafo
						)
					 VALUES(
					 		'".$rs2[0][0]."',
					 		'".$arreglo_datos["contenido"]."'
					 		)	";
				$this->rs = $this->procesarQuery2($sql2);
				return $rs2[0][0];	 			
			}else{
				return 'error-2';
			}
		//--	
		}else{
				return 'error-1';
			}
		//--	
	}
	//---
	//--Metodo para modificar datos de noticias
	public function actualizar_noticia($arreglo_datos){
		$sql1 = "UPDATE tbl_noticias SET titulo='".$arreglo_datos["titulo_noticia"]."' WHERE id='".$arreglo_datos["id_noticia"]."';";
		$this->rs = $this->procesarQuery2($sql1);
		if($this->rs==false){
			return "error-1";
		}else
		if($this->rs==true){
			$sql2 = "UPDATE
					tbl_detalles_noticias
				SET
					parrafo='".$arreglo_datos["contenido"]."'
				WHERE
					id_noticia='".$arreglo_datos["id_noticia"]."'";
				$rs2 = $this->procesarQuery2($sql2);
			if($rs2==false){
				return "error-2";
			}else{
				return $rs2;
			}	
		}
	}
	//--
	public function ac_imagen_noticia($nombre_archivo,$id){
		$sql = "UPDATE
						tbl_detalles_noticias
				SET
					imagen='".$nombre_archivo."'
				WHERE
					id_noticia='".$id."'";
		$this->rs = $this->procesarQuery2($sql);
		//return $sql;
		return $this->rs;			
	}
	//--
	public function consultar_noticias_lista(){
		$sql="SELECT
					a.id as id,
					a.id_idioma,
					b.idioma,
					a.titulo,
					c.parrafo,
					c.imagen,
					a.estatus
			  FROM
			  		tbl_noticias a
			  INNER JOIN 
			  		tbl_idiomas b
			  ON
			  		a.id_idioma=b.id
			  INNER JOIN
			  		tbl_detalles_noticias c
			  ON 
			  		a.id =c.id_noticia";
		$this->rs = $this->procesarQuery($sql);
		//return $sql;
		return $this->rs;
	}
	//---
	public function consultar_estatus($id){
		$sql ="SELECT estatus FROM tbl_noticias WHERE id ='".$id."'";
		$this->rs = $this->procesarQuery($sql);
		//return $sql;
		return $this->rs;
	}
	//---
	public function activar_inactivar_noticia($id,$estatus){
		$sql = "UPDATE tbl_noticias SET estatus='".$estatus."' WHERE id='".$id."';";
		$this->rs = $this->procesarQuery2($sql);
		//return $sql;
		return $this->rs;
	}
}
?>