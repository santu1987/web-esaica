<?php
require_once("../core/conex.php");
class redesModel extends Conex{
	private $rs;
	//--Metodo constructor...
	public function __construct(){
	}
	//---Consultar redes select
	public function consultar_select_redes(){
		$sql = "SELECT 
						id,
						tipo_red
				FROM 
						tbl_tipo_redes";
		$this->rs = $this->procesarQuery($sql);
		//return $sql...
		return $this->rs;				
	}
	//---Consultar ulr de redes
	public function consultar_url_redes($tipo_red){
		$sql = "SELECT
						id,
						enlace 
			    FROM 
			    	tbl_redes
			    WHERE 
			    	id_tipo_red='".$tipo_red."'";
		$this->rs = $this->procesarQuery($sql);
		//return $sql;
		return $this->rs;
	}
	//---Consultar si existe la url de redes
	public function existe_redes($tipo_red){
		$sql = "SELECT
						count(*)
			    FROM 
			    	tbl_redes
			    WHERE 
			    	id_tipo_red='".$tipo_red."'";
		$this->rs = $this->procesarQuery($sql);
		//return $sql;
		return $this->rs;
	}
	//--Registrar datos de redes
	public function registrar_redes($arreglo_datos){
		$sql = "INSERT INTO 
				tbl_redes
						(
							id_empresa,
							id_tipo_red,
							enlace
							)
				VALUES(
						'1',
						'".$arreglo_datos["tipo_red"]."',
						'".$arreglo_datos["red_social"]."'
					);";
		$this->rs = $this->procesarQuery2($sql);
		//return $sql;
		return $this->rs;
	}
	//---
	//--Metodo para modificar datos de redes
	public function actualizar_redes($arreglo_datos){
		$sql = "UPDATE
						tbl_redes
					SET
						id_empresa='1',
						id_tipo_red='".$arreglo_datos["tipo_red"]."',
						enlace='".$arreglo_datos["red_social"]."'
					WHERE id='".$arreglo_datos["id_red"]."'";
		$this->rs = $this->procesarQuery2($sql);
		//return $sql;
		return $this->rs;
	}
	//---
}
?>