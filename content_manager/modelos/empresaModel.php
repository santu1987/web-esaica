<?php
require_once("../core/conex.php");
class empresaModel extends Conex{
	private $rs;
	//--Metodo constructor...
	public function __construct(){
	}
	//--
	public function consultar_empresa($arreglo_datos){
		$sql = "SELECT 
					count(*)
				FROM 
					tbl_detalle_empresa
				WHERE
					id_idioma='".$arreglo_datos['idioma']."';";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--
	public function consultar_existe_empresa($id){
		$sql = "SELECT 
					count(*)
				FROM 
					tbl_detalle_empresa
				WHERE
					id='".$id."';";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;			
	}
	//--
	public function registrar_empresa($arreglo_datos){
		$sql = "INSERT INTO 
							tbl_detalle_empresa
				(
					id_empresa,
					id_idioma,
					direccion,
					telefono,
					telefono2,
					telefono3,
					email,
					horario
				)
				VALUES
				(
					'".$arreglo_datos['empresa']."',
					'".$arreglo_datos['idioma']."',
					'".$arreglo_datos['direccion']."',
					'".$arreglo_datos['tlf1']."',
					'".$arreglo_datos['tlf2']."',
					'".$arreglo_datos['tlf3']."',
					'".$arreglo_datos['email']."',
					'".$arreglo_datos['horario']."'
				);";
		$this->rs = $this->procesarQuery2($sql);
		//return $sql;
		return $this->rs;
	}
	//--
	public function actualizar_empresa($arreglo_datos){
		$sql ="UPDATE 
					tbl_detalle_empresa
				SET 
					direccion='".$arreglo_datos['direccion']."',
					telefono='".$arreglo_datos['tlf1']."',
					telefono2='".$arreglo_datos['tlf2']."',
					telefono3='".$arreglo_datos['tlf3']."',
					email='".$arreglo_datos['email']."',
					horario='".$arreglo_datos['horario']."'
				WHERE
					id='".$arreglo_datos['id_empresa']."'
				AND 
					id_idioma ='".$arreglo_datos['idioma']."';";
		$this->rs = $this->procesarQuery2($sql);					
		//return $sql;
		return $this->rs;
	}
	//--
	//Consultar empresa lista
	public function consultar_empresa_lista(){
		$sql = "SELECT 
					a.id AS id,
					a.id_empresa AS id_empresa,
					a.id_idioma AS id_idioma,
					a.direccion AS direccion,
					a.telefono AS telefono,
					a.telefono2 AS telefono2,
					a.telefono3 AS telefono3,
					a.email AS email,
					a.horario AS horario,
					b.nombre AS nombre_empresa,
					c.idioma AS nombre_idioma
				FROM 
				 	tbl_detalle_empresa a
				 INNER JOIN 
				 	tbl_empresa b
				 ON 
				 	b.id= a.id_empresa
				 INNER JOIN 
				 	tbl_idiomas c
				 ON 
				 	c.id = a.id_idioma			
				 ORDER BY 
				 	id";
		$this->rs = $this->procesarQuery($sql);
		//return $sql....
		return $this->rs;		 	
	}
	//Consultar empresa select
	public function consultar_select_emp(){
		$sql = "SELECT 
						id,
						nombre
				FROM 
						tbl_empresa";
		$this->rs = $this->procesarQuery($sql);
		//return $sql...
		return $this->rs;				
	}
	//Consultar idioma select
	public function consultar_select_idioma(){
		$sql = "SELECT 
						id,
						idioma
				FROM
						tbl_idiomas";
		$this->rs = $this->procesarQuery($sql);
		//return $sql...
		return $this->rs;	
	}
	//Consultar persona select
	public function consultar_select_persona(){
		$sql = "SELECT 
						id,
						nombres
				FROM
						tbl_personas";
		$this->rs = $this->procesarQuery($sql);
		//return $sql...
		return $this->rs;	
	}
	//--
}
?>