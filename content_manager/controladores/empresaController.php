<?php
require_once("../modelos/empresaModel.php");
//--Declaraciones
$mensajes = array();
$arreglo_datos = helper_userdata();
redireccionar_metodos($arreglo_datos);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'guardar':
			guardar_empresa($arreglo_datos);
			break;
		case 'consultar_empresa':
			consultar_empresa();
			break;
		case 'consultar_select_empresa':
			consultar_select_empresa();
			break;
		case 'consultar_select_idiomas':
			consultar_idiomas();
			break;
		case 'registrar_red':
			guardar_redes($arreglo_datos);	 			
			break;
		case 'consultar_select_personas':
			consultar_personas();
			break;	
	}	
}
//---
function helper_userdata(){
	$user_data = array();
	if($_POST){
		//--
		if(array_key_exists('accion', $_POST)){
			$user_data["accion"] = $_POST["accion"];
		}
		if(array_key_exists('idioma', $_POST)){
			$user_data["idioma"] = $_POST["idioma"];
		}
		if(array_key_exists('direccion', $_POST)){
			$user_data["direccion"] = $_POST["direccion"];
		}
		if(array_key_exists('empresa', $_POST)){
			$user_data["empresa"] = $_POST["empresa"];
		}
		if(array_key_exists('tlf1', $_POST)){
			$user_data["tlf1"] = $_POST["tlf1"];
		}
		if(array_key_exists('tlf2', $_POST)){
			$user_data["tlf2"] = $_POST["tlf2"];
		}
		if(array_key_exists('tlf3', $_POST)){
			$user_data["tlf3"] = $_POST["tlf3"];
		}
		if(array_key_exists('email', $_POST)){
			$user_data["email"] = $_POST["email"];
		}
		if(array_key_exists('horario', $_POST)){
			$user_data["horario"] = $_POST["horario"];
		}
		if(array_key_exists('id_empresa', $_POST)){
			$user_data["id_empresa"] = $_POST["id_empresa"];
		}
		//--	
		//--
	}
	return $user_data;
}
//-----------------------------------------------
function guardar_empresa($arreglo_datos){
	$arreglo_retorno = array();
	$obj = new empresaModel();
	if($arreglo_datos["id_empresa"]==""){
	//-------------------------------------
	//Para guardar
		$existe = $obj->consultar_empresa($arreglo_datos);
		//-------
		if($existe=="error"){
			$arreglo_retorno[0]=-1;//error en registro
		}else
		if ($existe[0][0]==0){
			$recordset = $obj->registrar_empresa($arreglo_datos);
			if($recordset==true){
				$arreglo_retorno[0]=1;//registro exitoso....
			}else
			{
				$arreglo_retorno[1]=2;//error en registro....
			}
		}else{
			$arreglo_retorno[0]=0;//existe un registro de empresa para ese idioma
		}
	//-------------------------------------	
	}else{
	//--------------------------------------	
	//Para modificar
		$existe = $obj->consultar_existe_empresa($arreglo_datos['id_empresa']);
		if($existe=="error"){
			$arreglo_retorno[0]=-1;//error en registro
		}else
		if ($existe[0][0]>0){
			$recordset = $obj->actualizar_empresa($arreglo_datos);
			if($recordset==true){
				$arreglo_retorno[0]=1;//registro exitoso....
			}else
			{
				$arreglo_retorno[0]=2;//error en registro....
			}
		}else{
			$arreglo_retorno[0]=-2;//no existe el registro de empresa...
		}
	//--------------------------------------	
	}
	die(json_encode($arreglo_retorno));
}
//------------------------------------------------------
function consultar_empresa(){
	$recordset = array();
	$arreglo_valor = array();
	$obj = new empresaModel();
	$recordset = $obj->consultar_empresa_lista();
	if($recordset!="error"){
		for($i=0;$i<count($recordset);$i++){
			$arreglo_valor[$i]["id"] = $recordset[$i][0];
			$arreglo_valor[$i]["id_empresa"] = $recordset[$i][1];
			$arreglo_valor[$i]["id_idioma"] = $recordset[$i][2];
			$arreglo_valor[$i]["direccion"] = $recordset[$i][3];
			$arreglo_valor[$i]["telefono"] = $recordset[$i][4];
			$arreglo_valor[$i]["telefono2"] = $recordset[$i][5];
			$arreglo_valor[$i]["telefono3"] = $recordset[$i][6];
			$arreglo_valor[$i]["email"] = $recordset[$i][7];
			$arreglo_valor[$i]["horario"] = $recordset[$i][8];
			$arreglo_valor[$i]["nombre_empresa"] = $recordset[$i][9];
			$arreglo_valor[$i]["idioma"] = $recordset[$i][10];
		}
		die(json_encode($arreglo_valor));
	}else
	{
		$recordset[0]="error";
		die(json_encode($recordset));
	}
}
//------------------------------------------------------
function consultar_select_empresa(){
	$recordset = array();
	$arreglo = array();
	$obj = new empresaModel();
	$recordset = $obj->consultar_select_emp();
	$select_empresa = "<option value='0' >--Seleccione una empresa--</option>";
	for($i=0;$i<count($recordset);$i++){
		$select_empresa.="<option value='".$recordset[$i][0]."'>".$recordset[$i][1]."</option>";
	}
	$arreglo["opciones"] = $select_empresa;
	die(json_encode($arreglo));
}
//------------------------------------------------------
function consultar_idiomas(){
	$recordset = array();
	$arreglo = array();
	$obj = new empresaModel();
	$recordset = $obj->consultar_select_idioma();
	$select_idioma = "<option value='0' >--Seleccione un Idioma--</option>";
	for($i=0;$i<count($recordset);$i++){
		$select_idioma.="<option value='".$recordset[$i][0]."'>".$recordset[$i][1]."</option>";
	}
	$arreglo["opciones"] = $select_idioma;
	die(json_encode($arreglo));	
}
//------------------------------------------------------
function consultar_personas(){
	$recordset = array();
	$arreglo = array();
	$obj = new empresaModel();
	$recordset = $obj->consultar_select_persona();
	$select_persona = "<option value='0' >--Seleccione una persona--</option>";
	for($i=0;$i<count($recordset);$i++){
		$select_persona.="<option value='".$recordset[$i][0]."'>".$recordset[$i][1]."</option>";
	}
	$arreglo["opciones"] = $select_persona;
	die(json_encode($arreglo));	
}
//------------------------------------------------------
?>