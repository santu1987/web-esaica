<?php
session_start();
require_once("../modelos/contactosModel.php");
require_once("../vistas_logicas/contactosView.php");
//--Declaraciones
$mensajes = array();
$arreglo_datos = helper_userdata();
redireccionar_metodos($arreglo_datos);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'cargar_lista_contactos':
			cargar_lista_contactos();
			break;
	}
}
//---
function helper_userdata(){
	$user_data = array();
	if($_POST){
		//--
		if(array_key_exists('accion', $_POST)){
			$user_data["accion"] = $_POST["accion"];
		}
		//--
	}
	return $user_data;
}
//---
function cargar_lista_contactos(){
	$recordset = array();
	$arreglo_datos = array();
	$obj = new contactosModel(); 
	$recordset = $obj->consultar_contactos_lista();
	if($recordset!="error"){
		render_vista_consulta("lista_contactos",$recordset);
	}else{
		$recordset = "error";
		die(json_encode($recordset));
	} 
}
//---
?>