<?php
require_once("../modelos/videosModel.php");
require_once("../vistas_logicas/videosView.php");
//--Declaraciones
$mensajes = array();
$arreglo_datos = helper_userdata();
redireccionar_metodos($arreglo_datos);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'guardar_video':
			guardar_video($arreglo_datos);
			break;
		case 'consultar_listado_video':
			consultar_listado_video();
			break;
		case 'publicar_video':
			publicar_video($arreglo_datos);
			break;					
	}	
}
//---
function helper_userdata(){
	$user_data = array();
	if($_POST){
		//--
		if(array_key_exists('accion', $_POST)){
			$user_data["accion"] = $_POST["accion"];
		}
		if(array_key_exists('id_video', $_POST)){
			$user_data["id_video"] = $_POST["id_video"];
		}
		if(array_key_exists('text_url_video', $_POST)){
			$user_data["text_url_video"] = $_POST["text_url_video"];
		}
		//--
	}
	return $user_data;
}
//------------------------------------------------------
function guardar_video($arreglo_datos){
	$recordset = array();
	$arreglo_retorno = array();
	$existe = array();
	$obj = new videosModel();
	//---Verifico si la url es valida
	$valida_url = $obj->url_exists($arreglo_datos["text_url_video"]);
	if ($valida_url==true){
		//----------------------------------------------------------
			if($arreglo_datos["id_video"]==""){
			//-------------------------------------
			//Para guardar
				$recordset = $obj->registrar_video($arreglo_datos);
				if($recordset==true){
					$arreglo_retorno[0]=1;//registro exitoso ...
				}else{
					$arreglo_retorno[0]=2;//error en registro....
				}
			//-------------------------------------	
			}else{
			//--------------------------------------	
			//Para modificar
				$existe = $obj->existe_video($arreglo_datos);
				if($existe[0][0]>0){
					$recordset = $obj->actualizar_video($arreglo_datos);
					if($recordset==true){
						$arreglo_retorno[0]=3;//registro exitoso....
					}else if($recordset=="error"){
						$arreglo_retorno[0]=4;//error en registro....
					}
				}else{
					$arreglo_retorno[0]=-3;//no existe registro....	
				}	
			//--------------------------------------	
			}
	}else{
		$arreglo_retorno[0] = 5; //url no valida
	}

	die(json_encode($arreglo_retorno));
}
//------------------------------------------------------
function consultar_listado_video(){
	$recordset = array();
	$arreglo_datos = array();
	$obj = new videosModel();
	$recordset = $obj->consultar_videos_lista();
	if($recordset!="error"){
		render_vista_consulta("lista_videos",$recordset);
	}else{
		$recordset="error";
		die($recordset);
	}
}
//------------------------------------------------------
function publicar_video($arreglo_datos){

	$recordset = array();
	$arreglo_retorno = array();
	$obj = new videosModel();
	$estatus = $obj->consultar_estatus($arreglo_datos['id_video']);
	if ($estatus!="error"){
	//-----------------------
		if($estatus[0][0]==1){
			$recordset = $obj->activar_inactivar_video($arreglo_datos['id_video'],0);
			$arreglo_retorno[1]="inactivar";
		}else
		if($estatus[0][0]==0){
			$recordset = $obj->activar_inactivar_video($arreglo_datos['id_video'],1);
			$arreglo_retorno[1]="activar";
		}
		//--
		if($recordset==true){
			$arreglo_retorno[0]=1; //Proceso exitoso...
		}else
		if($recordset==false){
			$arreglo_retorno[0]=0; //Error en proceso ...
		}
		//--
		
	//-----------------------
	}else{
		$arreglo_retorno[0] = -1; //Error en consulta de estatus...
	}
	die(json_encode($arreglo_retorno));
}
//-------------------------------------------------------
?>