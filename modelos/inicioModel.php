<?php
require_once('../core/conex.php');
class inicioModel extends Conex{

	private $rs; 
	//--Metodo constructor...
	public function __construct(){
	}
	//--Metodo que consulta informacion de pagina de inicio->nosotros
	public function consultar_inicio($idioma){
		$sql = "SELECT 
						* 
						FROM  
							tbl_nosotros
						WHERE 
							id_idioma ='".$idioma."'";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--Metodo que guarda informacion de pagina de inicio ->contactos
	public function guardar_contacto_inicio($arreglo_data){
		$sql = "INSERT 
					INTO 
						tbl_contactos
					(
						nombres,
						apellidos,
						telefono,
						correo,
						pais,
						mensaje
					)
				VALUES(
						'".$arreglo_data['nombres_contacto']."',
						'".$arreglo_data['apellidos_contacto']."',
						'".$arreglo_data['telefonos_contacto']."',
						'".$arreglo_data['correo_contacto']."',
						'".$arreglo_data['pais_contacto']."',
						'".$arreglo_data['contenido_contacto']."'
					);";
		$this->rs = $this->procesarQuery2($sql);
		//return $sql;
		return $this->rs;
	}
}
?>