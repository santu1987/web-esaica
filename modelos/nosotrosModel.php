<?php
require_once('../core/conex.php');
class nosotrosModel extends Conex{

	private $rs; 
	//--Metodo constructor...
	public function __construct(){
	}
	//--Metodo que consulta informacion de pagina de Nosotros->quienes somos
	public function consultar_nosotros($idioma){
		$sql = "SELECT 
						a.id,
						a.id_empresa,
						a.id_idioma,
						a.quienes_somos,
						a.mision,
						a.vision,
						a.producto,
						a.historia,
						(SELECT b.descripcion FROM tbl_cv b where id_persona=1 and id_idioma='".$idioma."') AS cv1,
						(SELECT b.descripcion FROM tbl_cv b where id_persona=2 and id_idioma='".$idioma."') AS cv2
				FROM  
						tbl_nosotros a

				WHERE 
						id_idioma ='".$idioma."'";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}

}
?>