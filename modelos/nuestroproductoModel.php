<?php
require_once('../core/conex.php');
class nuestroproductoModel extends Conex{

	private $rs; 
	//--Metodo constructor...
	public function __construct(){
	}
	//--Metodo que consulta informacion de pagina de Nuestro producto
	public function consultar_nuestroproducto($idioma){
		$sql = "SELECT 
						* 
						FROM  
							tbl_nosotros
						WHERE 
							id_idioma ='".$idioma."'";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--Metodo que consulta la información de la seccion nuestro producto -> cacao
	public function consultar_cacao($idioma){
		$sql = "SELECT 
					tbl_cacao.tipo_cacao,
					tbl_cacao.cantidad,
					tbl_cacao.id_especificaciones_tecnicas,
					tbl_caracteristicas_detalle.fermentacion,
				    tbl_caracteristicas.tipo,
				    tbl_caracteristicas_detalle.descripcion,
				    tbl_especificaciones.tamano,
				    tbl_especificaciones.humedad,
				    tbl_especificaciones.sustancias_extranas,
				    tbl_especificaciones.alcalinidad,
				    tbl_especificaciones.acidez,
				    tbl_especificaciones.mohosos,
				    tbl_especificaciones.pizarrosos,
				    tbl_especificaciones.danados,
				    tbl_cacao.total1,
				    tbl_cacao.total2 
				FROM 
				    tbl_cacao
				INNER JOIN 
					tbl_caracteristicas
				ON 
					tbl_caracteristicas.id = tbl_cacao.id_caracteristicas
				INNER JOIN 
				  tbl_caracteristicas_detalle 
				ON
				  tbl_caracteristicas_detalle.id_caracteristica = tbl_caracteristicas.id
				INNER JOIN
					tbl_especificaciones 
				ON 
					tbl_especificaciones.id = tbl_cacao.id_especificaciones_tecnicas	  	
				 WHERE 
				 	tbl_caracteristicas_detalle.id_idioma='".$idioma."'";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
}
?>