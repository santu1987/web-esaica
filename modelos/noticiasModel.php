<?php
require_once('../core/conex.php');
class noticiasModel extends Conex{

	private $rs; 
	//--Metodo constructor...
	public function __construct(){
	}
	//--Metodo que consulta informacion de pagina de noticias
	public function consultar_noticias($idioma,$offset,$limit,$actual){
		if(($offset!="")&&($limit!=""))	
		{
			$where2="limit ".$limit." 
                    offset ".$offset;
		}
		$sql = "SELECT 
						a.id,
						a.titulo,
						b.parrafo,
						b.imagen						
				FROM  
						tbl_noticias a
				INNER JOIN 
						tbl_detalles_noticias b
				ON 				
						a.id = b.id_noticia		
				WHERE 
						id_idioma ='".$idioma."'
				AND 
						estatus=1		
				order by a.id desc ".$where2."";
		//return $sql;		
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--Metodo que consulta numrows de noticias
	public function consultar_num_rows($idioma){
		$sql = "SELECT COUNT(*) from tbl_noticias where id_idioma=".$idioma." AND estatus=1;";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs[0][0];	
	}
	//--
}
?>