<?php
require_once('../core/conex.php');
class galeriasModel extends Conex{

	private $rs; 
	//--Metodo constructor...
	public function __construct(){
	}
	//--Metodo que consulta informacion de imagenes
	public function consultar_imagenes($idioma){
		$sql = "SELECT 
						a.id,
						a.archivo						
				FROM  
						tbl_imagenes a
				WHERE 
					a.estatus=1
				AND 
					a.id_idioma='".$idioma."'			
				order by a.id desc";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--
	//--Metodo que consulta informacion de pagina de imagenes con respectiva paginacion
	public function consultar_imagenes_paginacion($idioma,$offset,$limit,$actual){
		if(($offset!="")&&($limit!=""))	
		{
			$where2="limit ".$limit." 
                    offset ".$offset;
		}
		$sql = "SELECT 
						a.id,
						a.contenido,
						a.archivo						
				FROM  
						tbl_imagenes a	
				WHERE 
						id_idioma ='".$idioma."'
				AND 
						estatus=1		
				order by a.id desc ".$where2."";
		//return $sql;		
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--Metodo que consulta numrows de imagenes
	public function consultar_num_rows($idioma){
		$sql = "SELECT COUNT(*) from tbl_imagenes where id_idioma=".$idioma." AND estatus=1;";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs[0][0];	
	}
	//--Metodo que consulta videos con paginacion
	public function consultar_videos_paginacion($offset,$limit,$actual){
		if(($offset!="")&&($limit!=""))	
		{
			$where2="limit ".$limit." 
                    offset ".$offset;
		}
		$sql = "SELECT 
						a.id,
						a.url					
				FROM  
						tbl_videos a	
				WHERE 
						estatus=1		
				order by a.id desc ".$where2."";
		//return $sql;		
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--Metodo ue consulta numrows de videos
	public function consultar_num_rows_videos(){
		$sql = "SELECT COUNT(*) from tbl_videos where estatus=1;";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs[0][0];
	}
	//--
}
?>