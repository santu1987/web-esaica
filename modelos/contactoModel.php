<?php
require_once('../core/conex.php');
class contactoModel extends Conex{

	private $rs; 
	//--Metodo constructor...
	public function __construct(){
	}
	//--Metodo que consulta informacion de pagina de contacto
	public function consultar_contacto($idioma){
		$sql = "SELECT 
						a.id,
						a.direccion,
						a.telefono,
						a.telefono2,
						a.telefono3,
						a.email,
						a.horario
				FROM  
						tbl_detalle_empresa a

				WHERE 
						a.id_idioma ='".$idioma."'";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;
	}
	//--Metodo que consulta la informacion de redes sociaales
	public function consultar_redes($redes){
		$sql =  "SELECT 
						enlace 
				FROM
						tbl_redes
				WHERE
						id_tipo_red='".$redes."'";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;				
	}
}
?>