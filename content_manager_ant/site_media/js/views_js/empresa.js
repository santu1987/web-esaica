//---Bloque de eventos
//--
cargar_select_empresa();
cargar_select_idiomas();
$("#texo_btn_guardar").text("Guardar");
$("#btn_guardar_empresa").click(function(e){
	e.preventDefault();
	var empresa = $("#select_empresa").val();
	var idioma = $("#select_idioma").val();
	var direccion = $("#text_direccion").val();
	var tlf1 = $("#text_tlf").val();
	var tlf2 = $("#text_tlf2").val();
	var tlf3 = $("#text_tlf3").val();
	var email = $("#text_email").val();
	var horario = $("#text_horario").val();
	var id_empresa  = $("#id_empresa").val();
	var accion = 'guardar';
	mensaje_preloader("#campo_mensaje");
	if(validar_datos_empresa()==true){
	//-----------------------------------
		var data = {
					'empresa':empresa,
					'idioma':idioma,
					'direccion':direccion,
					'tlf1':tlf1,
					'tlf2':tlf2,
					'tlf3':tlf3,
					'email':email,
					'horario':horario,
					'id_empresa':id_empresa,
					'accion':accion
		};
	$.ajax({
				url:'./controladores/empresaController.php',
				type:'POST',
				data:data,
				cache:false,
				error: function(resp){
					console.log(resp);
				},
				success: function(resp){
					var recordset = $.parseJSON(resp);
					//alert('resp:'+resp+" recordset:"+recordset);
					if(recordset[0]=='1'){
						//Registro exitoso...
						mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Registro exitoso", "alert-success");
						limpiar_campos_empresa();
					}else if(recordset[0]=='2'){
						//Error en proceso...
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error en proceso de registro", "alert-danger");
					}else if(recordset[0]=='0'){
						//...Existe
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ya existe un registro de empresa con este idioma", "alert-danger");
					}else if(recordset[0]=='-2'){
						//...Existe
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> No existe el registro", "alert-danger");
					}
				}
	});
	//-----------------------------------	
	}
});
//---
$("#btn_ver_listado_emp").click(function(e){
	e.preventDefault();
	$("#cuerpo_principal").load("site_media/templates/lista_empresa.html");
	//--
	setTimeout(function(){
		mensaje_preloader("#campo_mensaje_lista");
	},2000);
	//Cargar datos tabla
	setTimeout(function(){
		cargar_lista_empresa();
	},3000);
	//--
});
$("#btn_nuevo").click(function(e){
	e.preventDefault();
	$("#cuerpo_principal").load("site_media/templates/empresa.html");
});
//---------------------------------------------------------------------------------------------------------
//---Bloque de funciones
function quitar_preloader(){
	$("#contenido_inicial").removeClass("mostrar").removeClass("contenido_inicial").addClass("ocultar");	
	$("#contenido_web").removeClass("ocultar").addClass("mostrar");
}
function cargar_select_empresa(){
	var accion = "consultar_select_empresa";
	var data = {
					'accion':accion
	};
	$.ajax({
				url:'./controladores/empresaController.php',
				type:'POST',
				cache:false,
				data:data,
				error:function(resp){
					console.log(resp);
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					$("#select_empresa").html(recordset["opciones"])
				}
	});
}
function cargar_select_idiomas(){
	var accion = "consultar_select_idiomas";
	var data = {
					'accion':accion
	};
	$.ajax({
				url:'./controladores/empresaController.php',
				type:'POST',
				cache:false,
				data:data,
				error:function(resp){
					console.log(resp);
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					$("#select_idioma").html(recordset["opciones"])
				}
	});
}
function validar_datos_empresa(){
//
	if($("#select_empresa").val()=="0"){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe seleccionar una empresa", "alert-danger");
		return false;
	}else
	if($("#select_idioma").val()=="0"){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe seleccionar un idioma", "alert-danger");
		return false;
	}else
	if($("#text_direccion").val()==""){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe ingresar una direcci&oacute;n", "alert-danger");
		return false;
	}else
	if($("#text_tlf").val()==""){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe ingresar un n&uacute;mero de tel&eacute;fono", "alert-danger");
		return false;
	}
	else
	if($("#text_tlf2").val()==""){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe ingresar un n&uacute;mero de tel&eacute;fono", "alert-danger");
		return false;
	}else
	if($("#text_tlf3").val()==""){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe ingresar un n&uacute;mero de tel&eacute;fono", "alert-danger");
		return false;
	}else
	if($("#text_email").val()==""){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe ingresar un email", "alert-danger");
		return false;
	}else
	if($("#text_horario").val()==""){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe ingresar un horario", "alert-danger");
		return false;
	}else{
		return true;
	}
//
}
//--Carga lista empresa
function cargar_lista_empresa(){
	var accion = "consultar_empresa";
	var data = {
					'accion':accion
	};
	$.ajax({
				url:'./controladores/empresaController.php',
				type:'POST',
				data: data,
				cache:false,
				error:function(resp){
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error inesperado", "alert-danger");
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					var tabla_dinamica = "";
					var i = 0;
					for(i=0;i<recordset.length;i++){
						tabla_dinamica+="<tr id='tab"+i+"' onclick='consultar_registro("+i+");' data='"+recordset[i]["id"]+"|"+recordset[i]["id_empresa"]+"|"+recordset[i]["id_idioma"]+"|"+recordset[i]["direccion"]+"|"+recordset[i]["telefono"]+"|"+recordset[i]["telefono2"]+"|"+recordset[i]["telefono3"]+"|"+recordset[i]["email"]+"|"+recordset[i]["horario"]+"|"+recordset[i]["instagram"]+"|"+recordset[i]["facebook"]+"'>\
											<td>"+recordset[i]["nombre_empresa"]+"</td>\
											<td>"+recordset[i]["idioma"]+"</td>\
											<td>"+recordset[i]["direccion"]+"</td>\
											<td>"+recordset[i]["telefono"]+"</td>\
											<td>"+recordset[i]["email"]+"</td>\
											<td>"+recordset[i]["horario"]+"</td>\
										</tr>";
					}	
					$("#tbody_lista_empresa").html(tabla_dinamica);
					quitar_preloader();
					//--Para datatable...
					iniciar_datatable();
				}
	});
}
//--
//Para inicializa datatable
function iniciar_datatable(){
	"use strict";
	// Init Theme Core    
	Core.init();
	// Init Demo JS  
	Demo.init();
	$('#datatable2').dataTable({
	  "aoColumnDefs": [{
	    'bSortable': false,
	    'aTargets': [-1]
	  }],
	  "oLanguage": {
	    "oPaginate": {
	      "sPrevious": "",
	      "sNext": ""
	    }
	  },
	  "iDisplayLength": 5,
	  "aLengthMenu": [
	    [5, 10, 25, 50, -1],
	    [5, 10, 25, 50, "All"]
	  ],
	  "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
	  "oTableTools": {
	    "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
	  }
	});
	// Add Placeholder text to datatables filter bar
	$('.dataTables_filter input').attr("placeholder", "Ingrese dato a filtrar...");
}
//--
function consultar_registro(numero){
	var datos_emp = $("#tab"+numero).attr("data");
	var arreglo_datos = datos_emp.split("|");
	//--Carga el formulario
	$("#btn_nuevo").click();
	//--
	setTimeout(function(){
		$("#id_empresa").val(arreglo_datos[0]);
		$("#select_empresa").val(arreglo_datos[1]);
		$("#select_idioma").val(arreglo_datos[2]);
		$("#text_direccion").val(arreglo_datos[3]);
		$("#text_tlf").val(arreglo_datos[4]);
		$("#text_tlf2").val(arreglo_datos[5]);
		$("#text_tlf3").val(arreglo_datos[6]);
		$("#text_email").val(arreglo_datos[7]);
		$("#text_horario").val(arreglo_datos[8]);
		$("#text_instagram").val(arreglo_datos[9]);
		if (arreglo_datos[10]!="null"){
			$("#text_facebook").val(arreglo_datos[10]);
		}
		$("#texo_btn_guardar").text("Actualizar");
	},1000);
	//--
}
//--Par limpiar campos del formulario
function limpiar_campos_empresa(){
	$("#select_empresa").val(0);
	$("#select_idioma").val(0);
	$("#text_direccion,#text_tlf,#text_tlf2,#text_tlf3,#text_email,#text_horario,#id_empresa").val("");	
	$("#texo_btn_guardar").text("Guardar");
}
//---------------------------------------------------------------------------------------------------------