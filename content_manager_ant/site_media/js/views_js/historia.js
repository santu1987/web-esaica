//---Bloque de Eventos
//-----------------------------------------------------
    "use strict";

    // Init Theme Core    
    Core.init();

    // Init Demo JS  
    Demo.init();


    // Init Summernote Plugin
    $('.summernote').summernote({
      height: 255, //set editable area's height
      focus: false, //set focus editable area after Initialize summernote
      oninit: function() {},
      onChange: function(contents, $editable) {},
    });

    // Init Inline Summernote Plugin
    $('.summernote-edit').summernote({
      airMode: true,
      focus: false //set focus editable area after Initialize summernote            
    });
    // Init MarkDown Editor
    $("#markdown-editor").markdown({
      savable: false,
      onChange: function(e) {
        var content = e.parseContent(),
          content_length = (content.match(/\n/g) || []).length + content.length
        if (content == '') {
          $('#md-footer').hide()
        } else {
          $('#md-footer').show().html(content)
        }
      }
    });
//-----------------------------------------------------
cargar_select_idiomas();
$("#markdown-editor,#md-footer").text("");
$("#select_idioma_historia").change(function(){
	var idioma = $("#select_idioma_historia").val();
	cargar_contenido_nosotros(idioma);
});
$("#btn_limpiar_historia").click(function(e){
	e.preventDefault();
	limpiar_nuestra_empresa();
});
$("#btn_guardar_historia").click(function(){
	var id = $("#id_nosotros").val();
	var idioma = $("#select_idioma_historia").val();
	var contenido = $('#md-footer').html();
	var accion = "guardar_historia";
	var data = {
					'accion':accion,
					'id':id,
					'contenido':contenido,
					'idioma':idioma
	}
	if(validar_formulario_nuestra_empresa()==true){
		$.ajax({
					url:'./controladores/nosotrosController.php',
					type:'POST',
					cache:false,
					data:data,
					error: function(resp){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute;o un error inesperado", "alert-danger");
					},
					success: function(resp){
						var recordset = $.parseJSON(resp);
						if(recordset[0]=='1'){
							mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Registro exitoso", "alert-success");
							limpiar_nuestra_empresa();
						}else
						if(recordset[0]=='0'){
							mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error en proceso de registro", "alert-danger");
							limpiar_nuestra_empresa();
						}else
						if(recordset[0]=='2'){
							mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Actualizaci&oacute;n exitosa", "alert-success");
							limpiar_nuestra_empresa();
						}else
						if(recordset[0]=='3'){
							mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error en proceso de actualizaci&oacute;n", "alert-danger");
							limpiar_nuestra_empresa();
						}
					}
		});
	}
});
//--Bloque de Funciones
function validar_formulario_nuestra_empresa(){
	if($("#select_idioma_historia").val()=="0"){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe seleccionar idioma", "alert-danger");
		return false;
	}else
	if(($("#md-footer").text()=="")||($("#markdown-editor").val()=="")){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe ingresar el contenido de la secci&oacute;n", "alert-danger");
		return false;
	}else
		return true;
}
//--
function cargar_select_idiomas(){
	var accion = "consultar_select_idiomas";
	var data = {
					'accion':accion
	};
	$.ajax({
				url:'./controladores/empresaController.php',
				type:'POST',
				cache:false,
				data:data,
				error:function(resp){
					console.log(resp);
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					$("#select_idioma_historia").html(recordset["opciones"]);
				}
	});
}
//--
function cargar_contenido_nosotros(idioma){
	var accion = "consultar_contenido_historia";
	var data = {
					'accion':accion,
					'idioma':idioma
	}
	$.ajax({
				url:'./controladores/nosotrosController.php',
				type:'POST',
				cache:false,
				data:data,
				error:function(resp){
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute;o un error inesperado", "alert-danger");
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					//alert(toMarkdown(recordset["contenido"]));
					if(recordset["contenido"]!=null){
						$("#md-footer").text(recordset["contenido"]);
						$("#markdown-editor").val(toMarkdown(recordset["contenido"]));
						$('#md-footer').show().html(recordset["contenido"]);
						$("#id_nosotros").val(recordset["id"]);
					}else{
						$("#md-footer").text("");
						$("#markdown-editor,#id_nosotros").val("");
					}
				}
	});
}
//--
function limpiar_nuestra_empresa(){
	$("#markdown-editor").val("");
	$("#md-footer").text("");
	$("#select_idioma_historia").val("0");
}