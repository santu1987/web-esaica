//--Bloque de eventos
cargar_lista_nosotros();
//--Bloque de funciones
//--Carga lista empresa
/*function cargar_lista_nosotros(){
	var accion = "consultar_nosotros";
	var data = {
					'accion':accion
	};
	$.ajax({
				url:'./controladores/nosotrosController.php',
				type:'POST',
				data: data,
				cache:false,
				error:function(resp){
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error inesperado", "alert-danger");
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					alert(resp);
					var tabla_dinamica = "";
					var i = 0;
					for(i=0;i<recordset.length;i++){
						tabla_dinamica+="<tr id='tab"+i+"' onclick='consultar_registro("+i+");' data='"+recordset[i]["id"]+"|"+recordset[i]["id_empresa"]+"|"+recordset[i]["id_idioma"]+"|"+recordset[i]["quienes_somos"]+"|"+recordset[i]["mision"]+"|"+recordset[i]["vision"]+"|"+recordset[i]["producto"]+"|"+recordset[i]["historia"]+"|"+recordset[i]["idioma"]+"'>\
											<td>"+i+"</td>\
											<td>"+text(recordset[i]["idioma"])+"</td>\
											<td>"+text(recordset[i]["quienes_somos"])+"</td>\
											<td>"+text(recordset[i]["mision"])+"</td>\
											<td>"+text(recordset[i]["vision"])+"</td>\
											<td>"+text(recordset[i]["producto"])+"</td>\
											<td></td>\
										</tr>";
					}	
					$("#tbody_lista_nosotros").html(tabla_dinamica);
					quitar_preloader();
					//--Para datatable...
					iniciar_datatable();
				}
	});
}*/
//--
//Para inicializa datatable
function iniciar_datatable(){
	"use strict";
	// Init Theme Core    
	Core.init();
	// Init Demo JS  
	Demo.init();
	$('#datatable2').dataTable({
	  "aoColumnDefs": [{
	    'bSortable': false,
	    'aTargets': [-1]
	  }],
	  "oLanguage": {
	    "oPaginate": {
	      "sPrevious": "",
	      "sNext": ""
	    }
	  },
	  "iDisplayLength": 5,
	  "aLengthMenu": [
	    [5, 10, 25, 50, -1],
	    [5, 10, 25, 50, "All"]
	  ],
	  "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
	  "oTableTools": {
	    "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
	  }
	});
	// Add Placeholder text to datatables filter bar
	$('.dataTables_filter input').attr("placeholder", "Ingrese dato a filtrar...");
}
