//-------------------------------
//Bloque de Eventos
//-------------------------------
$(document).ready(function(){
	carga_inicio();
	//$("#cuerpo_principal").load("site_media/templates/empresa.html");
	//--Opciones de menu
	$("#op_empresa").click(function(){
		$("#cuerpo_principal").load("site_media/templates/empresa.html");	
	});
	$("#op_nosotros").click(function(e){
		e.preventDefault();
		//--
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		//--
		//cargar_lista_nosotros();
	});
	//--
	$("#op_nos_empresa").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		cargar_nos_empresa();
	});
	//--
	$("#op_nos_mision").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		cargar_nos_mision();
	});
	//--
	$("#op_nos_vision").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		cargar_nos_vision();
	});
	//--
	$("#op_nos_redes").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		$("#cuerpo_principal").load("site_media/templates/redes.html");
	});
	//--
	$("#op_nos_historia").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		cargar_nos_historia();
	});
	//--
	$("#op_nos_productos").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		cargar_nos_productos();
	});
	//--
	$("#op_contactos").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		cargar_contacto();
	});
	//--
	$("#op_cv").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		cargar_cv();
	});
	$("#op_noticias").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		cargar_noticias();
	});
	$("#op_galeria_imagenes").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		carga_imagenes_form();
	});
	$("#op_galeria_videos").click(function(e){
		e.preventDefault();
		setTimeout(function(){
			mensaje_preloader("#campo_mensaje_lista");
		},2000);
		carga_videos_form();
	});
	//--
});
//-------------------------------
//Bloue de funciones
//-------------------------------
function carga_inicio(){
	var accion = "datos_usuario";
	var data = {'accion':accion};
	$.ajax({
				url:'./controladores/inicioController.php',
				type:'POST',
				data:data,
				cache:false,
				error:function(resp){
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					$("#nombre_usuario").text(recordset["nombre_usuario"]);
					$("#tipo_usuario,#tipo_usuario2").text(recordset["tipo_usuario"]);
				}
	});
}
function activar_btn(seccion){
	$("#op_empresa,#op_nosotros,#op_productos,#op_noticias,#op_galerias,#op_contactos").removeClass("activo_top").removeClass("activo_normal");
	switch(seccion){
		case 'empresa':
			$("#op_empresa").addClass("activo_top");
			break;
		case 'nosotros':
			$("#op_nosotros").addClass("activo_top");
			break;
		case 'productos':
			$("#op_productos").addClass("activo_top");
			break;
		case 'noticias':
			$("#op_noticias").addClass("activo_top");
			break;
		case 'galerias':
			$("#op_galerias").addClass("activo_top");
			break;
		case 'contactos':
			$("#op_contactos").addClass("activo_top");
			break;								
	}
}
//-----------------------------------------------------------------
function cargar_lista_nosotros(){
	var accion = "consultar_nosotros";
	var data = {
					'accion':accion
	};
	$.ajax({
				url:'./controladores/nosotrosController.php',
				type:'POST',
				data: data,
				cache:false,
				error:function(resp){
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error inesperado", "alert-danger");
				},
				success:function(resp){
					alert(resp);
					$("#cuerpo_principal").html(resp);
					quitar_preloader();
					//--Para datatable...
					iniciar_datatable();
				}
	});
}
//-------------------------------------------------------------------
function iniciar_datatable(){
	"use strict";
	// Init Theme Core    
	Core.init();
	// Init Demo JS  
	Demo.init();
	$('#datatable').dataTable({
	  "aoColumnDefs": [{
	    'bSortable': false,
	    'aTargets': [-1]
	  }],
	  "oLanguage": {
	    "oPaginate": {
	      "sPrevious": "",
	      "sNext": ""
	    }
	  },
	  "iDisplayLength": 5,
	  "aLengthMenu": [
	    [5, 10, 25, 50, -1],
	    [5, 10, 25, 50, "All"]
	  ],
	  "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
	  "oTableTools": {
	    "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
	  }
	});
	// Add Placeholder text to datatables filter bar
	$('.dataTables_filter input').attr("placeholder", "Ingrese dato a filtrar...");
}
//-------------------------------------------------------------------------------
function cargar_nos_empresa(){
	$("#cuerpo_principal").load("site_media/templates/nuestra_empresa.html");	
}
//-------------------------------------------------------------------------------
function cargar_nos_mision(){
	$("#cuerpo_principal").load("site_media/templates/mision.html");
}
//-------------------------------------------------------------------------------
function cargar_nos_vision(){
	$("#cuerpo_principal").load("site_media/templates/vision.html");
}
//-------------------------------------------------------------------------------
function cargar_nos_historia(){
	$("#cuerpo_principal").load("site_media/templates/historia.html");	
}
//-------------------------------------------------------------------------------
function cargar_nos_productos(){
	$("#cuerpo_principal").load("site_media/templates/productos.html");
}
//-------------------------------------------------------------------------------
function cargar_contacto(){
	var accion = 'cargar_lista_contactos';
	var data = {'accion':accion};
	$.ajax({
				url:'./controladores/contactosController.php',
				type:'POST',
				data:data,
				cache:false,
				error:function(resp){
					console.log(resp);
				},
				success: function(resp){
					$("#cuerpo_principal").html(resp);
					quitar_preloader();
					iniciar_datatable();
				}
	});
}
//----------------------------------------------------------------------------------
function cargar_cv(){
	$("#cuerpo_principal").load("site_media/templates/cv.html");
}
//----------------------------------------------------------------------------------
function cargar_noticias(){
	$("#cuerpo_principal").load("site_media/templates/noticias.html");
}
//----------------------------------------------------------------------------------
function carga_imagenes_form(){
	$("#cuerpo_principal").load("site_media/templates/imagenes.html");
}
//----------------------------------------------------------------------------------
function carga_videos_form(){
	$("#cuerpo_principal").load("site_media/templates/videos.html");
}
//----------------------------------------------------------------------------------