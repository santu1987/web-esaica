//---Bloque de Eventos
//-----------------------------------------------------
    "use strict";

    // Init Theme Core    
    Core.init();

    // Init Demo JS  
    Demo.init();

//-----------------------------------------------------
cargar_select_imagen();
$("#btn_limpiar_imagen").click(function(e){
	e.preventDefault();
	limpiar_imagen();
});
$("#btn_ver_listado_imagen").click(function(e){
	e.preventDefault();
	var accion = "consultar_listado_imagen";
	var data = {'accion':accion};
	$.ajax({
				url:'./controladores/imagenesController.php',
				type:'POST',
				cache:false,
				data:data,
				error:function(resp){
					console.log(resp);
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
				},
				success:function(resp){
					if(resp=="error"){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
					}else{
						$("#cuerpo_principal").html(resp);
						quitar_preloader();
						iniciar_datatable();
					}
				}
	});
});
$("#btn_guardar_imagen").click(function(){
	var id_imagen = $("#id_imagen").val();
	var idioma = $("#select_idioma_imagen").val();
	var contenido = $("#text_imagen").val();
	var accion = "guardar_imagen";
	var data = {
					'accion':accion,
					'id_imagen':id_imagen,
					'contenido':contenido,
					'idioma':idioma
	}
	if(validar_formulario_imagen()==true){
		$.ajax({
					url:'./controladores/imagenesController.php',
					type:'POST',
					cache:false,
					data:data,
					error: function(resp){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
					},
					success: function(resp){
						alert(resp);
						var recordset = $.parseJSON(resp);
						if(recordset[0]=='1'){
							//--------------------------------------------
							$("#id_imagen").val(recordset[1]);
							if($("#file_imagen").val()!=""){
								cargar_archivo("registro_exitoso");	
							}else{
								mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Registro exitoso", "alert-success");
								limpiar_imagen();
							}
							//-------------------------------------------
						}else
						if(recordset[0]=='2'){
							mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error en proceso de registro", "alert-danger");
						}else
						if(recordset[0]=='3'){
							//-------------------------------------------
							if($("#file_imagen").val()!=""){
								cargar_archivo("actualizacion_exitosa");
							}else{
								mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Actualizaci&oacute;n exitoso", "alert-success");
								limpiar_imagen();
							}
							//--------------------------------------------	
						}else
						if(recordset[0]=='4'){
							mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error en proceso de actualizaci&oacute;n", "alert-danger");
						}else
						if(recordset[0]=='-3'){
							mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> No existe el registro", "alert-danger");
						}
					}
		});
	}
});
//--Bloque de Funciones
//--
function validar_formulario_imagen(){
	if($("#text_imagen").val()==""){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe ingresar el texto relacionado a la imagen", "alert-danger");
		return false;
	}else
	if($("#select_idioma_imagen").val()=="0"){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe seleccionar idioma", "alert-danger");
		return false;
	}else
	if($("#file_imagen").val()==""){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe seleccionar idioma", "alert-danger");
		return false;
	}else{
		return true;
	}
}
//--
function cargar_select_imagen(){
	var accion = "consultar_select_idiomas";
	var data = {
					'accion':accion
	};
	$.ajax({
				url:'./controladores/empresaController.php',
				type:'POST',
				cache:false,
				data:data,
				error:function(resp){
					console.log(resp);
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					$("#select_idioma_imagen").html(recordset["opciones"]);
				}
	});
}
//--
function cargar_contenido_imagen(idioma){
	var accion = "consultar_contenido_noticia";
	var data = {
					'accion':accion,
					'idioma':idioma
	}
	$.ajax({
				url:'./controladores/noticiasController.php',
				type:'POST',
				cache:false,
				data:data,
				error:function(resp){
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute;o un error inesperado", "alert-danger");
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					//alert(toMarkdown(recordset["contenido"]));
					if(recordset["contenido"]!=null){
						$("#md-footer").text(recordset["contenido"]);
						$("#markdown-editor").val(toMarkdown(recordset["contenido"]));
						$('#md-footer').show().html(recordset["contenido"]);
						$("#id_noticia").val(recordset["id"]);
					}else{
						$("#md-footer").text("");
						$("#markdown-editor,#id_noticia").val("");
					}
				}
	});
}
//--
function limpiar_imagen(){
	$("#id_imagen,#text_imagen,#file_imagen").val("");
	$("#select_idioma_imagen").val("0");
}
//--
function cargar_archivo(valor){
      var formData = new FormData($("#form-ui")[0]);
      var message = "";   
      //hacemos la petición ajax  
      $.ajax({
                url:'./controladores/imagenesController.php',  
                type: 'POST',
                // Form data
                //datos del formulario
                data: formData,
                //necesario para subir archivos via ajax
                cache: false,
                contentType: false,
                processData: false,
                //mientras enviamos el archivo
                beforeSend: function(){
                    //message = $("<span class='before'>Subiendo la imagen, por favor espere...</span>");
                   // showMessage(message)         
                },
                //si ha ocurrido un error
                error: function()
                {
                    console.log(arguments);
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");                    
                },
                //una vez finalizado correctamente
                success: function(data)
                {
                    var recordset=data;
                    alert(recordset);
                    if((valor=="registro_exitoso")&&(recordset=="archivo_cargado"))
                    {
			           mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Registro exitoso", "alert-success");
                    	limpiar_imagen();
                    }else
                     if((valor=="actualizacion_exitosa")&&(recordset=="archivo_cargado"))
                    {
					   mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Actualizaci&oacute;n exitosa", "alert-success");
			       	   limpiar_imagen();	
			       	}else
                    if(recordset=="error_tipo_archivo")
                    {
                       	mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error- Carga imag&eacute;n: Solo puede subir imagenes en formato jpg, de tama&ntilde;o menor a 2 megas", "alert-danger");
                    }else
                    if(recordset=="error_no_carga")
                    {
   						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error- Carga imag&eacute;n", "alert-danger");
                    }else
                    if(recordset=="id_blanco")
                    {
   						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error- No se puede relacionar el registro con la imag&eacute;n", "alert-danger");
                    }else
                    if(recordset=="no_existe_registro")
                    {
   						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error- No se puede relacionar el registro con la imag&eacute;n", "alert-danger");
                    }  
                }
      });
}