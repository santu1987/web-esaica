//---Bloque de Eventos
//-----------------------------------------------------
    "use strict";

    // Init Theme Core    
    Core.init();

    // Init Demo JS  
    Demo.init();

//-----------------------------------------------------
cargar_select_idiomas();
$("#markdown-editor,#md-footer").text("");
$("#btn_limpiar_noticia").click(function(e){
	e.preventDefault();
	limpiar_noticia();
});
$("#btn_ver_listado_noticia").click(function(e){
	e.preventDefault();
	var accion = "consultar_listado_noticias";
	var data = {'accion':accion};
	$.ajax({
				url:'./controladores/noticiasController.php',
				type:'POST',
				cache:false,
				data:data,
				error:function(resp){
					console.log(resp);
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
				},
				success:function(resp){
					if(resp=="error"){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
					}else{
						$("#cuerpo_principal").html(resp);
						quitar_preloader();
						iniciar_datatable();
					}
				}
	});
});
$("#btn_guardar_noticia").click(function(){
	var id_noticia = $("#id_noticia").val();
	var idioma = $("#select_idioma_noticia").val();
	var titulo_noticia = $("#text_titulo_noticias").val();
	var contenido = $('#md-footer').html();
	var accion = "guardar_noticia";
	var data = {
					'accion':accion,
					'id_noticia':id_noticia,
					'contenido':contenido,
					'idioma':idioma,
					'titulo_noticia':titulo_noticia
	}
	if(validar_formulario_noticia()==true){
		$.ajax({
					url:'./controladores/noticiasController.php',
					type:'POST',
					cache:false,
					data:data,
					error: function(resp){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
					},
					success: function(resp){
						//alert(resp);
						var recordset = $.parseJSON(resp);
						if(recordset[0]=='1'){
							//--------------------------------------------
							$("#id_noticia").val(recordset[1]);
							if($("#file_imagen").val()!=""){
								cargar_archivo("registro_exitoso");	
							}else{
								mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Registro exitoso", "alert-success");
								limpiar_noticia();
							}
							//-------------------------------------------
						}else
						if(recordset[0]=='2'){
							mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error en proceso de registro", "alert-danger");
						}else
						if(recordset[0]=='3'){
							//-------------------------------------------
							if($("#file_imagen").val()!=""){
								cargar_archivo("actualizacion_exitosa");
							}else{
								mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Actualizaci&oacute;n exitoso", "alert-success");
								limpiar_noticia();
							}
							//--------------------------------------------	
						}else
						if(recordset[0]=='4'){
							mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error en proceso de actualizaci&oacute;n", "alert-danger");
						}else
						if(recordset[0]=='-3'){
							mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> No existe el registro", "alert-danger");
						}
					}
		});
	}
});
//--Bloque de Funciones
//--
/*function consultar_contacto(numero){
	alert(numero);
	/*var datos_noticias = $("#tab"+numero).attr("data");
	var datos = datos_noticias.split("|");
	$("#cuerpo_principal").load("./site_media/templates/noticias.html");
	/*setTimeout(function(){
		$("#id_noticia").val(datos[0]);
		$("#text_titulo_noticias").val(datos[3]);
		$("#select_idioma_noticia").val(datos[1]);
		$("#md-footer").text(datos[4]);
		$("#markdown-editor").val(datos[4]);
	},1000);
}*/
//--
function validar_formulario_noticia(){
	if($("#text_titulo_noticias").val()==""){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe ingresar el t&iacute;tulo de la noticia", "alert-danger");
		return false;
	}else
	if($("#select_idioma_noticia").val()=="0"){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe seleccionar idioma", "alert-danger");
		return false;
	}else
	if(($("#md-footer").text()=="")||($("#markdown-editor").val()=="")){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe ingresar el contenido de la noticia", "alert-danger");
		return false;
	}else
		return true;
}
//--
function cargar_select_idiomas(){
	var accion = "consultar_select_idiomas";
	var data = {
					'accion':accion
	};
	$.ajax({
				url:'./controladores/empresaController.php',
				type:'POST',
				cache:false,
				data:data,
				error:function(resp){
					console.log(resp);
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					$("#select_idioma_noticia").html(recordset["opciones"]);
				}
	});
}
//--
function cargar_contenido_noticia(idioma){
	var accion = "consultar_contenido_noticia";
	var data = {
					'accion':accion,
					'idioma':idioma
	}
	$.ajax({
				url:'./controladores/noticiasController.php',
				type:'POST',
				cache:false,
				data:data,
				error:function(resp){
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute;o un error inesperado", "alert-danger");
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					//alert(toMarkdown(recordset["contenido"]));
					if(recordset["contenido"]!=null){
						$("#md-footer").text(recordset["contenido"]);
						$("#markdown-editor").val(toMarkdown(recordset["contenido"]));
						$('#md-footer').show().html(recordset["contenido"]);
						$("#id_noticia").val(recordset["id"]);
					}else{
						$("#md-footer").text("");
						$("#markdown-editor,#id_noticia").val("");
					}
				}
	});
}
//--
function limpiar_noticia(){
	$("#id_noticia,#text_titulo_noticias,#markdown-editor,#file_imagen").val("");
	$("#md-footer").text("");
	$("#select_idioma_noticia").val("0");

}
//--
function cargar_archivo(valor)
{
      var formData = new FormData($("#form-ui")[0]);
      var message = "";   
      //hacemos la petición ajax  
      $.ajax({
                url:'./controladores/noticiasController.php',  
                type: 'POST',
                // Form data
                //datos del formulario
                data: formData,
                //necesario para subir archivos via ajax
                cache: false,
                contentType: false,
                processData: false,
                //mientras enviamos el archivo
                beforeSend: function(){
                    //message = $("<span class='before'>Subiendo la imagen, por favor espere...</span>");
                   // showMessage(message)         
                },
                //si ha ocurrido un error
                error: function()
                {
                    console.log(arguments);
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");                    
                },
                //una vez finalizado correctamente
                success: function(data)
                {
                    var recordset=data;
                    //alert(recordset);alert(valor);
                    if((valor=="registro_exitoso")&&(recordset=="archivo_cargado"))
                    {
			           mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Registro exitoso", "alert-success");
                    	limpiar_noticia();
                    }else
                     if((valor=="actualizacion_exitosa")&&(recordset=="archivo_cargado"))
                    {
					   mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Actualizaci&oacute;n exitosa", "alert-success");
			       	   limpiar_noticia();	
			       	}else
                    if(recordset=="error_tipo_archivo")
                    {
                       	mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error- Carga imag&eacute;n: Solo puede subir imagenes en formato jpg, de tama&ntilde;o menor a 2 megas", "alert-danger");
                    }else
                    if(recordset=="error_no_carga")
                    {
   						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error- Carga imag&eacute;n", "alert-danger");
                    }else
                    if(recordset=="id_blanco")
                    {
   						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error- No se puede relacionar el registro con la imag&eacute;n", "alert-danger");
                    }else
                    if(recordset=="no_existe_registro")
                    {
   						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error- No se puede relacionar el registro con la imag&eacute;n", "alert-danger");
                    }  
                }
      });
}
