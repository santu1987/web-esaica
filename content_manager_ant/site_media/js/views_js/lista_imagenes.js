$("#btn_nuevo_imagen").click(function(){
	$("#cuerpo_principal").load("site_media/templates/imagenes.html");
});
//--Bloque de funciones
function consultar_imagen(numero){
	var datos_noticias = $("#tab"+numero).attr("data");
	var datos = datos_noticias.split("|");
	$("#cuerpo_principal").load("./site_media/templates/imagenes.html");
	setTimeout(function(){
		$("#id_imagen").val(datos[0]);
		$("#text_imagen").val(datos[3]);
		$("#select_idioma_imagen").val(datos[1]);
	},1000);
}
//--
function publicar_imagen(numero){
	var datos_noticias = $("#tab"+numero).attr("data");
	var datos = datos_noticias.split("|");
	var id_noticia = datos[0];
	alert(id_noticia);
	var accion = 'publicar_imagen';
	var data = {
					'id_noticia':id_noticia,
					'accion':accion};
	$.ajax({
				url:'./controladores/imagenesController.php',
				type:'POST',
				data:data,
				cache:false,
				error:function(resp){
					console.log(resp);
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
				},	
				success:function(resp){
					var recordset = $.parseJSON(resp);
					if(recordset[0]==1){
						if(recordset[1]=="activar"){
							$("#icono_estatus_"+numero).removeClass("fa-cloud-upload").addClass("fa-cloud-download");
							$("#btn_estatus_"+numero).removeClass("btn-success").addClass("btn-warning");
							$("#btn_estatus_"+numero).attr({'title':'Inactivar publicación'});
							$("#div_estatus_"+numero).text("Publicado");
							mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Noticia publicada", "alert-success");
						}else{
							$("#icono_estatus_"+numero).removeClass("fa-cloud-download").addClass("fa-cloud-upload");
							$("#btn_estatus_"+numero).removeClass("btn-warning").addClass("btn-success");
							$("#btn_estatus_"+numero).attr({'title':'Publicar noticia'});
							$("#div_estatus_"+numero).text("Inactivo");
							mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Noticia inactiva", "alert-success");
						}
						
					}else if(recordset[0]==0){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
					}else if(recordset[0]==-1){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error en consulta de estatus ", "alert-danger");
					}
				}
	});				
}
//--