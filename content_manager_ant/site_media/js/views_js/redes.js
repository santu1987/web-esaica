//--Bloque de eventos
cargar_select_redes();
$("#btn_guardar_redes").click(function(e){
	e.preventDefault();
	var red_social = $("#text_url").val();
	var id_red = $("#id_red").val();
	var tipo_red = $("#select_redes").val();
	var accion = "registrar_red";
	var data = {
					'red_social':red_social,
					'id_red':id_red,
					'tipo_red':tipo_red,
					'accion':accion
	}
	if (validar_campos_redes()==true){
		$.ajax({
					url:'./controladores/redesController.php',
					type:'POST',
					data:data,
					cache:false,
					error: function(resp){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
					},
					success: function(resp){
						var recordset = $.parseJSON(resp);	
						//alert(resp);
						if(recordset[0]=='1'){
							//Registro exitoso...
							mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Registro exitoso", "alert-success");
							limpiar_campos_redes();
						}else if(recordset[0]=='-1'){
							//Error en proceso, ya existe el registro...
							mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error, ya existe el registro", "alert-danger");
						}
						else if(recordset[0]=='2'){
							//Error en proceso...
							mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error en proceso de registro", "alert-danger");
						}else if(recordset[0]=='3'){
							//...Actualizacion exitosa
							mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Actualizaci&oacute;n exitosa", "alert-success");
							limpiar_campos_redes();
						}else if(recordset[0]=='-3'){
							//Error en proceso, no existe el registro...
							mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error, no existe el registro", "alert-danger");
						}else if(recordset[0]=='4'){
							//...Error en actualizacion
							mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Error en proceso de actualizaci&oacute;n", "alert-danger");
						}
					}
		});
	}
});
$("#select_redes").change(function(){
	cargar_url();
});
$("#btn_limpiar_redes").click(function(e){
	limpiar_campos_redes();
});
//--Bloque de funciones
//--
function cargar_url(){
	var accion = "consultar_url";
	var tipo_red = $("#select_redes").val();
	var data = {
					'accion':accion,
					'tipo_red': tipo_red
	};
	$.ajax({
				url:'./controladores/redesController.php',
				type:'POST',
				cache:false,
				data:data,
				error:function(resp){
					console.log(resp);
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
				},
				success: function(resp){
					var recordset = $.parseJSON(resp);
					if(recordset["error"]==""){
						$("#text_url").val(recordset["url"]);
						$("#id_red").val(recordset["id"]);
					}else{
							mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");						
					}
				}
	});
}
//--
function cargar_select_redes(){
	var accion = "consultar_select_tipo_redes";
	var data = {
					'accion':accion
	};
	$.ajax({
				url:'./controladores/redesController.php',
				type:'POST',
				cache:false,
				data:data,
				error:function(resp){
					console.log(resp);
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					$("#select_redes").html(recordset["opciones"])
				}
	});
}
//--
function validar_campos_redes(){
	if($("#select_redes").val()=="0"){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe seleccionar al menos una red social", "alert-danger");
		return false;
	}if($("#text_url").val()==""){
		mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Debe ingresar la url", "alert-danger");
		return false;
	}else{
		return true;
	}
}
//--
function limpiar_campos_redes(){
	$("#select_redes").val(0);
	$("#text_url,#id_red").val("");
}
//--