$("#btn_nuevo_noticia").click(function(){
	$("#cuerpo_principal").load("site_media/templates/noticias.html");
});
//--Bloque de funciones
function consultar_noticia(numero){
	var datos_noticias = $("#tab"+numero).attr("data");
	var datos = datos_noticias.split("|");
	$("#cuerpo_principal").load("./site_media/templates/noticias.html");
	setTimeout(function(){
		$("#id_noticia").val(datos[0]);
		$("#text_titulo_noticias").val(datos[3]);
		$("#select_idioma_noticia").val(datos[1]);
		$('#md-footer').show().html(datos[4]);
		$("#markdown-editor").val(toMarkdown(datos[4]));
	},1000);
}
//--
function publicar_noticia(numero){
	var datos_noticias = $("#tab"+numero).attr("data");
	var datos = datos_noticias.split("|");
	var id_noticia = datos[0];
	alert(id_noticia);
	var accion = 'publicar_noticias';
	var data = {
					'id_noticia':id_noticia,
					'accion':accion};
	$.ajax({
				url:'./controladores/noticiasController.php',
				type:'POST',
				data:data,
				cache:false,
				error:function(resp){
					console.log(resp);
					mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
				},	
				success:function(resp){
					var recordset = $.parseJSON(resp);
					if(recordset[0]==1){
						if(recordset[1]=="activar"){
							$("#icono_estatus_"+numero).removeClass("fa-cloud-upload").addClass("fa-cloud-download");
							$("#btn_estatus_"+numero).removeClass("btn-success").addClass("btn-warning");
							$("#btn_estatus_"+numero).attr({'title':'Inactivar publicación'});
							$("#div_estatus_"+numero).text("Publicado");
							mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Noticia publicada", "alert-success");
						}else{
							$("#icono_estatus_"+numero).removeClass("fa-cloud-download").addClass("fa-cloud-upload");
							$("#btn_estatus_"+numero).removeClass("btn-warning").addClass("btn-success");
							$("#btn_estatus_"+numero).attr({'title':'Publicar noticia'});
							$("#div_estatus_"+numero).text("Inactivo");
							mensaje_alerta("#campo_mensaje","<i class='fa fa-check'></i> Noticia inactiva", "alert-success");
						}
						
					}else if(recordset[0]==0){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error inesperado", "alert-danger");
					}else if(recordset[0]==-1){
						mensaje_alerta("#campo_mensaje","<i class='fa fa-exclamation-circle'></i> Ocurri&oacute; un error en consulta de estatus ", "alert-danger");
					}
				}
	});				
}
//--