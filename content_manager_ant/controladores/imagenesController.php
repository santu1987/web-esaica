<?php
require_once("../modelos/imagenesModel.php");
require_once("../vistas_logicas/imagenesView.php");
//--Declaraciones
$mensajes = array();
//--
$nombre_archivo = $_FILES['file_imagen']['name'];
$tipo_archivo = $_FILES['file_imagen']['type'];
$tamano_archivo = $_FILES['file_imagen']['size'];
$tmp_name = $_FILES['file_imagen']['tmp_name'];
//--
$arreglo_datos = helper_userdata();
redireccionar_metodos($arreglo_datos,$nombre_archivo,$tipo_archivo,$tamano_archivo,$tmp_name);
//--
function redireccionar_metodos($arreglo_datos,$nombre_archivo,$tipo_archivo,$tamano_archivo,$tmp_name){
	switch ($arreglo_datos["accion"]) {
		case 'guardar_imagen':
			guardar_imagen($arreglo_datos);
			break;
		case 'cargar_imagen':
			cargar_imagen($arreglo_datos,$nombre_archivo,$tipo_archivo,$tamano_archivo,$tmp_name);
			break;
		case 'consultar_listado_imagen':
			consultar_listado_imagen();
			break;
		case 'publicar_imagen':
			publicar_imagen($arreglo_datos);
			break;					
	}	
}
//---
function helper_userdata(){
	$user_data = array();
	if($_POST){
		//--
		if(array_key_exists('accion', $_POST)){
			$user_data["accion"] = $_POST["accion"];
		}else{
			$user_data["accion"] = "cargar_imagen";
		}
		if(array_key_exists('id_imagen', $_POST)){
			$user_data["id_imagen"] = $_POST["id_imagen"];
		}
		if(array_key_exists('idioma', $_POST)){
			$user_data["idioma"] = $_POST["idioma"];
		}
		if(array_key_exists('contenido', $_POST)){
			$user_data["contenido"] = $_POST["contenido"];
		}
		//--
	}
	return $user_data;
}
//------------------------------------------------------
function guardar_imagen($arreglo_datos){
	$recordset = array();
	$arreglo_retorno = array();
	$existe = array();
	$obj = new imagenesModel();
	if($arreglo_datos["id_imagen"]==""){
	//-------------------------------------
	//Para guardar
		$recordset = $obj->registrar_imagen($arreglo_datos);
		if(($recordset=="error-1")or($recordset=="error-2")){
			$arreglo_retorno[0]=2;//error en registro
		}else{
			$arreglo_retorno[0]=1;//registro exitoso....
			$arreglo_retorno[1]=$recordset;//id del registro
		}
	//-------------------------------------	
	}else{
	//--------------------------------------	
	//Para modificar
		$existe = $obj->existe_imagen($arreglo_datos);
		if($existe[0][0]>0){
			$recordset = $obj->actualizar_imagen($arreglo_datos);
			if($recordset==true){
				$arreglo_retorno[0]=3;//registro exitoso....
			}else if(($recordset=="error-1")or($recordset=="error-2")){
				$arreglo_retorno[0]=4;//error en registro....
			}
		}else{
			$arreglo_retorno[0]=-3;//no existe registro....	
		}	
	//--------------------------------------	
	}
	die(json_encode($arreglo_retorno));
}
//------------------------------------------------------
function cargar_imagen($arreglo_datos,$nombre_archivo,$tipo_archivo,$tamano_archivo,$tmp_name){
	//--0.Instancio el obejto...
	$obj = new imagenesModel();
	//--1.verifico que el id no este en blanco
	if($arreglo_datos["id_imagen"]==""){
		die("id_blanco");
	}
	//--2.verificio que exista
	$existe = $obj->existe_imagen($arreglo_datos);
	if($existe[0][0]==0){
		die("no_existe_registro");
	} 
	//--Servidor
	//$serv = $_SERVER['DOCUMENT_ROOT']."/nueva_web/esaica/content_manager/site_media/img/noticias/";
	//--Local:
	$serv = $_SERVER['DOCUMENT_ROOT']."/esaica/site_media/img/galeria/";
	//valido el nombre del archivo segun el id
	$nombre_archivo2="imagen_galeria".$arreglo_datos["id_imagen"].".jpg";
	//creo la ruta
	$ruta=$serv.$nombre_archivo2;
	//valido que la imagen solo sea tipo jpg...
	if (!((strpos($tipo_archivo, "jpeg") || strpos($tipo_archivo, "jpg")||(strpos($tipo_archivo, "JPG"))) && ($tamano_archivo < 1000000000))) 
	{
		die("error_tipo_archivo");
	}else
	{
		//muevo la imagen de directorio
		if (move_uploaded_file($tmp_name,$ruta))
		{
		  	chmod($ruta,0777);//permisos al directorio
		  	$rs=$obj->ac_imagen_galeria($nombre_archivo2,$arreglo_datos["id_imagen"]);
		  	//die($rs);
		  	if($rs!=true)
		  	{
		  		die("error_bd");
		  	}
		  	else
		  	{
			    die("archivo_cargado");
		  	}

		}else
		{
		   die("error_no_carga");
		}
	}
	//--
}
//------------------------------------------------------
function consultar_listado_imagen(){
	$recordset = array();
	$arreglo_datos = array();
	$obj = new imagenesModel();
	$recordset = $obj->consultar_imagenes_lista();
	if($recordset!="error"){
		render_vista_consulta("lista_imagenes",$recordset);
	}else{
		$recordset="error";
		die($recordset);
	}
}
//------------------------------------------------------
function publicar_imagen($arreglo_datos){

	$recordset = array();
	$recordset["0"] = "EJemplo";
	$arreglo_retorno = array();
	$obj = new imagenesModel();
	$estatus = $obj->consultar_estatus($arreglo_datos['id_imagen']);
	//die(json_encode($estatus));
	if ($estatus!="error"){
	//-----------------------
		if($estatus[0][0]==1){
			$recordset = $obj->activar_inactivar_imagen($arreglo_datos['id_imagen'],0);
			$arreglo_retorno[1]="inactivar";
		}else
		if($estatus[0][0]==0){
			$recordset = $obj->activar_inactivar_imagen($arreglo_datos['id_imagen'],1);
			$arreglo_retorno[1]="activar";
		}
		//--
		if($recordset==true){
			$arreglo_retorno[0]=1; //Proceso exitoso...
		}else
		if($recordset==false){
			$arreglo_retorno[0]=0; //Error en proceso ...
		}
		//--
		
	//-----------------------
	}else{
		$arreglo_retorno[0] = -1; //Error en consulta de estatus...
	}
	die(json_encode($arreglo_retorno));
}
//-------------------------------------------------------
?>