<?php
require_once("../modelos/redesModel.php");
//--Declaraciones
$mensajes = array();
$arreglo_datos = helper_userdata();
redireccionar_metodos($arreglo_datos);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'consultar_select_tipo_redes':
			consultar_tipo_redes();
			break;
		case 'registrar_red':
			guardar_redes($arreglo_datos);	 			
			break;
		case 'consultar_url':
			consultar_url($arreglo_datos);
			break;	
	}	
}
//---
function helper_userdata(){
	$user_data = array();
	if($_POST){
		//--
		if(array_key_exists('accion', $_POST)){
			$user_data["accion"] = $_POST["accion"];
		}
		//--Para redes
		if(array_key_exists('red_social', $_POST)){
			$user_data["red_social"] = $_POST["red_social"];
		}
		if(array_key_exists('id_red', $_POST)){
			$user_data["id_red"] = $_POST["id_red"];
		}
		if(array_key_exists('tipo_red', $_POST)){
			$user_data["tipo_red"] = $_POST["tipo_red"];
		}
		//--
	}
	return $user_data;
}
//------------------------------------------------------
function consultar_tipo_redes(){
	$recordset = array();
	$arreglo = array();
	$obj = new redesModel();
	$recordset = $obj->consultar_select_redes();
	$select_redes = "<option value='0' >--Seleccione una red social--</option>";
	for($i=0;$i<count($recordset);$i++){
		$select_redes.="<option value='".$recordset[$i][0]."'>".$recordset[$i][1]."</option>";
	}
	$arreglo["opciones"] = $select_redes;
	die(json_encode($arreglo));
}
//------------------------------------------------------
function consultar_url($arreglo_datos){
	$recordset = array();
	$arreglo = array();
	$obj = new redesModel();
	$recordset = $obj->consultar_url_redes($arreglo_datos["tipo_red"]);
	//die(json_encode($resp));
	if($recordset[0][0]!="error"){
		$arreglo["url"] = $recordset[0][1];
		$arreglo["id"] = $recordset[0][0];
		$arreglo["error"] = "";
	}else{
		$arreglo["error"]="error";
	}
	die(json_encode($arreglo));	
}
//------------------------------------------------------
function guardar_redes($arreglo_datos){
	$recordset = array();
	$arreglo_retorno = array();
	$existe = array();
	$obj = new redesModel();
	if($arreglo_datos["id_red"]==""){
	//-------------------------------------
	//Para guardar
		$existe = $obj->existe_redes($arreglo_datos["tipo_red"]);
		if($existe[0][0]==0){
		//------------------
			$recordset = $obj->registrar_redes($arreglo_datos);
			if($recordset==true){
				$arreglo_retorno[0]=1;//registro exitoso....
			}else{
				$arreglo_retorno[0]=2;//error en registro....
			}
		//------------------	
		}else{
			$arreglo_retorno[0]=-1;//existe registro....
		}
	//-------------------------------------	
	}else{
	//--------------------------------------	
	//Para modificar
		$existe = $obj->existe_redes($arreglo_datos["tipo_red"]);
		if($existe[0][0]>0){
			$recordset = $obj->actualizar_redes($arreglo_datos);
			if($recordset==true){
				$arreglo_retorno[0]=3;//registro exitoso....
			}else{
				$arreglo_retorno[0]=4;//error en registro....
			}
		}else{
			$arreglo_retorno[0]=-3;//no existe registro....	
		}	
	//--------------------------------------	
	}
	die(json_encode($arreglo_retorno));
}
//------------------------------------------------------
?>