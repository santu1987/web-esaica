<?php
require_once("../modelos/nosotrosModel.php");
require_once("../vistas_logicas/noticiasView.php");
//--Declaraciones
$mensajes = array();
$arreglo_datos = helper_userdata();
redireccionar_metodos($arreglo_datos);
//--
function redireccionar_metodos($arreglo_datos){
	switch ($arreglo_datos["accion"]) {
		case 'consultar_nosotros':
			consultar_nosotros();
			break;
		case 'consultar_contenido_nuestra_empresa':
			consultar_contenido_nuestra_empresa($arreglo_datos["idioma"]);
			break;
		case 'guardar_nuestra_empresa':
			guardar_nuestra_empresa($arreglo_datos);
			break;
		case 'consultar_contenido_mision':
			consultar_contenido_mision($arreglo_datos["idioma"]);
			break;	
		case 'guardar_mision':
			guardar_mision($arreglo_datos);
			break;
		case 'consultar_contenido_vision':
			consultar_contenido_vision($arreglo_datos["idioma"]);
			break;	
		case 'guardar_vision':
			guardar_vision($arreglo_datos);
			break;
		case 'consultar_contenido_historia':
			consultar_contenido_historia($arreglo_datos["idioma"]);
			break;	
		case 'guardar_historia':
			guardar_historia($arreglo_datos);
			break;
		case 'consultar_contenido_producto':
			consultar_contenido_producto($arreglo_datos["idioma"]);
			break;	
		case 'guardar_producto':
			guardar_producto($arreglo_datos);
			break;
		case 'consultar_contenido_cv':
			consultar_contenido_cv($arreglo_datos);
			break;	
		case 'guardar_cv':
			guardar_cv($arreglo_datos);
			break;								
	}		
}
//---
function helper_userdata(){
	$user_data = array();
	if($_POST){
		//--
		if(array_key_exists('accion', $_POST)){
			$user_data["accion"] = $_POST["accion"];
		}
		if(array_key_exists('idioma', $_POST)){
			$user_data["idioma"] = $_POST["idioma"];
		}
		if(array_key_exists('contenido', $_POST)){
			$user_data["contenido"] = $_POST["contenido"];
		}
		if(array_key_exists('id', $_POST)){
			$user_data["id"] = $_POST["id"];
		}
		if(array_key_exists('persona', $_POST)){
			$user_data["persona"] = $_POST["persona"];
		}
		//--
	}
	return $user_data;
}
//------------------------------------------------------
function consultar_nosotros(){
	$recordset = array();
	$arreglo_valor = array();
	$obj = new nosotrosModel();
	$recordset = $obj->consultar_nosotros_lista();
	if($recordset!="error"){
		/*for($i=0;$i<count($recordset);$i++){
			$arreglo_valor[$i]["id"] = $recordset[$i][0];
			$arreglo_valor[$i]["id_empresa"] = $recordset[$i][1];
			$arreglo_valor[$i]["id_idioma"] = $recordset[$i][2];
			$arreglo_valor[$i]["quienes_somos"] = $recordset[$i][3];
			$arreglo_valor[$i]["mision"] = $recordset[$i][4];
			$arreglo_valor[$i]["vision"] = $recordset[$i][5];
			$arreglo_valor[$i]["producto"] = $recordset[$i][6];
			$arreglo_valor[$i]["historia"] = $recordset[$i][7];
			$arreglo_valor[$i]["idioma"] = $recordset[$i][8];
		//--------------------------------------------------------------
			//substr...
			$arreglo_valor[$i]["quienes_somos2"] = substr($arreglo_valor[$i]["quienes_somos"],0,20);
			$arreglo_valor[$i]["mision2"] = substr($arreglo_valor[$i]["mision"],0,20);
			$arreglo_valor[$i]["vision2"] = substr($arreglo_valor[$i]["vision"],0,20);
			$arreglo_valor[$i]["producto2"] = substr($arreglo_valor[$i]["producto"],0,20);
			$arreglo_valor[$i]["historia2"] = substr($arreglo_valor[$i]["historia"],0,20);
			$arreglo_valor[$i]["idioma2"] = substr($arreglo_valor[$i]["idioma"],0,20);	
		//--------------------------------------------------------------	
		}*/
		render_vista_consulta("lista_nosotros",$recordset);	
	}else
	{
		$recordset[0]="error";
		die(json_encode($recordset));
	}
}
//--
function consultar_contenido_nuestra_empresa($idioma){
	$recordset = array();
	$arreglo_valor = array();
	$obj = new nosotrosModel();
	$recordset = $obj->consultar_nuestra_empresa_texto($idioma);
	if($recordset!="error"){
		$arreglo_valor["error"] = "";
		$arreglo_valor["id"] = $recordset[0][0];
		$arreglo_valor["contenido"] = $recordset[0][1];
	}else{
		$arreglo_valor["error"]="error";
	}
	die(json_encode($arreglo_valor));
}
//--
function guardar_nuestra_empresa($arreglo_datos){
	$recordset = array();
	$arreglo_valor = array();
	$obj = new nosotrosModel();
	if($arreglo_datos["id"]==""){
		//-----------------------
		//--Para guardar
		$recordset = $obj->guardar_nuestra_empresa_contenido($arreglo_datos);
		//die(json_encode($recordset));
		if($recordset==true){
			$arreglo_valor[0]=1;//Registro exitoso...
		}else{
			$arreglo_valor[0]=0;//Ocurrio un error inesperado al registrar...
		}
	}
	else if($arreglo_datos["id"]!=""){
		//-----------------------
		//--Para actualizar
		$recordset = $obj->actualizar_nuestra_empresa_contenido($arreglo_datos);
		//die(json_encode($recordset));
		if($recordset==true){
			$arreglo_valor[0]=2;//Actualizacion exitosa...
		}else{
			$arreglo_valor[0]=3;//Ocurrio un error inesperado al actualizar...
		}
		//-----------------------
	}
	die(json_encode($arreglo_valor));
}
//--
//---Bloque Nosotros.Mision-----------------------------------------------------------
function consultar_contenido_mision($idioma){
	$recordset = array();
	$arreglo_valor = array();
	$obj = new nosotrosModel();
	$recordset = $obj->consultar_mision_texto($idioma);
	if($recordset!="error"){
		$arreglo_valor["error"] = "";
		$arreglo_valor["id"] = $recordset[0][0];
		$arreglo_valor["contenido"] = $recordset[0][1];
	}else{
		$arreglo_valor["error"]="error";
	}
	die(json_encode($arreglo_valor));
}
//--
function guardar_mision($arreglo_datos){
	$recordset = array();
	$arreglo_valor = array();
	$obj = new nosotrosModel();
	if($arreglo_datos["id"]==""){
		//-----------------------
		//--Para guardar
		$recordset = $obj->guardar_mision_contenido($arreglo_datos);
		//die(json_encode($recordset));
		if($recordset==true){
			$arreglo_valor[0]=1;//Registro exitoso...
		}else{
			$arreglo_valor[0]=0;//Ocurrio un error inesperado al registrar...
		}
	}
	else if($arreglo_datos["id"]!=""){
		//-----------------------
		//--Para actualizar
		$recordset = $obj->actualizar_mision_contenido($arreglo_datos);
		//die(json_encode($recordset));
		if($recordset==true){
			$arreglo_valor[0]=2;//Actualizacion exitosa...
		}else{
			$arreglo_valor[0]=3;//Ocurrio un error inesperado al actualizar...
		}
		//-----------------------
	}
	die(json_encode($arreglo_valor));
}
//--------------------------------------------------------------------------------------
//---Bloque Nosotros.Vision-----------------------------------------------------------
function consultar_contenido_vision($idioma){
	$recordset = array();
	$arreglo_valor = array();
	$obj = new nosotrosModel();
	$recordset = $obj->consultar_vision_texto($idioma);
	if($recordset!="error"){
		$arreglo_valor["error"] = "";
		$arreglo_valor["id"] = $recordset[0][0];
		$arreglo_valor["contenido"] = $recordset[0][1];
	}else{
		$arreglo_valor["error"]="error";
	}
	die(json_encode($arreglo_valor));
}
//--
function guardar_vision($arreglo_datos){
	$recordset = array();
	$arreglo_valor = array();
	$obj = new nosotrosModel();
	if($arreglo_datos["id"]==""){
		//-----------------------
		//--Para guardar
		$recordset = $obj->guardar_vision_contenido($arreglo_datos);
		//die(json_encode($recordset));
		if($recordset==true){
			$arreglo_valor[0]=1;//Registro exitoso...
		}else{
			$arreglo_valor[0]=0;//Ocurrio un error inesperado al registrar...
		}
	}
	else if($arreglo_datos["id"]!=""){
		//-----------------------
		//--Para actualizar
		$recordset = $obj->actualizar_vision_contenido($arreglo_datos);
		//die(json_encode($recordset));
		if($recordset==true){
			$arreglo_valor[0]=2;//Actualizacion exitosa...
		}else{
			$arreglo_valor[0]=3;//Ocurrio un error inesperado al actualizar...
		}
		//-----------------------
	}
	die(json_encode($arreglo_valor));
}
//--------------------------------------------------------------------------------------
//---Bloque Nosotros.Historia-----------------------------------------------------------
function consultar_contenido_historia($idioma){
	$recordset = array();
	$arreglo_valor = array();
	$obj = new nosotrosModel();
	$recordset = $obj->consultar_historia_texto($idioma);
	if($recordset!="error"){
		$arreglo_valor["error"] = "";
		$arreglo_valor["id"] = $recordset[0][0];
		$arreglo_valor["contenido"] = $recordset[0][1];
	}else{
		$arreglo_valor["error"]="error";
	}
	die(json_encode($arreglo_valor));
}
//--------------------------------------------------------------------------------------
function guardar_historia($arreglo_datos){
	$recordset = array();
	$arreglo_valor = array();
	$obj = new nosotrosModel();
	if($arreglo_datos["id"]==""){
		//-----------------------
		//--Para guardar
		$recordset = $obj->guardar_historia_contenido($arreglo_datos);
		//die(json_encode($recordset));
		if($recordset==true){
			$arreglo_valor[0]=1;//Registro exitoso...
		}else{
			$arreglo_valor[0]=0;//Ocurrio un error inesperado al registrar...
		}
	}
	else if($arreglo_datos["id"]!=""){
		//-----------------------
		//--Para actualizar
		$recordset = $obj->actualizar_historia_contenido($arreglo_datos);
		//die(json_encode($recordset));
		if($recordset==true){
			$arreglo_valor[0]=2;//Actualizacion exitosa...
		}else{
			$arreglo_valor[0]=3;//Ocurrio un error inesperado al actualizar...
		}
		//-----------------------
	}
	die(json_encode($arreglo_valor));
}
//--------------------------------------------------------------------------------------
//---Bloque Nosotros.Producto-----------------------------------------------------------
function consultar_contenido_producto($idioma){
	$recordset = array();
	$arreglo_valor = array();
	$obj = new nosotrosModel();
	$recordset = $obj->consultar_producto_texto($idioma);
	if($recordset!="error"){
		$arreglo_valor["error"] = "";
		$arreglo_valor["id"] = $recordset[0][0];
		$arreglo_valor["contenido"] = $recordset[0][1];
	}else{
		$arreglo_valor["error"]="error";
	}
	die(json_encode($arreglo_valor));
}
//--------------------------------------------------------------------------------------
function guardar_producto($arreglo_datos){
	$recordset = array();
	$arreglo_valor = array();
	$obj = new nosotrosModel();
	if($arreglo_datos["id"]==""){
		//-----------------------
		//--Para guardar
		$recordset = $obj->guardar_producto_contenido($arreglo_datos);
		//die(json_encode($recordset));
		if($recordset==true){
			$arreglo_valor[0]=1;//Registro exitoso...
		}else{
			$arreglo_valor[0]=0;//Ocurrio un error inesperado al registrar...
		}
	}
	else if($arreglo_datos["id"]!=""){
		//-----------------------
		//--Para actualizar
		$recordset = $obj->actualizar_producto_contenido($arreglo_datos);
		//die(json_encode($recordset));
		if($recordset==true){
			$arreglo_valor[0]=2;//Actualizacion exitosa...
		}else{
			$arreglo_valor[0]=3;//Ocurrio un error inesperado al actualizar...
		}
		//-----------------------
	}
	die(json_encode($arreglo_valor));
}
//--------------------------------------------------------------------------------------
//---Bloque Curriculum-----------------------------------------------------------
function consultar_contenido_cv($arreglo_datos){
	$recordset = array();
	$arreglo_valor = array();
	$obj = new nosotrosModel();
	$recordset = $obj->consultar_cv_texto($arreglo_datos);
	if($recordset!="error"){
		$arreglo_valor["error"] = "";
		$arreglo_valor["id"] = $recordset[0][0];
		$arreglo_valor["contenido"] = $recordset[0][1];
	}else{
		$arreglo_valor["error"]="error";
	}
	die(json_encode($arreglo_valor));
}
//--------------------------------------------------------------------------------------
function guardar_cv($arreglo_datos){
	$recordset = array();
	$arreglo_valor = array();
	$obj = new nosotrosModel();
	if($arreglo_datos["id"]==""){
		//-----------------------
		//--Para guardar
		$recordset = $obj->guardar_cv_contenido($arreglo_datos);
		//die(json_encode($recordset));
		if($recordset==true){
			$arreglo_valor[0]=1;//Registro exitoso...
		}else{
			$arreglo_valor[0]=0;//Ocurrio un error inesperado al registrar...
		}
	}
	else if($arreglo_datos["id"]!=""){
		//-----------------------
		//--Para actualizar
		$recordset = $obj->actualizar_cv_contenido($arreglo_datos);
		//die(json_encode($recordset));
		if($recordset==true){
			$arreglo_valor[0]=2;//Actualizacion exitosa...
		}else{
			$arreglo_valor[0]=3;//Ocurrio un error inesperado al actualizar...
		}
		//-----------------------
	}
	die(json_encode($arreglo_valor));
}
//--------------------------------------------------------------------------------------
?>