<?php
require_once("../core/conex.php");
class nosotrosModel extends Conex{
	private $rs;
	//--Metodo constructor...
	public function __construct(){
	}
	//Consultar nuestra empresa
	public function consultar_nuestra_empresa_texto($idioma){
		$sql = "SELECT 
						id,
						quienes_somos
				FROM 
						tbl_nosotros
				WHERE
						id_idioma='".$idioma."';";
				$this->rs = $this->procesarQuery($sql);
				//return $sql;
				return $this->rs;		
	}
	//Consultar nosotros lista
	public function consultar_nosotros_lista(){
		$sql = "SELECT 
					a.id AS id,
					a.id_empresa AS id_empresa,
					a.id_idioma AS id_idioma,
					a.quienes_somos AS quienes_somos,
					a.mision AS mision,
					a.vision AS vision,
					a.producto AS producto,
					a.historia AS historia,
					b.nombre AS nombre_empresa,
					c.idioma AS nombre_idioma
				 FROM 
				 	tbl_nosotros a
				 INNER JOIN 
				 	tbl_empresa b
				 ON 
				 	b.id= a.id_empresa
				 INNER JOIN 
				 	tbl_idiomas c
				 ON 
				 	c.id = a.id_idioma			
				 ORDER BY 
				 	id";
		$this->rs = $this->procesarQuery($sql);
		//return $sql....
		return $this->rs;		 	
	}
	//--Para guardar registro de nuestra empresa
	public function guardar_nuestra_empresa_contenido($arreglo_datos){
		$sql = 'INSERT INTO 
					tbl_nosotros
				(
					quienes_somos,
					id_idioma,
					id_empresa
				)
				VALUES
				(
					"'.$arreglo_datos['contenido'].'",
					"'.$arreglo_datos['idioma'].'",
					"1"
				);';
		$this->rs = $this->procesarQuery2($sql);
		//return $sql;
		return $this->rs;
	}
	//--Para actualizar registro de nuestra empresa
	public function actualizar_nuestra_empresa_contenido($arreglo_datos){
		$sql = "UPDATE 
					tbl_nosotros
				SET
					quienes_somos='".$arreglo_datos['contenido']."'
				WHERE
					id='".$arreglo_datos['id']."';";
		$this->rs = $this->procesarQuery2($sql);
		//return $sql;
		return $this->rs;	
	}
	//--
	//--Bloque de Nosotros.mision
	//--Para consultar contenido de mision
	public function consultar_mision_texto($idioma){
		$sql = "SELECT 
						id,
						mision
				FROM 
						tbl_nosotros
				WHERE
						id_idioma='".$idioma."';";
				$this->rs = $this->procesarQuery($sql);
				//return $sql;
				return $this->rs;		
	}
	//--Para guardar registro de mision
	public function guardar_mision_contenido($arreglo_datos){
		$sql = 'INSERT INTO 
					tbl_nosotros
				(
					mision,
					id_idioma,
					id_empresa
				)
				VALUES
				(
					"'.$arreglo_datos['contenido'].'",
					"'.$arreglo_datos['idioma'].'",
					"1"
				);';
		$this->rs = $this->procesarQuery2($sql);
		//return $sql;
		return $this->rs;
	}
	//--Para actualizar registro de mision
	public function actualizar_mision_contenido($arreglo_datos){
		$sql = "UPDATE 
					tbl_nosotros
				SET
					mision='".$arreglo_datos['contenido']."'
				WHERE
					id='".$arreglo_datos['id']."';";
		$this->rs = $this->procesarQuery2($sql);
		//return $sql;
		return $this->rs;	
	}
	//--
	//--Bloque de Nosotros.vision
	//--Para consultar contenido de vision
	public function consultar_vision_texto($idioma){
		$sql = "SELECT 
						id,
						vision
				FROM 
						tbl_nosotros
				WHERE
						id_idioma='".$idioma."';";
				$this->rs = $this->procesarQuery($sql);
				//return $sql;
				return $this->rs;		
	}
	//--Para guardar registro de vision
	public function guardar_vision_contenido($arreglo_datos){
		$sql = 'INSERT INTO 
					tbl_nosotros
				(
					vision,
					id_idioma,
					id_empresa
				)
				VALUES
				(
					"'.$arreglo_datos['contenido'].'",
					"'.$arreglo_datos['idioma'].'",
					"1"
				);';
		$this->rs = $this->procesarQuery2($sql);
		//return $sql;
		return $this->rs;
	}
	//--Para actualizar registro de mision
	public function actualizar_vision_contenido($arreglo_datos){
		$sql = "UPDATE 
					tbl_nosotros
				SET
					vision='".$arreglo_datos['contenido']."'
				WHERE
					id='".$arreglo_datos['id']."';";
		$this->rs = $this->procesarQuery2($sql);
		//return $sql;
		return $this->rs;	
	}
	//--
	//--Bloque de Nosotros.Historia
	//--Para consultar contenido de historia
	public function consultar_historia_texto($idioma){
		$sql = "SELECT 
						id,
						historia
				FROM 
						tbl_nosotros
				WHERE
						id_idioma='".$idioma."';";
				$this->rs = $this->procesarQuery($sql);
				//return $sql;
				return $this->rs;		
	}
	//--Para guardar registro de historia
	public function guardar_historia_contenido($arreglo_datos){
		$sql = 'INSERT INTO 
					tbl_nosotros
				(
					historia,
					id_idioma,
					id_empresa
				)
				VALUES
				(
					"'.$arreglo_datos['contenido'].'",
					"'.$arreglo_datos['idioma'].'",
					"1"
				);';
		$this->rs = $this->procesarQuery2($sql);
		//return $sql;
		return $this->rs;
	}
	//--Para actualizar registro de historia
	public function actualizar_historia_contenido($arreglo_datos){
		$sql = "UPDATE 
					tbl_nosotros
				SET
					historia='".$arreglo_datos['contenido']."'
				WHERE
					id='".$arreglo_datos['id']."';";
		$this->rs = $this->procesarQuery2($sql);
		//return $sql;
		return $this->rs;	
	}
	//--
	//--Bloque de Nosotros.Producto
	//--Para consultar contenido de productos
	public function consultar_producto_texto($idioma){
		$sql = "SELECT 
						id,
						producto
				FROM 
						tbl_nosotros
				WHERE
						id_idioma='".$idioma."';";
				$this->rs = $this->procesarQuery($sql);
				//return $sql;
				return $this->rs;		
	}
	//--Para guardar registro de historia
	public function guardar_producto_contenido($arreglo_datos){
		$sql = 'INSERT INTO 
					tbl_nosotros
				(
					producto,
					id_idioma,
					id_empresa
				)
				VALUES
				(
					"'.$arreglo_datos['contenido'].'",
					"'.$arreglo_datos['idioma'].'",
					"1"
				);';
		$this->rs = $this->procesarQuery2($sql);
		//return $sql;
		return $this->rs;
	}
	//--Para actualizar registro de historia
	public function actualizar_producto_contenido($arreglo_datos){
		$sql = "UPDATE 
					tbl_nosotros
				SET
					producto='".$arreglo_datos['contenido']."'
				WHERE
					id='".$arreglo_datos['id']."';";
		$this->rs = $this->procesarQuery2($sql);
		//return $sql;
		return $this->rs;	
	}
	//--
	//--Bloque de Curriculum
	//--Para consultar contenido de productos
	public function consultar_cv_texto($arreglo_datos){
		$sql = "SELECT 
						id,
						descripcion
				FROM 
						tbl_cv
				WHERE
						id_idioma='".$arreglo_datos["idioma"]."'
				AND 
						id_persona='".$arreglo_datos["persona"]."';";
				$this->rs = $this->procesarQuery($sql);
				//return $sql;
				return $this->rs;		
	}
	//--Para guardar registro de cv
	public function guardar_cv_contenido($arreglo_datos){
		$sql = 'INSERT INTO 
					tbl_cv
				(
					descripcion,
					id_idioma,
					id_persona
				)
				VALUES
				(
					"'.$arreglo_datos['contenido'].'",
					"'.$arreglo_datos['idioma'].'",
					"'.$arreglo_datos['persona'].'"
				);';
		$this->rs = $this->procesarQuery2($sql);
		//return $sql;
		return $this->rs;
	}
	//--Para actualizar registro de cv
	public function actualizar_cv_contenido($arreglo_datos){
		$sql = "UPDATE 
					tbl_cv
				SET
					descripcion='".$arreglo_datos['contenido']."'
				WHERE
					id='".$arreglo_datos['id']."';";
		$this->rs = $this->procesarQuery2($sql);
		//return $sql;
		return $this->rs;	
	}
	//--
}
?>