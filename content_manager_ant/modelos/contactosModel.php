<?php
require_once("../core/conex.php");
class contactosModel extends Conex{
	private $rs;
	//--Metodo constructor...
	public function __construct(){
	}
	//--
	public function consultar_contactos_lista(){
		$sql = "SELECT 
						a.id,
						a.nombres,
						a.apellidos,
						a.telefono,
						a.correo,
						a.pais,
						a.mensaje
				FROM 
						tbl_contactos a";
		$this->rs = $this->procesarQuery($sql);
		return $this->rs;					
	}
}	
?>