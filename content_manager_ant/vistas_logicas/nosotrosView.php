<?php
require("../core/fbasic.php");
//--Armo diccionario 
function render_list($html,$data){
//---------------------------------------------------------------------------------------
	$match_cal = set_match_identificador_dinamico($html,"<!--row_nosotros-->");
	if($data!="error"){
		for($i=0;$i<count($data);$i++){
			$dic = array(
									"{i}" 	=> $i,
									"{id}" 	=> $data[$i][0],
									"{tab}" => "tab".$data[$i][0],
									"{id_empresa}"	=> $data[$i][1],
									"{id_idioma}"	=>  $data[$i][2],
									"{quienes_somos}"	=>  $data[$i][3],
									"{mision}"	 => $data[$i][4],
									"{vision}"	 => $data[$i][5],
									"{producto}" => $data[$i][6],
									"{historia}" => $data[$i][7],
									"{idioma}"   =>$data[$i][8]
								);
			$render.=str_replace(array_keys($dic), array_values($dic), $match_cal);
		}
	}
	$html = str_replace($match_cal, $render, $html);
	return $html;
//----------------------------------------------------------------------------------------	
}
//--Para renderización estática
/*function render_estaticos($html,$data){
	foreach ($data as $clave => $valor) {
		$html = str_replace('{'.$clave.'}', $valor, $html);
	}
	return $html;
}*/
//----------------------------------------------------------------------------------------
//--Para renderizacion dinamica
function render_vista_consulta($html,$data){
	$template=get_template($html);
	$html = render_list($template,$data);//--renderización dinamica
	//$html = render_estaticos($html,$arr_pag);//--renderización estatica
	print $html;
	//die("aaa");
	//die($html);
}
//----------------------------------------------------------------------------------------
?>