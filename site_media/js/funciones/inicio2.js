//---------------BLOQUE DE EVENTOS--------------------------------------------
//Carga el slide principal
//$("#div_principal").load("vistas/slide_principal.html");
//Carga elemento de cuerpo inicio en pantalla principal
var idioma;
if ($("#idioma").hasClass("esta_espaniol")==true){
	idioma = 1;
}else if($("#idioma").hasClass("esta_ingles")==true)
{
	idioma = 2;
}
//carga_inicio2(idioma);
consultar_cuerpo_inicio(idioma);
//-- Boton de idioma ingles
$(document).on("click","#idioma_ingles",function(event){
    event.preventDefault();
	$("#idioma").addClass("esta_ingles").removeClass("esta_espaniol");
	consultar_cuerpo_inicio(2);
});
//--Boton de idioma espaniol
$(document).on("click","#idioma_espaniol",function(event){
    event.preventDefault();
	$("#idioma").addClass("esta_espaniol").removeClass("esta_ingles");
	consultar_cuerpo_inicio(1);
});
$(document).on("click","#texto_seccion_nosotros",function(e){
	carga_nosotros();
});
$(document).on("click","#texto_seccion_nuestro_producto",function(e){
	carga_nuestro_producto();
});
//----------------------------------------------------------------------------
//---------------BLOQUE DE FUNCIONES------------------------------------------
function consultar_cuerpo_inicio(idioma){
	var accion = 'consultar';
	var data = {
				'idioma':idioma,
				'accion':accion
	};
	$.ajax({
				url:"./controladores/inicioControler.php",
				type:"POST",
				data:data,
				cache:false,
				error:function(resp){
					alert("Error");
					console.log(resp);
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					//alert(recordset);
					$("#contenedor_quienes_somos").addClass("pagina_inicio");
					$("#contenedor_quienes_somos").html(recordset["quienes_somos"]);
					$("#contenedor_mision").html(recordset["mision"]);
					$("#contenedor_vision").html(recordset["vision"]);
					$("#contenedor_nuestro_producto").html(recordset["producto"]);
					consultar_tabla_cacao(idioma);
					cargar_titulos_inicio(idioma);
					$(".carousel").carousel({interval:3000});
				}
	});
}
//--Consulta de cacao
function consultar_tabla_cacao(idioma){
	var accion = 'consultar';
	var data = {
				'idioma':idioma,
				'accion':accion
	};
	$.ajax({
				url:"./controladores/cacaoControler.php",
				type:"POST",
				data:data,
				cache:false,
				error:function(resp){
					//alert("Error");
					console.log(resp);
				},
				success: function(resp){
					var recordset = $.parseJSON(resp);
					var html = armar_tabla(recordset);
					$("#tabla_cacao").html(html);
					estilos_tabla(idioma);
				}
	});
}
//--Funcion que arma tabla....
function armar_tabla(vector){
	var tabla;
	var fila_descripcion_cacao = "";
	var fila_especificaciones = "";
	var footer_tabla = "";
	tabla="<table class='tabla_cacao'>\
				<thead>\
					<tr class='th_tabla2'>\
						<th class='th_tabla'><span id='tbl_tipo' >Tipo de Cacao</span></th>\
						<th class='th_tabla'><span id='tbl_cantidad' >Cantidad Disponible (Tn)</span></th>\
						<th class='th_tabla th_caracteristicas'><span  id='tbl_caracteristicas'>Características del grano</span></th>\
						<th class='th_tabla th_especificaciones'><span id='tbl_especificaciones' >Especificaciones Técnicas</span></th>\
					</tr>\
				</thead>\
				<tbody>";
	for(i=0;i<vector.length;i++){
		fila_especificaciones = "";
		th_especificaciones = "";
		footer_tabla="<div class='titulo_negro3'>Total "+vector[0][14]+"&nbsp&nbsp&nbsp&nbsp"+vector[0][15]+"</div>";
		if(vector[i][5]!=""){
			fila_descripcion_cacao = "<div class='descripcion_cacao'>"+vector[i][5]+"</div>";
		}
		if(i==0 || i==2 || i==4 || i==6){
			fila_especificaciones = "<div class='tamano_grano'><div class='col-lg-10 str_texto left padding0'><span class='tbl_tamano'>Tamaño del grano 100 granos =</span></div>  <div class='col-lg-2 texto_humedad padding0 left'>"+vector[i][6]+"</div></div>\
									<div class='humedad'><div class=' col-lg-8 str_texto'><span class='tbl_humedad'>Humedad:</span></div><div class=' col-lg-4 texto_humedad padding0'>"+vector[i][7]+"</div></div>";
			fila_especificaciones = fila_especificaciones+"<div class='humedad'><div class='col-lg-8 str_texto'><span class='tbl_sustancias'>Sustancias extrañas</span></div><div class='col-lg-4 texto_humedad padding0'>"+vector[i][8]+"</div></div>\
														   <div class='humedad'><div class='col-lg-8 str_texto'><span class='tbl_alcalinidad'>Alcalinidad</span></div><div class='col-lg-4 texto_humedad padding0'>"+vector[i][9]+"</div></div>\
														   <div class='humedad'><div class='col-lg-8 str_texto'><span class='tbl_acidez'>Acidez</span></div><div class='col-lg-4 texto_humedad padding0'>"+vector[i][10]+"</div></div>\
														   <div class='humedad'><div class='col-lg-8 str_texto'><span class='tbl_mohosos'>Granos Mohosos</span></div><div class='col-lg-4 texto_humedad padding0'>"+vector[i][11]+"</div></div>\
   														   <div class='humedad'><div class='col-lg-8 str_texto'><span class='tbl_pizarrosos'>Granos pizarrosos</span></div><div class='col-lg-4 texto_humedad padding0'>"+vector[i][12]+"</div></div>\
														   <div class='humedad'><div class='col-lg-8 str_texto'><span class='tbl_planos'>Granos planos, germinados o dañados por insectos</span></div><div class='col-lg-4 texto_humedad padding0'>"+vector[i][13]+"</div></div>";									
		}
		if(fila_especificaciones!=""){
			th_especificaciones="<td class='td_tabla td_especificaciones' rowspan='2'>\
								"+fila_especificaciones+"</td>";
		}
		tabla=tabla+"<tr id='tr"+i+"'>\
							<td class='td_tabla'>"+vector[i][0]+"</td>\
							<td class='td_tabla'>"+vector[i][1]+"</td>\
							<td class='td_caracteristicas'>\
								<div class='fermentacion'>"+vector[i][3]+"</div>\
								<div class='tipo_cacao'><span class='tbl_detalle_tipo_cacao'>Tipos de cacao: </span>"+vector[i][4]+"</div>\
								"+fila_descripcion_cacao+"\
							</td>\
							"+th_especificaciones+"\
					 </tr>";
	}
	tabla = tabla +"</tbody>\
					</table>"+footer_tabla;
	return tabla;				
}
//--Funcion que coloca estilos a tabla
function estilos_tabla(idioma){
	$("#tr1,#tr3,#tr5,#tr6").addClass("linea_tabla");
	if (idioma==1){
		//--Para Tabla en espaniol
		$("#tbl_tipo").text("Tipo de Cacao");
		$("#tbl_cantidad").text("Cantidad Disponible (Tn)");
		$("#tbl_caracteristicas").text("Características del grano");
		$("#tbl_especificaciones").text("Especificaciones Técnicas");
		$(".tbl_detalle_tipo_cacao").text("Tipos de cacao: ");
		$(".tbl_tamano").text("Tamaño del grano 100 granos =");
		$(".tbl_humedad").text("Humedad");
		$(".tbl_sustancias").text("Sustancias extrañas");
		$(".tbl_alcalinidad").text("Alcalinidad");
		$(".tbl_acidez").text("Acidez");
		$(".tbl_mohosos").text("Granos Mohosos");
		$(".tbl_pizarrosos").text("Granos pizarrosos");
		$(".tbl_planos").text("Granos planos germinados o dañados por insectos");
		//--	
	}else if(idioma==2){
		//--Para tabla en ingles
		$("#tbl_tipo").text("Tipe of cacao");
		$("#tbl_cantidad").text("Amount available (Tn)");
		$("#tbl_caracteristicas").text("Characteristics of the bean");
		$("#tbl_especificaciones").text("Technical details");
		$(".tbl_detalle_tipo_cacao").text("Types of cacao: ");
		$(".tbl_tamano").text("Bean size 100 beans  =");
		$(".tbl_humedad").text("Humidity");
		$(".tbl_sustancias").text("Foreign substances");
		$(".tbl_alcalinidad").text("Alkalinity");
		$(".tbl_acidez").text("Acidity");
		$(".tbl_mohosos").text("Moldy beans");
		$(".tbl_pizarrosos").text("Slaty beans");
		$(".tbl_planos").text("Flat, germinated or damaged by insects beans");
		//--
	}
}
//--
function cargar_titulos_inicio(idioma){
	if(idioma==1){
		titulos_espaniol_inicio();
	}else if(idioma==2){
		titulos_ingles_inicio();
	}
}
function titulos_espaniol_inicio(){
	$("#titulo_quienes_somos").text("¿Quiénes somos?");
	$("#texto_seccion_nosotros").text("NOSOTROS");
	$("#texto_seccion_nuestro_producto").text("NUESTRO PRODUCTO");
	$("#titulo_mision").text("Misión");
	$("#titulo_vision").text("Visión");
	$("#titulo_noticias").text("NOTICIAS");
	$("#titulo_galeria").text("GALERÍA");
	//-- Slider principal
	$("#slide1").html("PRODUCIMOS EL MEJOR <span class='text_slider1'>CACAO VENEZOLANO</span>");
	$("#slide2").text("EXPORTAMOS EXCELENCIA");
	$("#slide3").text("SOMOS GARANTÍA DEL MEJOR CHOCOLATE DEL MUNDO ");

	//--Seccion de contactos formulario
	$("#contacto_nombres").text("Nombres:");
	$("#contacto_apellidos").text("Apellidos:");
	$("#contacto_telefono").text("Teléfono:");
	$("#contacto_email").text("Correo Electrónico:");
	$("#contacto_pais").text("País:");
	$("#contacto_mensaje").text("Su mensaje:");
	$("#contacto_enviar").text("Enviar");
	//--Seccion de contactos informacion
	$("#tbl_contacto").text("CONTACTO");
	$("#tbl_direccion").text("DIRECCIÓN DE OFICINA:");
	$("#tbl_tlf_contacto").text("TELÉFONOS");
	$("#tbl_horario").text("HORARIO OFICINA:");
	$("#tbl_horario2").text("lunes a viernes de 08:00 am a 05:00 pm:");
	$("#tbl_redes").text("SIGUENOS EN LAS REDES SOCIALES:");
}
function titulos_ingles_inicio(){
	$("#titulo_quienes_somos").text("About Us")
	$("#texto_seccion_nosotros").text("ABOUT US");
	$("#texto_seccion_nuestro_producto").text("OUR PRODUCT");
	$("#titulo_mision").text("Our Mission");
	$("#titulo_vision").text("Our Vission");
	$("#titulo_noticias").text("NEWS");
	$("#titulo_galeria").text("GALLERY");
	//-- Slider principal
	$("#slide1").text("PRODUCE THE BEST VENEZUELAN CACAO");
	$("#slide2").text("EXPORT EXCELLENCE");
	$("#slide3").text("WE ARE GUARANTEE BEST CHOCOLATE WORLD");
	//--Seccion de contactos formulario
	$("#contacto_nombres").text("Name:");
	$("#contacto_apellidos").text("Last Name:");
	$("#contacto_telefono").text("Telephone:");
	$("#contacto_email").text("Email:");
	$("#contacto_pais").text("Country:");
	$("#contacto_mensaje").text("Message:");
	$("#contacto_enviar").text("Send");
	//--Seccion de contactos informacion
	$("#tbl_contacto").text("CONTACT US");
	$("#tbl_direccion").text("ADDRESS:");
	$("#tbl_tlf_contacto").text("TELEPHONES");
	$("#tbl_horario").text("BUSINESS HOURS:");
	$("#tbl_horario2").text("Monday to Friday 08:00 am to 05:00 pm:");
	$("#tbl_redes").text("FOLLOW US ON:");
}