//---------------BLOQUE DE EVENTOS--------------------------------------------
//Carga elemento de cuerpo inicio en pantalla principal
consultar_cuerpo_nuestro_producto(1);
//----------------------------------------------------------------------------
//---------------BLOQUE DE FUNCIONES------------------------------------------
function consultar_cuerpo_nuestro_producto(idioma){
	var accion = 'consultar';
	var data = {
				'idioma':idioma,
				'accion':accion
	};
	$.ajax({
				url:"./controladores/nuestroproductoControler.php",
				type:"POST",
				data:data,
				cache:false,
				error:function(resp){
					//alert("Error");
					console.log(resp);
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					$("#contenedor_nuestro_producto").html(recordset["producto"]);
				}
	});
}
//----------------------------------------------------------------------------