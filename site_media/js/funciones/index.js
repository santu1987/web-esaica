//---------------BLOQUE DE EVENTOS--------------------------------------------
$(document).ready(function(){
	//1)Bloque eventos index
	//------------------------------------
	window.onload = function() {
        // Carga completa
		$("#contenido_web").removeClass("ocultar").addClass("mostrar");
		carga_inicio2();
		//--OBLIGATORIO PARA CORRER WOW-----------------------------------------------
		var wow = new WOW(
		  {
		    boxClass:     'wow',      // animated element css class (default is wow)
		    animateClass: 'animated', // animation css class (default is animated)
		    offset:       200,          // distance to the element when triggering the animation (default is 0)
		    mobile:       false,       // trigger animations on mobile devices (default is true)
		    live:         true,       // act on asynchronously loaded content (default is true)
		    callback:     function(box) {
		      // the callback is fired every time an animation is started
		      // the argument that is passed in is the DOM node being animated
		    }
		  }
		);
		wow.init();
		$("#contenido_inicial").removeClass("mostrar").removeClass("contenido_inicial").addClass("ocultar");	
	}
	cambiar_menu();
	//------------------------------------
	//carga_inicio2();
	//--Boton inicio...
	$("#btn_inicio").click(function(e){
		//configura_botones(1);
		//e.preventDefault();
		carga_inicio();
	});
	//--Boton de nosotros...
	$("#btn_nosotros").click(function(e){
		//configura_botones(2);
		e.preventDefault();
		carga_nosotros();
	});
	//--Boton de nuestro producto
	$(".btn_nuestro_producto").click(function(e){
		//configura_botones(3);
		e.preventDefault();
		carga_nuestro_producto();
	});
	//--Boton de contacto
	$(".btn_contacto").click(function(e){
		e.preventDefault();
		carga_contacto();
	});
	//--Boton de noticias
	$(".btn_noticias").click(function(e){
		e.preventDefault();
		carga_noticias();
	});
	//--Boton de galeria
	$(".btn_galeria").click(function(e){
		e.preventDefault();
		carga_galeria();
	});
	//-- Boton de idioma ingles
	$("#idioma_ingles").click(function(e){
		e.preventDefault;
		$("#idioma").addClass("esta_ingles").removeClass("esta_espaniol");
		cargar_ingles();
		//--Pagina de inicio
		if(($("#btn_inicio").hasClass("activo_top")==true)||($("#btn_inicio").hasClass("activo_normal")==true)){
			consultar_cuerpo_inicio(2);
		}else
		if(($("#btn_nosotros").hasClass("activo_top")==true)||($("#btn_nosotros").hasClass("activo_normal")==true)){
			consultar_cuerpo_nosotros(2);
		}else
		if(($("#btn_nuestro_producto").hasClass("activo_top")==true)||($("#btn_nuestro_producto").hasClass("activo_normal")==true)){
			consultar_cuerpo_nuestro_producto(2);
		}else
		if(($("#btn_noticias").hasClass("activo_top")==true)||($("#btn_noticias").hasClass("activo_normal")==true)){
			consultar_cuerpo_noticias(2,0,4,0);
		}else
		if(($("#btn_galeria").hasClass("activo_top")==true)||($("#btn_galeria").hasClass("activo_normal")==true)){
			consultar_cuerpo_galeria(2,0,10,0);
		}else
		if(($("#btn_contacto").hasClass("activo_top")==true)||($("#btn_galeria").hasClass("activo_normal")==true)){
			consultar_cuerpo_contacto(2);
		}
		//--
	});
	//--Boton de idioma espaniol
	$("#idioma_espaniol").click(function(e){
		e.preventDefault;
		$("#idioma").addClass("esta_espaniol").removeClass("esta_ingles");
		cargar_espaniol();
		//--Pagina de inicio
		if(($("#btn_inicio").hasClass("activo_top")==true)||($("#btn_inicio").hasClass("activo_normal")==true)){
			consultar_cuerpo_inicio(1);
		}else
		if(($("#btn_nosotros").hasClass("activo_top")==true)||($("#btn_nosotros").hasClass("activo_normal")==true)){
			consultar_cuerpo_nosotros(1);
		}else
		if(($("#btn_nuestro_producto").hasClass("activo_top")==true)||($("#btn_nuestro_producto").hasClass("activo_normal")==true)){
			consultar_cuerpo_nuestro_producto(1);
		}else
		if(($("#btn_noticias").hasClass("activo_top")==true)||($("#btn_noticias").hasClass("activo_normal")==true)){
			consultar_cuerpo_noticias(1,0,4,0);
		}else
		if(($("#btn_galeria").hasClass("activo_top")==true)||($("#btn_galeria").hasClass("activo_normal")==true)){
			consultar_cuerpo_galeria(1,0,10,0);
		}else
		if(($("#btn_contacto").hasClass("activo_top")==true)||($("#btn_galeria").hasClass("activo_normal")==true)){
			consultar_cuerpo_contacto(1);
		}
		//--
	});
	//--boton de instagram
	//$(".btn_instagram").click(function(){
	$(document).on("click",".btn_instagram",function(e){
		var accion = 'consultar_redes';
		var red = '3';
		var data = {
						'accion':accion,
						'red':red
		}
		$.ajax({
					url:"./controladores/contactoController.php",
					type:'POST',
					cache:false,
					data:data,
					error: function(resp){
						console.log(resp);
					},
					success: function(resp){
						var recordset = $.parseJSON(resp);
						//alert(recordset["instagram"]);
						window.open(recordset["instagram"]);
						//"https://www.instagram.com/esaica_cacao/"
					}

		});
	});
	//--Div contacto en cabecera
	$("#gran_caracas,.telefonillo, .contacto").click(function(e){
		e.preventDefault();
		carga_contacto();		
	});
	//--------------------------------------------------------------------------
	//2) Bloque de Eventos de inicio js
	$(document).on("click","#texto_seccion_nosotros",function(e){
		carga_nosotros();
	});
	$(document).on("click","#texto_seccion_nuestro_producto",function(e){
		carga_nuestro_producto();
	});
	//--Al pulsar contactos1
	$(document).on("click",".boton_contactos",function(e){
		var valor = '';
		if ($("#idioma").hasClass("esta_espaniol")==true){
			var idioma = 1
		}else if($("#idioma").hasClass("esta_ingles")==true)
		{
			var idioma = 2;
		}
		if(validar_contactos(valor,idioma)==true){	
		//-----------------------------	
			cargar_guardar_contactos(valor);
		//-----------------------------
		}
	});
	//--Al pulsar contactos 2
	$(document).on("click",".boton_contactos2",function(e){
		var valor = '2';
		if ($("#idioma").hasClass("esta_espaniol")==true){
			var idioma = 1
		}else if($("#idioma").hasClass("esta_ingles")==true)
		{
			var idioma = 2;
		}
		if(validar_contactos(valor,idioma)==true){	
		//-----------------------------	
			cargar_guardar_contactos(valor);
		//-----------------------------
		}
	});
	//--------------------------------------------------------------------------
	//3)Bloque de Eventos de nosotros.js
	//--------------------------------------------------------------------------
	//4)Bloque de Eventos nuestro_producto.js
	//-------------------------------------------------------------------------
});
//----------------------------------------------------------------------------
//---------------BLOQUE DE FUNCIONES------------------------------------------
//--0)Bloque de Funciones genericas para todas las secciones
function scroll_top(){
	$("html, body").animate({scrollTop: 0}, 600);
}
function ocultar_elementos_ini(){
	$("#div_pie_pagina").removeClass("mostrar").addClass("ocultar");
}
function cargar_guardar_contactos(valor){
	preloader_guardar();
	if ($("#idioma").hasClass("esta_espaniol")==true){
		texto_mensaje = "Sus datos fueron registrados de forma exitosa";
		texto_error = "Ocurri&oacute; un error en el proceso";
	}else if($("#idioma").hasClass("esta_ingles")==true)
	{
		texto_mensaje = 'Their data were successfully registered';
		texto_error = "An error occurred in the process ";
	}
	//-----------------------------------------------------------
    var nombres_contacto   = $("#nombres_contacto"+valor).val();
    var apellidos_contacto = $("#apellidos_contacto"+valor).val();
    var telefonos_contacto = $("#telefonos_contacto"+valor).val();
    var correo_contacto    = $("#correo_contacto"+valor).val();
    var pais_contacto      = $("#pais_contacto"+valor).val();
    var mensaje_contacto   = $("#mensaje_contacto"+valor).val();
    var accion = "guardar_contacto";
    var data = {
    				'accion':accion,
    				'nombres_contacto':nombres_contacto,
    				'apellidos_contacto':apellidos_contacto,
    				'telefonos_contacto':telefonos_contacto,
    				'correo_contacto':correo_contacto,
    				'pais_contacto':pais_contacto,
    				'mensaje_contacto':mensaje_contacto
    };
    $.ajax({
    		url:"./controladores/inicioControler.php",
    		type:"POST",
    		data:data,
    		cache:false,
    		error:function(resp){
    			console.log(resp);
    		},
    		success: function(resp){
    			var recordset = $.parseJSON(resp);
    			if(recordset["mensaje"]=='1'){
    				$("#aceptar_mensaje").removeClass("ocultar").addClass("mostrar");
    				mensajes(texto_mensaje);
    				enviar_correo(valor);
    			}else if(recordset["mensaje"]=='0'){
    				mensajes(texto_error);	
    			}
    			
    		}

    });
    //---------------------------------------------------------
}
//--Preloader al guardar
function preloader_guardar(){
	$("#aceptar_mensaje").removeClass("mostrar").addClass("ocultar");
	if ($("#idioma").hasClass("esta_espaniol")==true){
		texto_mensaje = 'Espere unos segundos mientras se registran sus datos';
	}else if($("#idioma").hasClass("esta_ingles")==true)
	{
		texto_mensaje = 'Wait a few seconds while your data are recorded';
	}
	mensajes('<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>'+texto_mensaje);	
}
//--Funcion paraa el envio de emails.
function enviar_correo(valor){
	var nombres_contacto   = $("#nombres_contacto"+valor).val();
    var apellidos_contacto = $("#apellidos_contacto"+valor).val();
    var telefonos_contacto = $("#telefonos_contacto"+valor).val();
    var correo_contacto    = $("#correo_contacto"+valor).val();
    var pais_contacto      = $("#pais_contacto"+valor).val();
    var mensaje_contacto   = $("#mensaje_contacto"+valor).val();
    var accion = "enviar_correo";
    var data = {
    				'accion':accion,
    				'nombres_contacto':nombres_contacto,
    				'apellidos_contacto':apellidos_contacto,
    				'telefonos_contacto':telefonos_contacto,
    				'correo_contacto':correo_contacto,
    				'pais_contacto':pais_contacto,
    				'mensaje_contacto':mensaje_contacto
    };
	$.ajax({
				url:"./controladores/inicioControler.php",
				type:"POST",
				data:data,
				cache:false,
				error:function(resp){
					console.log(resp);
				},
				success: function(resp){
					var recordset = $.parseJSON(resp);
					//alert(recordset["mensaje"]);
					if(recordset["mensaje"]==0){
						mensajes("Ocurri&oacute; un error en el envi&oacute; de correo");
					}else{
						limpiar_contactos(valor);
					}	
				}
	});
}
function limpiar_contactos(valor){
	$("#nombres_contacto"+valor).val("");
    $("#apellidos_contacto"+valor).val("");
    $("#telefonos_contacto"+valor).val("");
    $("#correo_contacto"+valor).val("");
    $("#pais_contacto"+valor).val("");
    $("#mensaje_contacto"+valor).val("");
}
function mostrar_elementos_ini(){
	$("#div_pie_pagina").removeClass("ocultar").addClass("mostrar");
}
function validar_contactos(valor, idioma){
	if($("#nombres_contacto"+valor).val()==""){
		//--
		if(idioma==1){
			mensajes("El campo nombres no puede estar en blanco");
		}
		else{
			mensajes("The field name can't be blank");	
		}
		//--
		$("#nombres_contacto"+valor).focus();
		return false;
	}else if($("#apellidos_contacto"+valor).val()==""){
		//--
		if(idioma==1){
			mensajes("El campo apellidos no puede estar en blanco");
		}
		else{
			mensajes("The field last name can't be blank");	
		}
		//--		
		$("#apellidos_contacto"+valor).focus();
		return false;
	}else if($("#telefonos_contacto"+valor).val()==""){
		if(idioma==1){
			mensajes("El campo telefonos no puede estar en blanco");
		}
		else{
			mensajes("The field telephone can't be blank");	
		}
		//--
		$("#telefonos_contacto"+valor).focus();
		return false;
	}else if($("#correo_contacto"+valor).val()==""){
		if(idioma==1){
			mensajes("El campo email no puede estar en blanco");
		}
		else{
			mensajes("The field email can't be blank");	
		}
		//--
		$("#correo_contacto"+valor).focus();
		return false;
	}else if($("#pais_contacto"+valor).val()==""){
		if(idioma==1){
			mensajes("El campo país no puede estar en blanco");
		}
		else{
			mensajes("The field country can't be blank");	
		}
		//--
		$("#pais_contacto"+valor).focus();
		return false;
	}else if($("#mensaje_contacto"+valor).val()==""){
		if(idioma==1){
			mensajes("El campo mensaje no puede estar en blanco");
		}
		else{
			mensajes("The field message can't be blank");	
		}
		//--
		$("#mensaje_contacto"+valor).focus();
		return false;
	}else{
		return true;
	}
}
function quitar_preloader(contenido_inicial,contenido_web){
	$(contenido_inicial).removeClass("mostrar").removeClass("contenido_inicial").addClass("ocultar");	
	$(contenido_web).removeClass("ocultar").addClass("mostrar");
	mostrar_elementos_ini();
	//--OBLIGATORIO PARA CORRER WOW-----------------------------------------------
	var wow = new WOW(
	  {
	    boxClass:     'wow',      // animated element css class (default is wow)
	    animateClass: 'animated', // animation css class (default is animated)
	    offset:       200,          // distance to the element when triggering the animation (default is 0)
	    mobile:       false,       // trigger animations on mobile devices (default is true)
	    live:         true,       // act on asynchronously loaded content (default is true)
	    callback:     function(box) {
	      // the callback is fired every time an animation is started
	      // the argument that is passed in is the DOM node being animated
	    }
	  }
	);
	wow.init();
	//-------------------------------------------------------------------------------
}
//------------------------------------------------------------------------------
//--1)Bloque de Funciones index.js
function cambiar_menu(){
   var scroll_start = 0;
   var startchange = $('#quienes_somos');
   var offset = startchange.offset();
   $(document).scroll(function() { 
      scroll_start = $(this).scrollTop();
      if(scroll_start > 34) {
      	$("#ul_master").addClass("nav_normal").removeClass("nav_top");
        $('#btn_inicio,#btn_nosotros,#btn_nuestro_producto,#btn_noticias,#btn_galeria,#btn_contacto').addClass('btn_menu').removeClass('btn_menu_top');
        var logo ='<img src="site_media/img/logo.png" class="imagen_logo" alt="">';
      	cambiar_clase_btn_principal(1);
      }else {
       	$("#ul_master").addClass("nav_top").removeClass("nav_normal");
       	$('#btn_inicio,#btn_nosotros,#btn_nuestro_producto,#btn_noticias,#btn_galeria,#btn_contacto').addClass('btn_menu_top').removeClass('btn_menu');
       	var logo = '<img src="site_media/img/logo_blanco.png" class="imagen_logo" alt="">';
       	cambiar_clase_btn_principal(2);
       }
       $("#logo_web").html(logo);
   });
}

function cambiar_clase_btn_principal(opcion){
	//-----------------------------------------
	var arreglos_botones =new Array('#btn_inicio','#btn_nosotros','#btn_nuestro_producto','#btn_noticias','#btn_galeria','#btn_contacto');
	if(opcion==1){
		for(i=0;i<=arreglos_botones.length;i++){
		//---------------------------------------
			if($(arreglos_botones[i]).hasClass("activo_top")==true){
				$(arreglos_botones[i]).addClass("activo_normal").removeClass("activo_top");
			}
		//---------------------------------------	
		}
		//--
		$("#barras1,#barras2,#barras3").removeClass("icon-bar-blanco").addClass("icon-bar-negro");
		//--
	}	
	//----------------------------------------	
	else if(opcion==2){
		for(i=0;i<=arreglos_botones.length;i++){
		//---------------------------------------
			if($(arreglos_botones[i]).hasClass("activo_normal")==true){
				$(arreglos_botones[i]).addClass("activo_top").removeClass("activo_normal");
			}
		//---------------------------------------	
		}
		//--
		$("#barras1,#barras2,#barras3").removeClass("icon-bar-negro").addClass("icon-bar-blanco");
		//--
	}
	//------------------------------------------
}

function carga_inicio(){
	//--oculto menu,header y footter
	ocultar_elementos_ini();
	$("#cuerpo_sistema").load("vistas/inicio.html", function(e){
	//---------------------------
	activar_btn("inicio");
	var idioma;
	if ($("#idioma").hasClass("esta_espaniol")==true){
		idioma = 1;
	}else if($("#idioma").hasClass("esta_ingles")==true)
	{
		idioma = 2;
	}
	consultar_cuerpo_inicio(idioma);
	consultar_cuerpo_contacto(idioma);
	//---------------------------	
	});
}

function carga_inicio2(){
	activar_btn("inicio");
	var idioma = 1;
	$("#idioma").addClass("esta_espaniol").removeClass("esta_ingles");
	consultar_cuerpo_inicio(idioma);
	consultar_cuerpo_contacto(idioma);
	//$("#cuerpo_sistema").load("vistas/inicio.html");
	/*$(".btn_inicio").css({"background-color":"#000","color":"#fff"});
	$(".btn_nosotros,.btn_nuestro_producto,.btn_contacto").css({"background-color":"#fff","color":"#000"});*/
}

function carga_nosotros(){
	//--oculto menu,header y footter
	ocultar_elementos_ini();
	$("#cuerpo_sistema").load("vistas/nosotros.html",function(e){
	//------------------------------------------------------------
	activar_btn("nosotros");
	var idioma;
	if ($("#idioma").hasClass("esta_espaniol")==true){
		idioma = 1;
	}else if($("#idioma").hasClass("esta_ingles")==true)
	{
		idioma = 2;
	}
	consultar_cuerpo_nosotros(idioma);
	//------------------------------------------------------------	
	});
}

function carga_nuestro_producto(){
	//--oculto menu,header y footter
	ocultar_elementos_ini();
	$("#cuerpo_sistema").load("vistas/nuestro_producto.html",function(e){
	//------------------------------------------------------------------
	activar_btn("nuestro_producto");
	var idioma;
	if ($("#idioma").hasClass("esta_espaniol")==true){
		idioma = 1;
	}else if($("#idioma").hasClass("esta_ingles")==true)
	{
		idioma = 2;
	}
	consultar_cuerpo_nuestro_producto(idioma);
	//------------------------------------------------------------------	
	});
}

function carga_contacto(){
	//--oculto menu,header y footter
	ocultar_elementos_ini();
	$("#cuerpo_sistema").html("");
	$("#cuerpo_sistema").load("vistas/contacto.html");
	activar_btn("contacto");
	var idioma;
	if ($("#idioma").hasClass("esta_espaniol")==true){
		idioma = 1;
	}else if($("#idioma").hasClass("esta_ingles")==true)
	{
		idioma = 2;
	}
	setTimeout(function(){
		consultar_cuerpo_contacto(idioma);
	},2000);
	//consultar_cuerpo_contacto(idioma);
}

function carga_noticias(){
	ocultar_elementos_ini();
	$("#cuerpo_sistema").html("");
	//---------------------------------
	$("#cuerpo_sistema").load("vistas/noticias.html");
	activar_btn("noticias");
	var idioma;
	if ($("#idioma").hasClass("esta_espaniol")==true){
		idioma = 1;
	}else if($("#idioma").hasClass("esta_ingles")==true)
	{
		idioma = 2;
	}
	//---------------------------------	
	setTimeout(function(){
		consultar_cuerpo_noticias(idioma,0,4,0);
	},3000);
}

function carga_galeria(){
	//--oculto menu,header y footter
	ocultar_elementos_ini();
	$("#cuerpo_sistema").html("");
	$("#cuerpo_sistema").load("vistas/galeria.html");
	activar_btn("galeria");
	var idioma;
	if ($("#idioma").hasClass("esta_espaniol")==true){
		idioma = 1;
	}else if($("#idioma").hasClass("esta_ingles")==true)
	{
		idioma = 2;
	}
	//---------------------------------	
	setTimeout(function(){
		consultar_cuerpo_galeria(idioma,0,10,0);

	},2000);
}

function configura_botones(opcion){
	switch(opcion){
		case 1:
			$(".btn_inicio").css({"background-color":"#000","color":"#fff"});
			$(".btn_nosotros,.btn_nuestro_producto,.btn_contacto,.btn_noticias,.btn_galeria").css({"background-color":"#fff","color":"#000"});
		case 2:
			$(".btn_nosotros").css({"background-color":"#000","color":"#fff"});
			$(".btn_inicio,.btn_nuestro_producto,.btn_contacto,.btn_noticias,.btn_galeria").css({"background-color":"#fff","color":"#000"});
		case 3:
			$(".btn_nuestro_producto").css({"background-color":"#000","color":"#fff"});
			$(".btn_inicio,.btn_nosotros,.btn_contacto,.btn_noticias,.btn_galeria").css({"background-color":"#fff","color":"#000"});
			
	}
}

//--Funcion que carga sectores en ingles
function cargar_ingles(){
	//-Cambio a ingles de texto de botones
	$("#text_inicio").text("HOME");
	$("#text_nosotros").text("ABOUT US");
	$("#text_nproducto").text("OUR PRODUCT");
	$("#text_noticias").text("NEWS");
	$("#text_galeria").text("GALLERY");
	$("#text_contacto").text("CONTACT US");

}

//--Funcion que carga sectores en espanish
function cargar_espaniol(){
	//-Cambio a espaniol de texto de botones
	$("#text_inicio").text("INICIO");
	$("#text_nosotros").text("NOSOTROS");
	$("#text_nproducto").text("NUESTRO PRODUCTO");
	$("#text_noticias").text("NOTICIAS");
	$("#text_galeria").text("GALERIA");
	$("#text_contacto").text("CONTACTO");
}

function activar_btn(seccion){
	$("#btn_inicio,#btn_nosotros,#btn_nuestro_producto,#btn_contacto,#btn_noticias,#btn_galeria").removeClass("activo_top").removeClass("activo_normal");
	switch(seccion){
		case 'inicio':
			$("#btn_inicio").addClass("activo_top");
			break;
		case 'nosotros':
			$("#btn_nosotros").addClass("activo_top");
			break;
		case 'nuestro_producto':
			$("#btn_nuestro_producto").addClass("activo_top");
			break;
		case 'contacto':
			$("#btn_contacto").addClass("activo_top");
			break;
		case 'noticias':
			$("#btn_noticias").addClass("activo_top");
			break;
		case 'galeria':
			$("#btn_galeria").addClass("activo_top");
			break;								
	}
}

//--Bloque de Funciones de inicio.js
function consultar_cuerpo_inicio(idioma){
	var accion = 'consultar';
	var data = {
				'idioma':idioma,
				'accion':accion
	};
	$.ajax({
				url:"./controladores/inicioControler.php",
				type:"POST",
				data:data,
				cache:false,
				error:function(resp){
					//alert("Error");
					console.log(resp);
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					ocultar_elementos_ini();
					//$("#contenedor_quienes_somos").addClass("pagina_inicio");
					$("#contenedor_quienes_somos").html(recordset["quienes_somos"]);
					$("#contenedor_mision").html(recordset["mision"]);
					$("#contenedor_vision").html(recordset["vision"]);
					$("#contenedor_nuestro_producto").html(recordset["producto"]);
					cargar_titulos_inicio(idioma);
					consultar_noticias(idioma);
					consultar_imagenes_galeria(idioma);
				}
	});
}

//--Consulta de cacao
function consultar_tabla_cacao(idioma){
	var accion = 'consultar';
	var data = {
				'idioma':idioma,
				'accion':accion
	};
	$.ajax({
				url:"./controladores/cacaoControler.php",
				type:"POST",
				data:data,
				cache:false,
				error:function(resp){
					//alert("Error");
					console.log(resp);
				},
				success: function(resp){
					var recordset = $.parseJSON(resp);
					var html = armar_tabla(recordset);
					$("#tabla_cacao").html(html);
					estilos_tabla(idioma);
				}
	});
}

//--Funcion que arma tabla....
function armar_tabla(vector){
	var tabla;
	var fila_descripcion_cacao = "";
	var fila_especificaciones = "";
	var footer_tabla = "";
	tabla="<table class='tabla_cacao'>\
				<thead>\
					<tr class='th_tabla2'>\
						<th class='th_tabla'><span id='tbl_tipo' >Tipo de Cacao</span></th>\
						<th class='th_tabla'><span id='tbl_cantidad' >Cantidad Disponible (Tn)</span></th>\
						<th class='th_tabla th_caracteristicas'><span  id='tbl_caracteristicas'>Características del grano</span></th>\
						<th class='th_tabla th_especificaciones'><span id='tbl_especificaciones' >Especificaciones Técnicas</span></th>\
					</tr>\
				</thead>\
				<tbody>";
	for(i=0;i<vector.length;i++){
		fila_especificaciones = "";
		th_especificaciones = "";
		footer_tabla="<div class='titulo_negro3'>Total "+vector[0][14]+"&nbsp&nbsp&nbsp&nbsp"+vector[0][15]+"</div>";
		if(vector[i][5]!=""){
			fila_descripcion_cacao = "<div class='descripcion_cacao'>"+vector[i][5]+"</div>";
		}
		if(i==0 || i==2 || i==4 || i==6){
			fila_especificaciones = "<div class='tamano_grano'><div class='col-lg-10 str_texto left padding0'><span class='tbl_tamano'>Tamaño del grano 100 granos =</span></div>  <div class='col-lg-2 texto_humedad padding0 left'>"+vector[i][6]+"</div></div>\
									<div class='humedad'><div class=' col-lg-8 str_texto'><span class='tbl_humedad'>Humedad:</span></div><div class=' col-lg-4 texto_humedad padding0'>"+vector[i][7]+"</div></div>";
			fila_especificaciones = fila_especificaciones+"<div class='humedad'><div class='col-lg-8 str_texto'><span class='tbl_sustancias'>Sustancias extrañas</span></div><div class='col-lg-4 texto_humedad padding0'>"+vector[i][8]+"</div></div>\
														   <div class='humedad'><div class='col-lg-8 str_texto'><span class='tbl_alcalinidad'>Alcalinidad</span></div><div class='col-lg-4 texto_humedad padding0'>"+vector[i][9]+"</div></div>\
														   <div class='humedad'><div class='col-lg-8 str_texto'><span class='tbl_acidez'>Acidez</span></div><div class='col-lg-4 texto_humedad padding0'>"+vector[i][10]+"</div></div>\
														   <div class='humedad'><div class='col-lg-8 str_texto'><span class='tbl_mohosos'>Granos Mohosos</span></div><div class='col-lg-4 texto_humedad padding0'>"+vector[i][11]+"</div></div>\
   														   <div class='humedad'><div class='col-lg-8 str_texto'><span class='tbl_pizarrosos'>Granos pizarrosos</span></div><div class='col-lg-4 texto_humedad padding0'>"+vector[i][12]+"</div></div>\
														   <div class='humedad'><div class='col-lg-8 str_texto'><span class='tbl_planos'>Granos planos, germinados o dañados por insectos</span></div><div class='col-lg-4 texto_humedad padding0'>"+vector[i][13]+"</div></div>";									
		}
		if(fila_especificaciones!=""){
			th_especificaciones="<td class='td_tabla td_especificaciones' rowspan='2'>\
								"+fila_especificaciones+"</td>";
		}
		tabla=tabla+"<tr id='tr"+i+"'>\
							<td class='td_tabla'>"+vector[i][0]+"</td>\
							<td class='td_tabla'>"+vector[i][1]+"</td>\
							<td class='td_caracteristicas'>\
								<div class='fermentacion'>"+vector[i][3]+"</div>\
								<div class='tipo_cacao'><span class='tbl_detalle_tipo_cacao'>Tipos de cacao: </span>"+vector[i][4]+"</div>\
								"+fila_descripcion_cacao+"\
							</td>\
							"+th_especificaciones+"\
					 </tr>";
	}
	tabla = tabla +"</tbody>\
					</table>"+footer_tabla;
	return tabla;				
}

//--Funcion que coloca estilos a tabla
function estilos_tabla(idioma){
	$("#tr1,#tr3,#tr5,#tr6").addClass("linea_tabla");
	if (idioma==1){
		//--Para Tabla en espaniol
		$("#tbl_tipo").text("Tipo de Cacao");
		$("#tbl_cantidad").text("Cantidad Disponible (Tn)");
		$("#tbl_caracteristicas").text("Características del grano");
		$("#tbl_especificaciones").text("Especificaciones Técnicas");
		$(".tbl_detalle_tipo_cacao").text("Tipos de cacao: ");
		$(".tbl_tamano").text("Tamaño del grano 100 granos =");
		$(".tbl_humedad").text("Humedad");
		$(".tbl_sustancias").text("Sustancias extrañas");
		$(".tbl_alcalinidad").text("Alcalinidad");
		$(".tbl_acidez").text("Acidez");
		$(".tbl_mohosos").text("Granos Mohosos");
		$(".tbl_pizarrosos").text("Granos pizarrosos");
		$(".tbl_planos").text("Granos planos germinados o dañados por insectos");
		//--	
	}else if(idioma==2){
		//--Para tabla en ingles
		$("#tbl_tipo").text("Tipe of cacao");
		$("#tbl_cantidad").text("Amount available (Tn)");
		$("#tbl_caracteristicas").text("Characteristics of the bean");
		$("#tbl_especificaciones").text("Technical details");
		$(".tbl_detalle_tipo_cacao").text("Types of cacao: ");
		$(".tbl_tamano").text("Bean size 100 beans  =");
		$(".tbl_humedad").text("Humidity");
		$(".tbl_sustancias").text("Foreign substances");
		$(".tbl_alcalinidad").text("Alkalinity");
		$(".tbl_acidez").text("Acidity");
		$(".tbl_mohosos").text("Moldy beans");
		$(".tbl_pizarrosos").text("Slaty beans");
		$(".tbl_planos").text("Flat, germinated or damaged by insects beans");
		//--
	}
}

//--
function cargar_titulos_inicio(idioma){
	if(idioma==1){
		titulos_espaniol_inicio();
	}else if(idioma==2){
		titulos_ingles_inicio();
	}
}

function titulos_espaniol_inicio(){
	$("#titulo_quienes_somos").text("Nuestra Empresa");
	$("#texto_seccion_nosotros").text("NOSOTROS");
	$("#texto_seccion_nuestro_producto").text("NUESTRO PRODUCTO");
	$("#titulo_mision").text("Misión");
	$("#titulo_vision").text("Visión");
	$("#texto_seccion_noticias").text("NOTICIAS");
	$("#texto_seccion_galeria").text("GALERÍA");
	//--Seccion de contactos formulario
	$("#contacto_nombres").text("Nombres:");
	$("#contacto_apellidos").text("Apellidos:");
	$("#contacto_telefono").text("Teléfono:");
	$("#contacto_email").text("Correo Electrónico:");
	$("#contacto_pais").text("País:");
	$("#contacto_mensaje").text("Su mensaje:");
	$("#contacto_enviar").text("Enviar");
	//--Seccion de contactos informacion
	$("#tbl_contacto").text("CONTACTO");
	$("#tbl_direccion").text("DIRECCIÓN DE OFICINA:");
	$("#tbl_tlf_contacto").text("TELÉFONOS");
	$("#tbl_horario").text("HORARIO OFICINA:");
	$("#tbl_horario2").text("lunes a viernes de 08:00 am a 05:00 pm:");
	$("#tbl_redes").text("SIGUENOS EN LAS REDES SOCIALES:");
	quitar_preloader("#contenido_inicial_inicio","#contenido_web_inicio");
}

function titulos_ingles_inicio(){
	$("#titulo_quienes_somos").text("About Us")
	$("#texto_seccion_nosotros").text("ABOUT US");
	$("#texto_seccion_nuestro_producto").text("OUR PRODUCT");
	$("#titulo_mision").text("Our Mission");
	$("#titulo_vision").text("Our Vission");
	$("#texto_seccion_noticias").text("NEWS");
	$("#texto_seccion_galeria").text("GALLERY");
	//--Seccion de contactos informacion
	$("#tbl_contacto").text("CONTACT US");
	$("#tbl_direccion").text("ADDRESS:");
	$("#tbl_tlf_contacto").text("TELEPHONES");
	$("#tbl_horario").text("BUSINESS HOURS:");
	$("#tbl_horario2").text("Monday to Friday 08:00 am to 05:00 pm:");
	$("#tbl_redes").text("FOLLOW US ON:");
	//--Seccion de contactos formulario
	$("#contacto_nombres").text("Name:");
	$("#contacto_apellidos").text("Last Name:");
	$("#contacto_telefono").text("Telephone:");
	$("#contacto_email").text("Email:");
	$("#contacto_pais").text("Country:");
	$("#contacto_mensaje").text("Message:");
	$("#contacto_enviar").text("Send");
	$("#contacto_nombres2").text("Name:");
	$("#contacto_apellidos2").text("Last Name:");
	$("#contacto_telefono2").text("Telephone:");
	$("#contacto_email2").text("Email:");
	$("#contacto_pais2").text("Country:");
	$("#contacto_mensaje2").text("Message:");
	$("#contacto_enviar2").text("Send");
	quitar_preloader("#contenido_inicial_inicio","#contenido_web_inicio");
}
//----------------------------------------------------------------------------
//3)Bloque de funciones de nosotros.js
function consultar_cuerpo_nosotros(idioma){
	var accion = 'consultar';
	var data = {
				'idioma':idioma,
				'accion':accion
	};
	$.ajax({
				url:"./controladores/nosotrosControler.php",
				type:"POST",
				data:data,
				cache:false,
				error:function(resp){
					alert("Error");
					console.log(resp);
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					//-------------------------------------------------------------	
						$("#contenedor_nosotros").html(recordset["quienes_somos"]);
						$("#contenedor_mision").html(recordset["mision"]);
						$("#contenedor_vision").html(recordset["vision"]);
						$("#contenedor_nosotros_historia").html(recordset["historia"]);
						$("#contenedor_texto_cv1").html(recordset["cv1"]);
						$("#contenedor_texto_cv2").html(recordset["cv2"]);
						cargar_titulos_nosotros(idioma);
					//-------------------------------------------------------------	
				}
	});
}
//--Funcion para carga titulos de pagina nosotros
function cargar_titulos_nosotros(idioma){
	if(idioma==1){
		titulos_espaniol_nosotros();
	}else if(idioma==2){
		titulos_ingles_nosotros();
	}	
}
//--Funcion que carga titulos en español..
function titulos_espaniol_nosotros(){
	$("#texto_seccion_nosotros").text("NOSOTROS");
	$("#texto_seccion_nuestro_equipo").text("NUESTRO EQUIPO");
	$("#titulo_mision").text("Misión");
	$("#titulo_vision").text("Visión");
	$("#titulo_nuestra_historia").text("NUESTRA HISTORIA");
	$("#texto_seccion_valores").text("VALORES");
	$("#valores1").text(" Excelencia");
	$("#valores2").text(" Responsabilidad");
	$("#valores3").text(" Honestidad");
	$("#valores4").text(" Compromiso");
	$("#valores5").text(" Solidaridad");
	$("#valores6").text(" Trabajo en equipo");
	$("#valores7").text(" Perseverancia");
	$("#valores8").text(" Responsabilidad Ambiental");
	quitar_preloader("#contenido_inicial_nosotros","#contenido_web_nosotros");
}
//--Funcion que carga titulos en ingles...
function titulos_ingles_nosotros(){
	$("#texto_seccion_nosotros").text("ABOUT US");
	$("#texto_seccion_nuestro_equipo").text("OUR TEAM");
	$("#titulo_mision").text("Our Mission");
	$("#titulo_vision").text("Our Vission");
	$("#titulo_nuestra_historia").text("OUR STORY");
	$("#texto_seccion_valores").text("OUR VALUES");
	$("#valores1").text(" Excellence");
	$("#valores2").text(" Responsibility");
	$("#valores3").text(" Solidarity");
	$("#valores4").text(" Teamwork");
	$("#valores5").text(" Honesty");
	$("#valores6").text(" Perseverance");
	$("#valores7").text(" Commitment");
	$("#valores8").text(" Environmental responsibility");
	quitar_preloader("#contenido_inicial_nosotros","#contenido_web_nosotros");
}
//----------------------------------------------------------------------------------
//4)Bloque de funciones de nosotros.js
function consultar_cuerpo_nuestro_producto(idioma){
	var accion = 'consultar';
	var data = {
				'idioma':idioma,
				'accion':accion
	};
	$.ajax({
				url:"./controladores/nuestroproductoControler.php",
				type:"POST",
				data:data,
				cache:false,
				error:function(resp){
					//alert("Error");
					console.log(resp);
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					$("#contenedor_nuestro_producto").html(recordset["producto"]);
					cargar_titulos_nuestro_producto(idioma);
				}
	});
}
//--Funcion para carga titulos de pagina nosotros
function cargar_titulos_nuestro_producto(idioma){
	if(idioma==1){
		titulos_espaniol_nuestro_producto();
	}else if(idioma==2){
		titulos_ingles_nuestro_producto();
	}	
}
//--Funcion que carga titulos en español..
function titulos_espaniol_nuestro_producto(){
	$("#texto_seccion_nuestro_producto").text("NUESTRO PRODUCTO");
	quitar_preloader("#contenido_inicial_nuestro_producto","#contenido_web_nuestro_producto");
}
//--Funcion que carga titulos en ingles...
function titulos_ingles_nuestro_producto(){
	$("#texto_seccion_nuestro_producto").text("OUR PRODUCT");
	quitar_preloader("#contenido_inicial_nuestro_producto","#contenido_web_nuestro_producto");
}
//----------------------------------------------------------------------------
//--5)Bloque de noticias
function consultar_cuerpo_noticias(idioma,offset,limit,actual){
	cargar_titulos_noticias(idioma);
	var accion = "consultar_noticias_detalles";
	var data = {

					'accion':accion,
					'idioma':idioma,
					'offset':offset,
					'limit':limit,
					'actual':actual
			
	};
	$.ajax({
				url:'./controladores/noticiasControler.php',
				type:'POST',
				data:data,
				cache:false,
				error: function(resp){

				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					$("#contenedor_noticias_detalle2").html(recordset["cabecera_html"]);
					$("#estructura_noticias2").html(recordset["detalle_html"]);
					$("#paginacion_tabla").html(recordset["cuerpo_paginacion"]);
				}
	});
}
function cargar_titulos_noticias(idioma){
	if(idioma==1){
		titulos_espaniol_noticias();
	}else if(idioma==2){
		titulos_ingles_noticias();
	}
}
function titulos_espaniol_noticias(){
	$("#texto_seccion_noticias").text("NOTICIAS");
	//$("#en_construccion").text("Esta sección se encuentra en construcción");
	quitar_preloader("#contenido_inicial_noticias","#contenido_web_noticias");	
}
function titulos_ingles_noticias(){
	$("#texto_seccion_noticias").text("NEWS");
	//$("#en_construccion").text("This section is under construcction");
	quitar_preloader("#contenido_inicial_noticias","#contenido_web_noticias");
}
//----------------------------------------------------------------------------
//--6)Bloque de galerias
function consultar_cuerpo_galeria(idioma,offset,limit,actual){
	cargar_titulos_galeria(idioma);
	cargar_pagina_imagen(idioma,offset,limit,actual);
	cargar_pagina_video(idioma,offset,4,actual);
}
//--Carga pagina videos
function cargar_pagina_video(idioma,offset,limit,actual){
	var accion = "consultar_cuerpo_galeria_video";
	var data = {

					'accion':accion,
					'offset':offset,
					'limit':limit,
					'actual':actual,
					'idioma':idioma
	};
	$.ajax({
				url:'./controladores/galeriasControler.php',
				type:'POST',
				data:data,
				cache:false,
				error: function(resp){

				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					if(recordset["cuantos_son"]>0){
						$("#estructura_videos2").html(recordset["detalle_html"]);
						$("#paginacion_tabla_videos").html(recordset["cuerpo_paginacion"]);
						$("#contenedor_videos").removeClass("ocultar").addClass("mostrar");
					}else{
						$("#contenedor_videos").removeClass("mostrar").addClass("ocultar");
					}
				}
	});
}
//--Carga pagina imagenes
function cargar_pagina_imagen(idioma,offset,limit,actual){
	var accion = "consultar_cuerpo_galeria_detalle";
	var data = {

					'accion':accion,
					'idioma':idioma,
					'offset':offset,
					'limit':limit,
					'actual':actual
			
	};
	$.ajax({
				url:'./controladores/galeriasControler.php',
				type:'POST',
				data:data,
				cache:false,
				error: function(resp){

				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					$("#contenedor_imagenes_detalle2").html(recordset["cabecera_html"]);
					$("#estructura_imagenes2").html(recordset["detalle_html"]);
					$("#paginacion_tabla").html(recordset["cuerpo_paginacion"]);
				}
	});
}
function cargar_titulos_galeria(idioma){
	if(idioma==1){
		titulos_espaniol_galeria();
	}else if(idioma==2){
		titulos_ingles_galeria();
	}
}

function titulos_espaniol_galeria(){
	$("#texto_seccion_galeria").text("GALERIA");
	//$("#en_construccion").text("Esta sección se encuentra en construcción");
	quitar_preloader("#contenido_inicial_galeria","#contenido_web_galeria");	
}

function titulos_ingles_galeria(){
	$("#texto_seccion_galeria").text("GALLERY");
	//$("#en_construccion").text("This section is under construcction");
	quitar_preloader("#contenido_inicial_galeria","#contenido_web_galeria");
}
function cargar_imagen_principal(numero,id,idioma,offset,limit,actual){
var accion = "consultar_imagen_principal";	
var data = {
				'accion':accion,
				'id':id,
				'idioma':idioma,
				'offset':offset,
				'limit':limit,
				'actual':actual
}	
$.ajax({
				url:'./controladores/galeriasControler.php',
				type:'POST',
				data:data,
				cache:false,
				error:function(resp){
					console.log(resp);
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					$("#contenedor_imagenes_detalle2").html(recordset["cabecera_html"]);
					$("#estructura_imagenes2").html(recordset["detalle_html"]);
					$("#paginacion_tabla").html(recordset["cuerpo_paginacion"]);
					$('html, body').animate({scrollTop:500}, 2000); 
				}
});
}
//----------------------------------------------------------------------------
//--7)Bloque de contacto
function consultar_cuerpo_contacto(idioma){
	var accion = 'consultar';
	var data = {
					'idioma':idioma,
					'accion':accion
	}
	$.ajax({
				'url':'./controladores/contactoController.php',
				'type':'POST',
				'data':data,
				'caache':false,
				error:function(resp){
					console.log(resp);
				},
				success:function(resp){
					var vector = $.parseJSON(resp);
					cargar_titulos_contacto(idioma,vector);
				}
	});
}

function cargar_titulos_contacto(idioma,vector){
	if(idioma==1){
		titulos_espaniol_contacto(vector);
	}else if(idioma==2){
		titulos_ingles_contacto(vector);
	}
}

function titulos_espaniol_contacto(vector){
	//--Seccion de contactos formulario--------------------------------
	$("#contacto_nombres").text("Nombres:");
	$("#contacto_apellidos").text("Apellidos:");
	$("#contacto_telefono").text("Teléfono:");
	$("#contacto_email").text("Correo Electrónico:");
	$("#contacto_pais").text("País:");
	$("#contacto_mensaje").text("Su mensaje:");
	$("#contacto_enviar").text("Enviar");
	$("#contacto_nombres2").text("Nombres:");
	$("#contacto_apellidos2").text("Apellidos:");
	$("#contacto_telefono2").text("Teléfono:");
	$("#contacto_email2").text("Correo Electrónico:");
	$("#contacto_pais2").text("País:");
	$("#contacto_mensaje2").text("Su mensaje:");
	$("#contacto_enviar2").text("Enviar");
	//--Seccion de contactos informacion
	$("#tbl_contacto").text("CONTACTO");
	$("#tbl_direccion").text("DIRECCIÓN DE OFICINA:");
	$("#info_direccion").text(vector["direccion"]);
	$("#tbl_tlf_contacto").text("TELÉFONOS");
	$("#info_telefono").text(vector["telefono"]);
	$("#info_telefono2").text(vector["telefono2"]);
	$("#info_telefono3").text(vector["telefono3"]);
	$("#info_email").text(vector["email"]);
	$("#tbl_horario").text("HORARIO OFICINA:");
	$("#tbl_horario2").text(vector["horario"]);
	$("#tbl_redes").text("SIGUENOS EN LAS REDES SOCIALES:");
	quitar_preloader("#contenido_inicial_contacto","#contenido_web_contacto");
	//-----------------------------------------------------------------
}

function titulos_ingles_contacto(vector){
	//--Seccion de contactos informacion
	$("#tbl_contacto").text("CONTACT US");
	$("#tbl_direccion").text("ADDRESS:");
	$("#info_direccion").text(vector["direccion"]);
	$("#tbl_tlf_contacto").text("TELEPHONES");
	$("#info_telefono").text(vector["telefono"]);
	$("#info_telefono2").text(vector["telefono2"]);
	$("#info_telefono3").text(vector["telefono3"]);
	$("#info_email").text(vector["email"]);
	$("#tbl_horario").text("BUSINESS HOURS:");
	$("#tbl_horario2").text(vector["horario"]);
	$("#tbl_redes").text("FOLLOW US ON:");
	//--Seccion de contactos formulario
	$("#contacto_nombres").text("Name:");
	$("#contacto_apellidos").text("Last Name:");
	$("#contacto_telefono").text("Telephone:");
	$("#contacto_email").text("Email:");
	$("#contacto_pais").text("Country:");
	$("#contacto_mensaje").text("Message:");
	$("#contacto_enviar").text("Send");
	$("#contacto_nombres2").text("Name:");
	$("#contacto_apellidos2").text("Last Name:");
	$("#contacto_telefono2").text("Telephone:");
	$("#contacto_email2").text("Email:");
	$("#contacto_pais2").text("Country:");
	$("#contacto_mensaje2").text("Message:");
	$("#contacto_enviar2").text("Send");
	quitar_preloader("#contenido_inicial_contacto","#contenido_web_contacto");
}
//-----------------------------------------------------------------------------
//8)Bloque de noticias
function consultar_noticias(idioma){
	var accion = "consultar_cuerpo_noticias";
	var data = {
					'accion':accion,
					'idioma':idioma
	};
	$.ajax({
				url:'./controladores/noticiasControler.php',
				type:'POST',
				data:data,
				cache:false,
				error: function(resp){

				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					limpiar_cuadros_noticias();
					var i;
					var a=0;
					for(i=0;i<=2;i++){
						a=i+1;
						if(a<=recordset.length){
							if(recordset[i]["imagen"]!=""){
								$("#imagen_noticias"+i).attr("src","./site_media/img/noticias/"+recordset[i]["imagen"]);
							}
							$("#titulo_noticias"+i).text(recordset[i]["titulo"]);
							$("#parrafo_noticias"+i).html(recordset[i]["parrafo"]);
							$("#btn_noticias"+i).removeClass("ocultar").addClass("mostrar");
							$("#btn_noticias"+i).attr("onclick","leer_mas2("+recordset[i]["id"]+","+idioma+",0,4,0)");
						}
					}
				}
	});
}
//Limpiar noticias
function limpiar_cuadros_noticias(){
	for(i=0;i<=2;i++){
		$("#imagen_noticias"+i).attr("src","./site_media/img/gallery.png");
		$("#titulo_noticias"+i).text(".. ");
		$("#parrafo_noticias"+i).html("<p>Noticia no cargada</p>");
		$("#btn_noticias"+i).removeClass("ocultar").addClass("mostrar");
	}
}
//--
function consultar_imagenes_galeria(idioma){
	var accion = "consultar_imagenes_galeria";
	var data = {
					'accion':accion,
					'idioma':idioma
	};
	$.ajax({
				url:'./controladores/galeriasControler.php',
				type:'POST',
				data:data,
				cache:false,
				error: function(resp){

				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					limpiar_cuadros_galerias();
					var i;
					var a=0;
					for(i=0;i<=7;i++){
						a=i+1;
						if(a<=recordset.length){
							$("#imagen_galeria"+i).attr("src","./site_media/img/galeria/"+recordset[i]["imagen"]);
						}
					}
				}
	});
}
//--
//Limpiar galeria
function limpiar_cuadros_galerias(){
	for(i=0;i<=7;i++){
		$("#imagen_galeria"+i).attr("src","./site_media/img/gallery.png");
	}
}
//--
function leer_mas(id,idioma,offset,limit,actual){
var accion = "consultar_leer_mas";	
var data = {
				'accion':accion,
				'id':id,
				'idioma':idioma,
				'offset':offset,
				'limit':limit,
				'actual':actual
}	
$.ajax({
				url:'./controladores/noticiasControler.php',
				type:'POST',
				data:data,
				cache:false,
				error:function(resp){
					console.log(resp);
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					$("#contenedor_noticias_detalle2").html(recordset["cabecera_html"]);
					$("#estructura_noticias2").html(recordset["detalle_html"]);
					$("#paginacion_tabla").html(recordset["cuerpo_paginacion"]);
					$('html, body').animate({scrollTop:500}, 2000); 
				}
});
}
//---------------------
function leer_mas2(id,idioma,offset,limit,actual){
	ocultar_elementos_ini();
	$("#cuerpo_sistema").html("");
	//---------------------------------
	$("#cuerpo_sistema").load("vistas/noticias.html");
	activar_btn("noticias");
	var idioma;
	if ($("#idioma").hasClass("esta_espaniol")==true){
		idioma = 1;
	}else if($("#idioma").hasClass("esta_ingles")==true)
	{
		idioma = 2;
	}
	//---------------------------------	
	setTimeout(function(){
		cargar_titulos_noticias(idioma);
		leer_mas(id,idioma,offset,limit,actual);
	},3000);
}
/*itulo_actual=$("#titulo_noticias"+numero).text();
//---
var imagen_arriba=$("#imagen_noticias_detalles").attr("src");
var parrafo_arriba=$("#contenedor_noticias_detalle").text();
var titulo_arriba=$("#titulo_noticias_detalles").text();
alert("Actual:"+imagen_actual+"-"+parrafo_actual+"-"+titulo_actual+"--Arriba:"+imagen_arriba+"-"+parrafo_arriba+"-"+titulo_arriba);
//--Cambio de valores
//A principal
$("#imagen_noticias_detalles").attr("src",imagen_actual);
$("#contenedor_noticias_detalle").text(parrafo_actual);
$("#titulo_noticias_detalles").text(titulo_actual);
//Bajar
$("#imagen_noticias_detalles").attr("src",imagen_arriba);
$("#contenedor_noticias_detalle").text(parrafo_arriba);
$("#titulo_noticias_detalles").text(titulo_arriba);*/
//-------------------------------------------------------------------------------