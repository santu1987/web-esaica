//---------------BLOQUE DE EVENTOS--------------------------------------------
//Carga elemento de cuerpo inicio en pantalla principal
consultar_cuerpo_nosotros(1);
//----------------------------------------------------------------------------
//---------------BLOQUE DE FUNCIONES------------------------------------------
function consultar_cuerpo_nosotros(idioma){
	var accion = 'consultar';
	var data = {
				'idioma':idioma,
				'accion':accion
	};
	$.ajax({
				url:"./controladores/nosotrosControler.php",
				type:"POST",
				data:data,
				cache:false,
				error:function(resp){
					alert("Error");
					console.log(resp);
				},
				success:function(resp){
					var recordset = $.parseJSON(resp);
					/*alert(recordset);*/
					$("#contenedor_nosotros").html(recordset["quienes_somos"]);
					$("#contenedor_mision").html(recordset["mision"]);
					$("#contenedor_vision").html(recordset["vision"]);
					$("#contenedor_nosotros_historia").html(recordset["historia"]);
				}
	});
}
//----------------------------------------------------------------------------